<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Data PDF</title>
    <link href="{{ public_path('backend/css/pdf.css') }}" rel="stylesheet">
    <style media="screen">
        @page {
            margin-top: 1cm;
            margin-left: 2cm;
            margin-right: 2cm;
        }

        .table-borderless>tbody>tr>td,
        .table-borderless>tbody>tr>th,
        .table-borderless>tfoot>tr>td,
        .table-borderless>tfoot>tr>th,
        .table-borderless>thead>tr>td,
        .table-borderless>thead>tr>th {
            border: none;
        }
    </style>
  </head>
  <body>
      <div class="row">
        <hr>
        <div class="col-3">
            <img src="{{ public_path('backend/images/logo.jpeg') }}" alt="{{ config('app.name_full') }}" style="width: 90px; float: left; margin-top: 1px;">
        </div>
        <div class="col-9">
            <h6>{{ config('app.name_full') }}</h6>
            <h6>{{ config('app.nama_jalan') }}, {{ config('app.nama_tlpon') }}</h6>
        </div>
        <hr>
        <div class="col-sm-12 text-center">
            <h5>
                <u> Kartu Stok </u>
            </h5>
            <br>
            <br>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>
                        Kode Produk
                    </th>
                    <th>
                        Nama Produk
                    </th>
                    <th>
                        Masuk
                    </th>
                    <th>
                        Keluar
                    </th>
                    <th>
                        Total
                    </th>
                    <th>
                        keterangan
                    </th>
                    <th>
                        Periode
                    </th>
                    <th>
                        Tahun
                    </th>
                </tr>
            </thead>
            <tbody>
                @isset($kartu_stok)
                    @foreach ($kartu_stok as $item)
                        <tr>
                            <td>
                                {{ baca_produk($item->id_produk)->kode_produk }}
                            </td>
                            <td>
                                {{ baca_produk($item->id_produk)->nama_produk }}
                            </td>
                            <td>
                                {{ number_format($item->masuk) }}
                            </td>
                            <td>
                                {{ number_format($item->keluar) }}
                            </td>
                            <td>
                                {{ number_format($item->total) }}
                            </td>
                            <td>
                                {{ $item->keterangan }}
                            </td>
                            <td>
                                {{ baca_periode($item->id_periode) }}
                            </td>
                            <td>
                                {{ $item->tahun }}
                            </td>
                        </tr>
                    @endforeach
                @endisset
            </tbody>
        </table>
    </div>
  </body>
</html>
