<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Data PDF Penjualan</title>
    <link href="{{ public_path('backend/css/pdf.css') }}" rel="stylesheet">
    <style media="screen">
        @page {
            margin-top: 1cm;
            margin-left: 2cm;
            margin-right: 2cm;
        }

        .table-borderless>tbody>tr>td,
        .table-borderless>tbody>tr>th,
        .table-borderless>tfoot>tr>td,
        .table-borderless>tfoot>tr>th,
        .table-borderless>thead>tr>td,
        .table-borderless>thead>tr>th {
            border: none;
        }
    </style>
  </head>
  <body>
    <table class="tabel" style="width: 100%; margin-left: 10px;">
        <tr>
            <td style="width:30%;">
                
            </td>
            <td class="text-center" style="width:30%; font-weight: bold;">
                <img src="{{ public_path('backend/images/logo.jpeg') }}" style="width: 50px; margin-top: 20px; float: right;">
            </td>
        </tr>
    </table>
    <hr>
    <div class="row">
        <div class="col-sm-12 text-left">
            <h5>{{ config('app.name_full') }}</h5>
            <b>{{ config('app.nama_jalan') }}, {{ config('app.nama_tlpon') }}</b>
            <br>
            <br>
            <hr>
        </div>
        <hr>
        <div class="col-sm-12 text-center">
            <h5>
                <u> Laporan Penjualan </u>
            </h5>
            <br>
            <br>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>No Faktur</th>
                    <th>Total belanja</th>
                    <th>Diskon</th>
                    <th>Total Bayar</th>
                    <th>Jenis Transaksi</th>
                    <th>Jenis Bayar</th>
                    <th>Status Pembayaran</th>
                </tr>
            </thead>
            <tbody>
                @isset($penjualan)
                    @foreach ($penjualan as $item)
                        <tr>
                            <th>{{ $item->faktur }}</th>
                            <th>{{ $item->total_belanja}}</th>
                            <th>{{ $item->diskon}}</th>
                            <th>{{ $item->total_bayar}}</th>
                            <th>{{ $item->jenis_transaksi}}</th>
                            <th>{{ $item->jenis_bayar}}</th>
                            <th>{{ $item->status}}</th>                            
                        </tr>
                    @endforeach
                @endisset
            </tbody>
        </table>
    </div>
  </body>
</html>
