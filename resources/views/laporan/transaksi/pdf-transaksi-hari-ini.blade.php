<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Data PDF Transaksi Hari Ini</title>
    <link href="{{ public_path('css/pdf.css') }}" rel="stylesheet">
    <style media="screen">
        @page {
            margin-top: 1cm;
            margin-left: 2cm;
            margin-right: 2cm;
        }

        .table-borderless>tbody>tr>td,
        .table-borderless>tbody>tr>th,
        .table-borderless>tfoot>tr>td,
        .table-borderless>tfoot>tr>th,
        .table-borderless>thead>tr>td,
        .table-borderless>thead>tr>th {
            border: none;
        }
    </style>
  </head>
  <body>
    <table class="tabel" style="width: 100%; margin-left: 10px;">
        <tr>
            <td style="width:30%;">
                
            </td>
            <td class="text-center" style="width:30%; font-weight: bold;">
                <img src="{{ public_path('file/logo.jpeg') }}" style="width: 50px; margin-top: 20px; float: right;">
            </td>
        </tr>
    </table>
    <hr>
    <div class="row">
        <div class="col-sm-12 text-left">
            <h5>{{ config('app.name_full') }}</h5>
            <b>{{ config('app.nama_jalan') }}, {{ config('app.nama_tlpon') }}</b>
            <br>
            <br>
            <hr>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>No Faktur</th>
                    <th>Detail</th>
                    <th>Total Belanja (Rp.)</th>
                    <th>Diskon (Rp.)</th>
                    <th>Status Retur</th>
                </tr>
            </thead>
            <tbody>
                @isset($list_transaksi)
                    @foreach ($list_transaksi as $item)
                        <tr>
                            <th>{{ $item->faktur }}</th>
                            <th>
                                @foreach (data_penjualan_detail_kode($item->faktur) as $items)
                                    <ul>
                                        <li>
                                            {{ baca_produk($items->id_produk)->nama_produk }} [{{ $items->jumlah }}]
                                        </li>
                                    </ul>
                                @endforeach    
                            </th>
                            <th>{{ number_format($item->total_belanja) }}</th>
                            <th>{{ number_format($item->diskon) }}</th>
                            <th class="text-center">
                                @if (baca_retur_count($item->faktur) > 0)
                                    <span class="pull-center-container">
                                        <small class="label pull-center bg-green">Ada Retur</small>
                                    </span>
                                @else
                                    <span class="pull-center-container">
                                        <small class="label pull-center bg-red">Tidak Ada Retur</small>
                                    </span>
                                @endif
                            </th>
                        </tr>
                    @endforeach
                @endisset
            </tbody>
            <tfoot>
                <tr>
                    <th>Total Pemasukan Hari Ini Non Diskon</th>
                    <th>
                        <u>
                            : Rp. {{ number_format($transaksi_total_belanja) }}
                        </u>
                    </th>
                </tr>
                <tr>
                    <th>Total Diskon</th>
                    <th>
                        <u>
                            : Rp. {{ number_format($transaksi_total_diskon) }}
                        </u>
                    </th>
                </tr>
                <tr>
                    <th>Total Pemasukan Hari Ini + Diskon</th>
                    <th>
                        <u>
                            : Rp. {{ number_format($transaksi_total_belanja-$transaksi_total_diskon) }}
                        </u>
                    </th>
                </tr>
            </tfoot>
        </table>
    </div>
  </body>
</html>
