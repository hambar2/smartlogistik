@extends('template')

@section('title')
  Laporan Barang
@endsection

@section('breadcrumb')
 
@endsection

@section('content')
<section class="content container-fluid">
    <div class="box box-default">
        <div class="box-body">
            <div class="col-md-12">
                @if ($sukses = Session::get('sukses_simpan_total'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ json_encode($sukses)  }}</strong>
                    </div>
                @endif
                <form method="POST" class="form-horizontal" action="{{ url('laporan/barang') }}">
                    @csrf @method('POST')
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Tanggal Mulai</label>
                                <div class="input-group col-md-6 date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <select class="form-control select2" name="abjad">
                                    <option value="" selected>Pilih Semua</option>
                                    @foreach (range('A', 'Z') as $char)
                                      @if (isset($_POST['abjad']) && $_POST['abjad'] == $char)
                                        <option value="{{ $_POST['abjad'] }}" selected>{{ $_POST['abjad'] }}</option>
                                      @else
                                        <option value="{{ $char }}">{{ $char }}</option>
                                      @endif
                                    @endforeach
                                </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="box-footer">
                            <input type="submit" name="lanjut" class="btn btn-primary btn-flat" value="CARI">
                            {{-- <input type="submit" name="excel" class="btn btn-success btn-flat fa-file-excel-o" value=" &#xf1c3; EXCEL"> --}}
                            <input type="submit" name="pdf" class="btn btn-warning btn-flat fa-file-excel-o" value=" &#xf1c3; PDF">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12">
            </div>
        </div>
    </div>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">List Laporan</h3>
        </div>
        <div class="box-body">
            <div class="col-md-12">
                <table class="table table-borderless">
                    <thead>
                        <tr>
                            <th>
                                Kode Produk
                            </th>
                            <th>
                                Nama Produk
                            </th>
                            <th>
                                Jumlah Stok
                            </th>
                            <th>
                                Harga (Rp.)
                            </th>
                            <th>
                                Harga Jual (Rp.)
                            </th>
                            <th>
                                Profit (Rp.)
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @isset($detail)
                            @foreach ($detail as $item)
                                <tr>
                                    <th>
                                        {{ $item->kode_produk }}
                                    </th>
                                    <th>
                                        {{ $item->nama_produk }}
                                    </th>
                                    <th>
                                        {{ $item->jumlah_stok }}
                                    </th>
                                    <th>
                                        {{ number_format($item->harga,0,",",".") }}
                                    </th>
                                    <th>
                                        {{ number_format($item->harga_jual,0,",",".") }}
                                    </th>
                                    <th>
                                        {{ number_format($item->laba,0,",",".") }}
                                    </th>
                                </tr>
                            @endforeach
                        @endisset
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
  <script type="text/javascript">
  </script>
@endsection
