@extends('template')

@section('title')
  Laporan Harian Transaksi Pemakaian
@endsection

@section('breadcrumb')
 
@endsection

@section('content')
<section class="content container-fluid">
    <div class="box box-default">
        <div class="box-body">
            <div class="col-md-12">
                @if ($sukses = Session::get('sukses_simpan_total'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ json_encode($sukses)  }}</strong>
                    </div>
                @endif
                <form method="POST" class="form-horizontal" action="{{ url('laporan/transaksi-pemakaian-harian') }}">
                    @csrf @method('POST')
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Tanggal Mulai</label>
                                <div class="input-group col-md-6 date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="tgl_awal" class="form-control pull-right datepicker" id="tgl_mulai" value="{{ !empty($_POST['tgl_awal']) ? $_POST['tgl_awal'] : date('Y-m-d') }}" style="width: 275px;">
                                <small class="text-danger">{{ $errors->first('tgl_awal') }}</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Tanggal Akhir</label>
                                <div class="input-group col-md-6 date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="tgl_akhir" class="form-control pull-right datepicker" id="tgl_akhir" value="{{ !empty($_POST['tgl_akhir']) ? $_POST['tgl_akhir'] : date('Y-m-t') }}" style="width: 275px;">
                                <small class="text-danger">{{ $errors->first('tgl_akhir') }}</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="box-footer">
                            <input type="submit" name="lanjut" class="btn btn-primary btn-flat" value="CARI">
                            {{-- <input type="submit" name="excel" class="btn btn-success btn-flat fa-file-excel-o" value=" &#xf1c3; EXCEL"> --}}
                            <input type="submit" name="pdf" class="btn btn-warning btn-flat fa-file-excel-o" value=" &#xf1c3; PDF">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12">
            </div>
        </div>
    </div>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">List Laporan</h3>
        </div>
        <div class="box-body">
            <div class="col-md-12">
                <table class="table table-borderless">
                    <thead>
                        <tr>
                            <th>
                                Nomor Transaksi
                            </th>
                            <th>
                                Tanggal
                            </th>
                            <th>
                                Nama Barang
                            </th>
                            <th>
                                Qty
                            </th>
                            <th>
                                Harga (Rp.)
                            </th>
                            <th>
                                Profit (Rp.)
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @isset($pemakaian)
                            @foreach ($pemakaian as $item)
                                <tr>
                                    <th>
                                        {{ $item->faktur }}
                                    </th>
                                    <th>
                                        {{ baca_pemakaian($item->faktur)->created_at }}
                                    </th>
                                    <th>
                                        @foreach (data_pemakaian_detail_kode($item->faktur) as $items)
                                            <ul>
                                                <li>
                                                    {{ baca_produk($items->id_produk)->nama_produk }}
                                                </li>
                                            </ul>
                                        @endforeach
                                    </th>
                                    <th>
                                        @foreach (data_pemakaian_detail_kode($item->faktur) as $items)
                                            <ul>
                                                <li>
                                                    {{ $items->jumlah }}
                                                </li>
                                            </ul>
                                        @endforeach
                                    </th>
                                    <th>
                                        @foreach (data_pemakaian_detail_kode($item->faktur) as $items)
                                            <ul>
                                                <li>
                                                    {{ number_format(baca_produk($items->id_produk)->harga_jual,0,",",".") }}
                                                </li>
                                            </ul>
                                        @endforeach
                                    </th>
                                    <th class="text-right">
                                        @foreach (data_pemakaian_detail_kode($item->faktur) as $items)
                                            <ul>
                                                <li>
                                                    {{ number_format($items->jumlah*baca_produk($items->id_produk)->laba,0,",",".") }}
                                                </li>
                                            </ul>
                                        @endforeach
                                    </th>
                                </tr>
                            @endforeach
                        @endisset
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="text-right" colspan="5">
                                Total Profit (Rp.)
                            </th>
                            <th class="text-right">
                                {{ number_format($total_penjualan,0,",",".") }}
                            </th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
  <script type="text/javascript">
  </script>
@endsection
