@extends('template')

@section('title')
  Laporan Penjualan
@endsection

@section('breadcrumb')
 
@endsection

@section('content')
<section class="content container-fluid">
    <div class="box box-default">
        <div class="box-body">
            <div class="col-md-12">
                <form method="POST" class="form-horizontal" action="{{ url('laporan/penjualan') }}">
                    @csrf @method('POST')
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Tanggal Mulai</label>
                                <div class="input-group col-md-6 date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="tgl_awal" class="form-control pull-right datepicker" id="tgl_mulai" value="{{ !empty($_POST['tgl_awal']) ? $_POST['tgl_awal'] : date('Y-m-d') }}" style="width: 275px;">
                                <small class="text-danger">{{ $errors->first('tgl_awal') }}</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Tanggal Akhir</label>
                                <div class="input-group col-md-6 date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="tgl_akhir" class="form-control pull-right datepicker" id="tgl_akhir" value="{{ !empty($_POST['tgl_akhir']) ? $_POST['tgl_akhir'] : date('Y-m-t') }}" style="width: 275px;">
                                <small class="text-danger">{{ $errors->first('tgl_akhir') }}</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="box-footer">
                            <input type="submit" name="lanjut" class="btn btn-primary btn-flat" value="CARI">
                            {{-- <input type="submit" name="excel" class="btn btn-success btn-flat fa-file-excel-o" value=" &#xf1c3; EXCEL"> --}}
                            <input type="submit" name="pdf" class="btn btn-warning btn-flat fa-file-excel-o" value=" &#xf1c3; PDF">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12">
            </div>
        </div>
    </div>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">List Laporan penjualan</h3>
        </div>
        <div class="box-body">
            <div class="col-md-12">
                <table class="table table-borderless">
                    <thead>
                        <tr>
                            <th>
                                Kode Faktur
                            </th>
                            <th>
                                Total Belanja
                            </th>
                            <th>
                                Total Bayar
                            </th>
                            <th>
                                Jenis Transaksi
                            </th>
                            <th>
                                Jenis Bayar
                            </th>
                            <th>
                                Status Bayar
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @isset($penjualan)
                            @foreach ($penjualan as $item)
                                <tr>
                                    <th>
                                        {{ $item->faktur }}
                                    </th>
                                    <th>
                                        {{ $item->total_belanja }}
                                    </th>
                                    <th>
                                        {{ $item->total_bayar }}
                                    </th>
                                    <th>
                                        {{ $item->jenis_transaksi }}
                                    </th>
                                    <th>
                                        {{ $item->jenis_bayar }}
                                    </th>
                                    <th>
                                        {{ $item->status }}
                                    </th>
                                </tr>
                            @endforeach
                        @endisset
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
  <script type="text/javascript">
  </script>
@endsection
