@extends('template')

@section('title')
  Transaksi Keluar <a class="btn btn-default btn-sm btn-flat" href="{{ route('transaksi.index') }}"><i class="fa fa-plus"></i> TAMBAH</a>
@endsection

@section('breadcrumb')
  <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Transaksi Keluar</li>
@endsection

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<i class="fa fa-calendar"></i>
            <h3 class="box-title">
            Data Transaksi Keluar

        	</h3>
		</div>
		<div class="box-body">
            <div class="table-responsive">
                @if ($sukses = Session::get('sukses'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $sukses }}</strong>
                    </div>
                @endif
                <table class="table table-hover table-condensed table-bordered">
					<thead>
						<tr>
							<th>No Faktur</th>
							<th>Nama Pelanggan</th>
							<th>Jenis Bayar</th>
							<th>Status</th>
							<th>Total Belanja</th>
							<th>Diskon</th>
							<th>Total Bayar</th>
							<th>Total Kembali</th>
							<th>Edit</th>
						</tr>
					</thead>
					<tbody>
                        @foreach ($list_transaksi_keluar as $ltk)
                            <tr>
                                <th>{{ $ltk->faktur }}</th>
                                <th>{{ $ltk->nama }}</th>
                                <th>{{ $ltk->jenis_bayar }}</th>
                                <th>{{ $ltk->status }}</th>
                                <th>{{ $ltk->total_belanja }}</th>
                                <th>{{ $ltk->diskon }}</th>
                                <th>{{ $ltk->total_bayar }}</th>
                                <th>{{ $ltk->total_kembali }}</th>
                                <th>
                                    <a type="button" href="{{ route('transaksi-list-keluar.edit', $ltk->id) }}" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-edit"></i></a>
                                </th>
                            </tr>
                        @endforeach
					</tbody>
				</table>
			</div>
		</div>
		<div class="box-footer">

		</div>
	</div>

@endsection

@section('script')
  <script type="text/javascript">
    //RESET FORM
    function resetForm() {
    	$('.groupName').removeClass('has-error')
		$('.errorName').text('')
		$('.groupEmail').removeClass('has-error')
		$('.errorEmail').text('')
		$('.groupPassword').removeClass('has-error')
		$('.errorPassword').text('')
		$('.groupRole').removeClass('has-error')
		$('.errorRole').text('')
    }

    //ADD FORM
    function addForm() {
    	resetForm()
    	$('#modalUser').modal('show')
    	$('.modal-title').text('Tambah User')
    	$('#formUser')[0].reset()
    	$('input[type="checkbox"]').attr('checked', false);
    }

  </script>
@endsection
