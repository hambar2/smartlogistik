@extends('template')

@section('title')
  Periode <a class="btn btn-default btn-sm btn-flat" href="{{ route('periode.create') }}"><i class="fa fa-plus"></i> TAMBAH</a>
@endsection

@section('breadcrumb')
  <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Periode</li>
@endsection

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<i class="fa fa-calendar"></i>
            <h3 class="box-title">
            Data Periode

        	</h3>
		</div>
		<div class="box-body">
            <div class="table-responsive">
                @if ($sukses = Session::get('sukses'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $sukses }}</strong>
                    </div>
                @endif
                <table class="table table-hover table-condensed table-bordered">
					<thead>
						<tr>
							<th>Nama</th>
							<th>Edit</th>
						</tr>
					</thead>
					<tbody>
                        @foreach ($periode as $item)
                            <tr>
                                <th>{{ $item->nama }}</th>
                                <th>
                                    <a type="button" href="{{ route('periode.edit', $item->id) }}" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-edit"></i></a>
                                </th>
                            </tr>
                        @endforeach
					</tbody>
				</table>
			</div>
		</div>
		<div class="box-footer">

		</div>
	</div>

@endsection

@section('script')
  <script type="text/javascript">
    //RESET FORM
    function resetForm() {
    	$('.groupName').removeClass('has-error')
		$('.errorName').text('')
		$('.groupEmail').removeClass('has-error')
		$('.errorEmail').text('')
		$('.groupPassword').removeClass('has-error')
		$('.errorPassword').text('')
		$('.groupRole').removeClass('has-error')
		$('.errorRole').text('')
    }

    //ADD FORM
    function addForm() {
    	resetForm()
    	$('#modalUser').modal('show')
    	$('.modal-title').text('Tambah User')
    	$('#formUser')[0].reset()
    	$('input[type="checkbox"]').attr('checked', false);
    }

  </script>
@endsection
