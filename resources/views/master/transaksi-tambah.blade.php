@extends('template')

@section('title')
  Prouk
@endsection

@section('breadcrumb')
  <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Produk</li>
@endsection

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<i class="fa fa-calendar"></i>
            <h3 class="box-title">
            Data Produk
		</div>
        <form method="POST" class="form-horizontal" action="{{ route('produk.store') }}">
            <div class="box-body">
                @if ($sukses = Session::get('gagal'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                       <strong>{{ json_encode($sukses)  }}</strong>
                    </div>
                @endif
                @csrf @method('POST')
              
                <div class="form-group groupName">
                    <label for="idkategori" class="col-sm-2 control-label">Kategori</label>
                    <div class="col-sm-4">
                        <select name="idkategori" id="">
                        @foreach ($kategori as $item)
                            <option value="{{ $item->id}}">{{ $item->nama}}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                
                <div class="form-group groupName">
                    <label for="nama_produk" class="col-sm-2 control-label">Nama Produk</label>
                    <div class="col-sm-4">
                        <input type="text" name="nama_produk" class="form-control" value="">
                        <small class="text-danger errorName"></small>
                    </div>
                </div>
                <div class="form-group groupAlamat">
                    <label for="jumlah_stok" class="col-sm-2 control-label">Qty</label>
                    <div class="col-sm-4">
                        <input type="text" name="jumlah_stok" class="form-control" value="">
                        <small class="text-danger errorAlamat"></small>
                    </div>
                </div>
                <div class="form-group groupEmail">
                    <label for="tanggal_masuk" class="col-sm-2 control-label">Tanggal</label>
                    <div class="col-sm-4">
                        <input type="text" name="tanggal_masuk" class="form-control" value="">
                        <small class="text-danger errorEmail"></small>
                    </div>
                </div>
               
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
            </div>
        </form>
	</div>
@endsection

@section('script')
  <script type="text/javascript">
    //RESET FORM
    function resetForm() {
    	$('.groupName').removeClass('has-error')
		$('.errorName').text('')
		$('.groupEmail').removeClass('has-error')
		$('.errorEmail').text('')
		$('.groupAlamat').removeClass('has-error')
		$('.errorAlamat').text('')
		$('.groupNotlp').removeClass('has-error')
		$('.errorNotlp').text('')
    }

  </script>
@endsection
