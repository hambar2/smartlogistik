@extends('template')

@section('title')
Gudang
@endsection

@section('breadcrumb')
<li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active">Gudang</li>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">
            Daftar Gudang
            <a class="btn btn-info btn-sm" onclick="tambah()"><i class="fa fa-plus"></i></a>
        </h3>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-hover table-bordered table-condensed">
                <thead>
                    <tr>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="Modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="form" method="POST" class="form-horizontal">
                    @csrf
                    @method('PUT')
                    <input type="text" name="id" value="" hidden>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Kode</label>
                        <div class="col-sm-9">
                            <input type="text" name="kode" class="form-control">
                            <span class="text-danger kodeError"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nama</label>
                        <div class="col-sm-9">
                            <input type="text" name="nama" class="form-control">
                            <span class="text-danger namaError"></span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Keluar</button>
                <button type="button" class="btn btn-primary btn-flat" onclick="save()">Simpan</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    //SHOW DATA
    var table;
    table = $('.table').DataTable({
        'language': {
            'url': '/DataTables/datatable-language.json',
        },
        autoWidth: false,
        processing: true,
        serverSide: true,
        ajax: '{{ route('gudang.index') }}',
        columns: [{
            data: 'kode'
        }, {
            data: 'nama'
        }, {
            data: 'aksi',
            searchable: false
        }]
    });

    function tambah() {
        $('#Modal').modal({
            backdrop: 'static',
            keyboard: false,
        })
        $("#form")[0].reset()
        $('input[name="id"]').val()
        $('input[name="_method"]').val('POST')
        $('.modal-title').text('Form Gudang')
    }

    function edit(id) {
        if (confirm('Yakin Edit Data Ini?') == true) {
            $('#Modal').modal({
                backdrop: 'static',
                keyboard: false,
            })
            $("#form")[0].reset()
            $('.modal-title').text('Form Gudang')
            $('input[name="id"]').val(id)
            $('input[name="_method"]').val('PATCH')
            $.get('/gudang/' + id + '/edit', function (resp) {
                $('input[name="kode"]').val(resp.gudang.kode)
                $('input[name="nama"]').val(resp.gudang.nama)
            })
        }
    }


    function save() {
        if (confirm('Yakin Simpan Data Ini?') == true) {
            var data = $('#form').serialize()
            var id = $('input[name="id"]').val()

            if (id == '') {
                var url = '{{ route('gudang.store') }}'
            } else {
                var url = '/gudang/' + id
            }

            $.post(url, data, function (resp) {
                if (resp.sukses == false) {
                    if (resp.error.kode) {
                        $('.kodeError').text(resp.error.kode[0]);
                    }
                    if (resp.error.nama) {
                        $('.namaError').text(resp.error.nama[0]);
                    }
                }
                if (resp.sukses == true) {
                    $('#form')[0].reset();
                    $('#Modal').modal('hide');
                    table.ajax.reload()
                    Swal({
                        position: 'middle',
                        type: 'success',
                        title: resp.message,
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
            })
        }
    }

    function saveForm() {
        var data = $('#formRole').serialize();
        var id = $('input[name="id"]').val();

        $.ajax({
                url: '/gudang/' + id,
                type: 'POST',
                dataType: 'json',
                data: data,
            })
            .done(function (data) {
                if (data.success == true) {
                    $('#formRole')[0].reset()
                    $('#Modal').modal('hide')
                    table.ajax.reload()
                    Swal({
                        position: 'middle',
                        type: 'success',
                        title: data.message,
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
            });

    }
</script>
@endsection