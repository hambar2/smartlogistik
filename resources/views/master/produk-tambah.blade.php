@extends('template')

@section('title')
    Produk
@endsection

@section('breadcrumb')
    <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Produk</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-calendar"></i>
            <h3 class="box-title">
                Data Produk
        </div>
        <form method="POST" class="form-horizontal" action="{{ route('produk.store') }}">
            <div class="box-body">
                @if ($sukses = Session::get('gagal'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ json_encode($sukses) }}</strong>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @csrf @method('POST')
                <div class="row">
                    <div class="col-md-3">
                        <label>Nama Produk</label>
                        <input type="text" name="nama_produk" class="form-control" value="">
                    </div>
                    <div class="col-md-3">
                        <label>Kategori</label>
                        <select name="id_kategori" class="form-control" required>
                            <option value="">Pilih</option>
                            @foreach ($kategori as $item)
                                <option value="{{ $item->id }}">{{ $item->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        {{-- <label>Ukuran Produk</label>
                        <select name="ukuran[]" class="form-control select2" multiple="multiple"
                            data-placeholder="Select a State" style="width: 100%;">
                            <option>XXXL</option>
                            @foreach (['S', 'M', 'L', 'XL', 'XXL', 'XXXL'] as $d)
                                <option value="{{ $d }}">{{ $d }}</option>
                            @endforeach
                        </select> --}}
                        <label>Jumlah</label>
                        <input type="number" name="jumlah_stok" class="form-control" value="">
                    </div>
                    <div class="col-md-3">
                        <label>Harga Beli</label>
                        <input type="text" name="harga" class="form-control uang" value="">
                    </div>
                </div>
                <div class="row ">
                    <div class="col-md-3">
                        <label>Harga ecer </label>
                        <input type="text" name="harga_ecer" class="form-control uang" value="">
                    </div>
                    <div class="col-md-3">
                        <label>Harga Grosir</label>
                        <input type="text" name="harga_grosir" class="form-control uang" value="">
                    </div>
                    <div class="col-md-3">
                        <label>Harga reseller</label>
                        <input type="text" name="harga_reseller" class="form-control uang" value="">
                    </div>
                    <div class="col-md-3">
                        <label>Harga Partai</label>
                        <input type="text" name="harga_partai" class="form-control uang" value="">
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script type="text/javascript"></script>
@endsection
