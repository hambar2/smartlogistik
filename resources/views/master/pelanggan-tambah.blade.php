@extends('template')

@section('title')
  Pelanggan
@endsection

@section('breadcrumb')
  <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Pelanggan</li>
@endsection

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<i class="fa fa-calendar"></i>
            <h3 class="box-title">
            Data Pelanggan
        	</h3>
		</div>
        <form method="POST" class="form-horizontal" action="{{ route('pelanggan.store') }}">
            <div class="box-body">
                @if ($sukses = Session::get('gagal'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ json_encode($sukses)  }}</strong>
                    </div>
                @endif
                @csrf @method('POST')
                <div class="form-group groupName">
                    <label for="nama" class="col-sm-2 control-label">Nama</label>
                    <div class="col-sm-4">
                        <input type="text" name="nama" class="form-control" value="">
                        <small class="text-danger errorName"></small>
                    </div>
                </div>
                <div class="form-group groupAlamat">
                    <label for="alamat" class="col-sm-2 control-label">Alamat</label>
                    <div class="col-sm-4">
                        <input type="text" name="alamat" class="form-control" value="">
                        <small class="text-danger errorAlamat"></small>
                    </div>
                </div>
                <div class="form-group groupEmail">
                    <label for="email" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-4">
                        <input type="email" name="email" class="form-control" value="">
                        <small class="text-danger errorEmail"></small>
                    </div>
                </div>
                <div class="form-group groupNotlp">
                    <label for="nomortelepon" class="col-sm-2 control-label">Nomor Telepon</label>
                    <div class="col-sm-4">
                        <input type="number" name="nomortelepon" class="form-control" value="">
                        <small class="text-danger errorNotlp"></small>
                    </div>
                </div>
               
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
            </div>
        </form>
	</div>
@endsection

@section('script')
  <script type="text/javascript">
    //RESET FORM
    function resetForm() {
    	$('.groupName').removeClass('has-error')
		$('.errorName').text('')
		$('.groupEmail').removeClass('has-error')
		$('.errorEmail').text('')
		$('.groupAlamat').removeClass('has-error')
		$('.errorAlamat').text('')
		$('.groupNotlp').removeClass('has-error')
		$('.errorNotlp').text('')
    }

  </script>
@endsection
