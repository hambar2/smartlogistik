@extends('template')

@section('title')
    Produk
@endsection

@section('breadcrumb')
    <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Produk</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-calendar"></i>
            <h3 class="box-title">
                Data Produk
            </h3>
        </div>
        <form method="POST" class="form-horizontal" action="{{ route('produk.update', $produk->id) }}">
            <div class="box-body">
                @if ($sukses = Session::get('gagal'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ json_encode($sukses) }}</strong>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        @foreach ($errors->all() as $error)
                            <strong>{{ $error }}, </strong>
                        @endforeach
                    </div>
                @endif
                @csrf @method('PUT')
                <input type="text" name="id" value="{{ $produk->id }}" hidden>
                <div class="row">
                    <div class="col-md-3">
                        <label>Kode Produk</label>
                        <input type="text" name="kode_produk" class="form-control" value="{{ $produk->kode_produk }}"
                            readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label>Nama Produk</label>
                        <input type="text" name="nama_produk" class="form-control" value="{{ $produk->nama_produk }}">
                    </div>
                    <div class="col-md-3">
                        <label>Kategori</label>
                        <select name="id_kategori" class="form-control">
                            @foreach ($kategori as $item)
                                @if ($produk->id_kategori == $item->id)
                                    <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>
                                @else
                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        {{-- <label>Ukuran Produk</label>
                        <select name="ukuran[]" class="form-control select2" multiple="multiple" data-placeholder="Select a State"
                        style="width: 100%;">
                        
                            @foreach (['S', 'M', 'L', 'XL', 'XXL', 'XXXL'] as $d)
                                @if (in_array($d, explode(',', $produk->ukuran)))
                                <option value="{{ $d }}" selected>{{ $d }}</option>
                                @else
                                <option value="{{ $d }}">{{ $d }}</option>
                                @endif
                            @endforeach
                        </select> --}}
                        <label>Jumlah</label>
                        <input type="number" name="jumlah_stok" class="form-control" value="{{ $produk->jumlah_stok }}"
                            disabled>
                    </div>
                    <div class="col-md-3">
                        <label>Harga Beli</label>
                        <input type="text" name="harga" class="form-control uang"
                            value="{{ number_format($produk->harga, 0, ',', '.') }}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label>Harga Ecer</label>
                        <input type="text" name="harga_ecer" class="form-control uang"
                            value="{{ number_format($produk->harga_jual, 0, ',', '.') }}">
                    </div>
                    <div class="col-md-3">
                        <label>Harga Grosir</label>
                        <input type="text" name="harga_grosir" class="form-control uang"
                            value="{{ number_format($produk->harga_grosir, 0, ',', '.') }}">
                    </div>
                    <div class="col-md-3">
                        <label>Harga Reseller</label>
                        <input type="text" name="harga_retail" class="form-control uang"
                            value="{{ number_format($produk->harga_retail, 0, ',', '.') }}">
                    </div>
                    <div class="col-md-3">
                        <label>Harga partai</label>
                        <input type="text" name="harga_partai" class="form-control uang"
                            value="{{ number_format($produk->harga_partai, 0, ',', '.') }}">
                    </div>
                </div>
                <div class="row">


                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        //RESET FORM
        function resetForm() {
            $('.groupName').removeClass('has-error')
            $('.errorName').text('')
            $('.groupEmail').removeClass('has-error')
            $('.errorEmail').text('')
            $('.groupAlamat').removeClass('has-error')
            $('.errorAlamat').text('')
            $('.groupNotlp').removeClass('has-error')
            $('.errorNotlp').text('')
        }
    </script>
@endsection
