@extends('template')

@section('title')
  Kategori
@endsection

@section('breadcrumb')
  <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Kategori</li>
@endsection

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<i class="fa fa-calendar"></i>
            <h3 class="box-title">
            Data Kategori
        	</h3>
		</div>
        <form method="POST" class="form-horizontal" action="{{ route('kategori.store') }}">
            <div class="box-body">
                @if ($sukses = Session::get('gagal'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ json_encode($sukses->nama)  }}</strong>
                    </div>
                @endif
                @csrf @method('POST')
                <div class="form-group groupName">
                    <label for="nama" class="col-sm-1 control-label">Nama</label>
                    <div class="col-sm-4">
                        <input type="text" name="nama" class="form-control" value="">
                        <small class="text-danger errorNama"></small>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
            </div>
        </form>
	</div>
@endsection

@section('script')
  <script type="text/javascript">
  </script>
@endsection
