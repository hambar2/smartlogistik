@extends('template')

@section('title')
    Produk <a class="btn btn-default btn-sm btn-flat" href="{{ route('produk.create') }}"><i class="fa fa-plus"></i>
        TAMBAH</a>
@endsection

@section('breadcrumb')
    <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">produk</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-calendar"></i>
            <h3 class="box-title">
                Data produk

            </h3>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                @if ($sukses = Session::get('sukses'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $sukses }}</strong>
                    </div>
                @endif
                <table class="table table-hover table-condensed table-bordered" id="example1">
                    <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Nama Produk</th>
                            <th>Jumlah</th>
                            <th>Harga Beli (Rp.)</th>
                            <th>Harga Jual (Rp.)</th>
                            <th>Harga Grosir (Rp.)</th>
                            <th>Harga Retail (Rp.)</th>
                            <th>Edit</th>
                            <th>Hapus</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($produk as $item)
                            <tr>
                                <th>{{ $item->kode_produk }}</th>
                                <th>{{ $item->nama_produk }}</th>
                                <th>{{ $item->jumlah_stok }}</th>
                                <th>{{ number_format($item->harga) }}</th>
                                <th>{{ number_format($item->harga_jual) }}</th>
                                <th>{{ number_format($item->harga_grosir) }}</th>
                                <th>{{ number_format($item->harga_retail) }}</th>
                                <th>
                                    <a type="button" href="{{ route('produk.edit', $item->id) }}"
                                        class="btn btn-sm btn-primary btn-flat"><i class="fa fa-edit"></i></a>
                                </th>
                                <th>
                                    <form method="POST" class="d-inline" onsubmit="return confirm('Yakin dihapus?')"
                                        action="{{ route('produk.destroy', $item->id) }}">
                                        @csrf
                                        <input type="hidden" value="DELETE" name="_method">
                                        <input type="submit" value="Hapus" class="btn btn-sm btn-danger btn-flat">
                                    </form>
                                </th>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="box-footer">

        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        //RESET FORM
        function resetForm() {
            $('.groupName').removeClass('has-error')
            $('.errorName').text('')
            $('.groupEmail').removeClass('has-error')
            $('.errorEmail').text('')
            $('.groupPassword').removeClass('has-error')
            $('.errorPassword').text('')
            $('.groupRole').removeClass('has-error')
            $('.errorRole').text('')
        }

        //ADD FORM
        function addForm() {
            resetForm()
            $('#modalUser').modal('show')
            $('.modal-title').text('Tambah User')
            $('#formUser')[0].reset()
            $('input[type="checkbox"]').attr('checked', false);
        }
    </script>
@endsection
