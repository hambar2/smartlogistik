@extends('template')

@section('title')
@endsection

@section('breadcrumb')
  <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Transaksi Pengeluaran</li>
@endsection

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<i class="fa fa-calendar"></i>
            <h3 class="box-title">
            Data Transaksi Pengeluaran

        	</h3>
		</div>
		<div class="box-body">
            <div class="table-responsive">
                @if ($sukses = Session::get('sukses'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $sukses }}</strong>
                    </div>
                @endif
                <table class="table table-hover table-condensed table-bordered" id="example1">
					<thead>
						<tr>
							<th>Nomor Transaksi</th>
							<th>Tanggl</th>
							<th>Total Pengeluaran</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						@isset($pengeluaran)
							@foreach ($pengeluaran as $item)
								<tr>
									<th>{{ $item->nomor_transaksi }}</th>
									<th>{{ baca_pengeluaran_kode($item->nomor_transaksi)->tanggal }}</th>
									<th>{{ number_format(sum_pengeluaran($item->nomor_transaksi)) }}</th>
									<th>
										<a type="button" href="{{ route('transaksi-pengeluaran.show', $item->nomor_transaksi) }}" class="btn btn-sm btn-info btn-flat"><i class="fa fa-eye"></i></a>
									</th>
								</tr>
							@endforeach
						@endisset
					</tbody>
				</table>
			</div>
		</div>
		<div class="box-footer">

		</div>
	</div>

@endsection

@section('script')
  <script type="text/javascript">
    //RESET FORM
    function resetForm() {
    	$('.groupName').removeClass('has-error')
		$('.errorName').text('')
		$('.groupEmail').removeClass('has-error')
		$('.errorEmail').text('')
		$('.groupPassword').removeClass('has-error')
		$('.errorPassword').text('')
		$('.groupRole').removeClass('has-error')
		$('.errorRole').text('')
    }

    //ADD FORM
    function addForm() {
    	resetForm()
    	$('#modalUser').modal('show')
    	$('.modal-title').text('Tambah User')
    	$('#formUser')[0].reset()
    	$('input[type="checkbox"]').attr('checked', false);
    }

  </script>
@endsection
