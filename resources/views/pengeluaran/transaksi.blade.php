@extends('template')

@section('title')
  Transaksi Pengeluaran
@endsection

@section('breadcrumb')
 
@endsection

@section('content')
<section class="content container-fluid">
    <div class="box box-default">
        <div class="box-body">
            <div class="col-md-12">
                <form method="POST" class="form-horizontal" action="{{ route('transaksi-pengeluaran.store') }}">
                    @csrf @method('POST')
                    <div class="row">
                        <div class="col-md-4">
                            <label>Nama Pengeluaran</label>
                            <input type="text" name="nama_pengeluaran" class="form-control" value="">
                        </div>
                        <div class="col-md-4">
                            <label>Tanggal</label>
                            <input type="date" name="tanggal" class="form-control" value="">
                        </div>
                        <div class="col-md-4">
                            <label>Total Pengeluaran</label>
                            <input type="text" name="total_pengeluaran" class="form-control uang" value="">
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="margin">
                                <div class="btn-group">
                                    <input type="submit" class="btn btn-success" name="simpan" value="Simpan">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <hr style="border-top: 2px solid rgb(0, 0, 0);">
            </div>
            <div class="col-md-12 col-sm-12">
                @if ($sukses = Session::get('gagal'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                       <strong>{{ json_encode($sukses)  }}</strong>
                    </div>
                @endif
                <h4 class="box-title">
                    Daftar Pengeluaran Hari Ini
                </h4>
                <table class="table table-condensed table-bordered">
                    <thead>
                        <tr>
                            <th>
                                Nomor Transaksi
                            </th>
                            <th>
                                Nama Pengeluaran
                            </th>
                            <th>
                                Tanggal
                            </th>
                            <th>
                                Total Pengeluaran
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @isset($pengeluaran_hari_ini)
                            @foreach ($pengeluaran_hari_ini as $item)
                                <tr>
                                    <td>
                                        {{ $item->nomor_transaksi }}
                                    </td>
                                    <td>
                                        {{ $item->nama_pengeluaran }}
                                    </td>
                                    <td>
                                        {{ $item->tanggal }}
                                    </td>
                                    <td>
                                        {{ number_format($item->total_pengeluaran) }}
                                    </td>
                                </tr>
                            @endforeach
                        @endisset
                    </tbody>
                    <tfoot>
                        
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
  <script type="text/javascript">
    jQuery(document).ready(function() {
		$('.total_kembali').text('0');
        $('input[name="total_kembali"]').val('0');
	});

    function hitung_diskon(){
        var total_diskon    = $('input[name="diskon"]').val();
		var total_belanja   = $('input[name="total_belanja"]').val();
        var diskon          = parseInt(total_diskon.split('.').join("") );
		var belanja         = parseInt(total_belanja.split('.').join("") );
        var total_semua    	= belanja - diskon;
		$('input[name="total_bayar"]').val(ribuan(total_semua));

	}

    function hitung(){
        var total_diskon    = $('input[name="diskon"]').val();
		var total_belanja   = $('input[name="total_belanja"]').val();
        var total_bayar     = $('input[name="total_bayar"]').val();
		var belanja         = parseInt(total_belanja.split('.').join("") );
        var bayar           = parseInt(total_bayar.split('.').join("") );
        var diskon          = parseInt(total_diskon.split('.').join("") );
		var total    	    = belanja - diskon;
        var total_semua     = bayar - total;
		$('input[name="total_kembali"]').val(ribuan(total_semua));
        $('.total_kembali').text(ribuan(total_semua));
	}
  </script>
@endsection
