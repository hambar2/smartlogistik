@extends('template')

@section('title')
  Transaksi Detail Pengeluaran <a class="btn btn-default btn-sm btn-flat" href="{{ route('transaksi-pengeluaran.index') }}"><i class="fa fa-list"></i> Daftar Transaksi Pengeluaran</a>
@endsection

@section('breadcrumb')
  <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Transaksi Detail Pengeluaran</li>
@endsection

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
		</div>
		<div class="box-body">
            <div class="table-responsive">
                <table class="table table-hover">
					<thead>
						<tr class="bg-primary">
							<th>Nomor Transaksi</th>
                            <th>Creator</th>
                            <th>Total Pengeluaran</th>
						</tr>
                        <tr class="bg-info">
							<th>{{ $pengeluaran->nomor_transaksi }}</th>
                            <th>{{ baca_user($pengeluaran->user_id) }}</th>
							<th>{{ number_format(sum_pengeluaran($pengeluaran->nomor_transaksi)) }}</th>
						</tr>
					</thead>
				</table>
                <hr>
                <table class="table table-hover">
					<thead class="bg-primary">
						<tr>
							<th>Nomor Transaksi</th>
							<th>Nama Pengeluaran</th>
                            <th>Tanggal</th>
                            <th>Nominal Pengeluaran</th>
						</tr>
					</thead>
					<tbody class="bg-info">
                        @isset($detail)
                            @foreach($detail as $item)
                                <tr>
                                    <td>{{ $item->nomor_transaksi }}</td>
                                    <th>{{ $item->nama_pengeluaran }}</th>
                                    <th>{{ $item->tanggal }}</th>
                                    <th>{{ number_format($item->total_pengeluaran ) }}</th>
                                </tr>
                            @endforeach
                        @endisset
					</tbody>
				</table>
			</div>
		</div>
		<div class="box-footer">

		</div>
	</div>

@endsection

@section('script')
  <script type="text/javascript">

  </script>
@endsection
