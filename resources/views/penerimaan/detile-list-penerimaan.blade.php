@extends('template')

@section('title')
  Transaksi Masuk <a class="btn btn-default btn-sm btn-flat" href="{{ route('penerimaan.index') }}"><i class="fa fa-list"></i> List Penerimaan</a>
@endsection

@section('breadcrumb')
  <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Transaksi Masuk</li>
@endsection

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<i class="fa fa-calendar"></i>
            <h3 class="box-title">
            List data Penerimaan
            </h3>
		</div>
		<div class="box-body">
            <div class="table-responsive">
                <table class="table table-hover table-condensed table-bordered">
					<thead>
						<tr>
							<th>No masuk</th>
							<th>Tanggal Masuk</th>
							<th>Creator</th>
                            <th>List Barang</th>
							
						</tr>
					</thead>
					<tbody>
                        @isset($produk_masuk)
                            @foreach($produk_masuk as $produks)
                                <tr>
                                    <th>{{ $produks->no_masuk }}</th>
                                    <th>{{ $produks->tanggal }}</th>
                                    <th>{{ baca_user($produks->id_user)}}</th>
                                    <th><select name="list" class="form-control" onchange="pilih()">
                                            <option value="">Pilih Barang</option>
                                        @foreach ($detail as $item )
                                            <option value="{{ $item->id }}"> {{ $item->nama_produk }}
                                            </option>
                                        @endforeach
                                        </select>
                                        <br>
                                        <span class="text-danger NamaError"></span>
                                    </th>
                                </tr>
                            @endforeach
                        @endisset
					</tbody>
				</table>
			</div>
		</div>
		<div class="box-footer"> <hr>
            <div class="TampilDetile hide">
                <form id="form" method="POST" class="form-horizontal tampil">
                            @csrf  @method('POST')
                        
                            <div class="col-sm-3" hidden>
                                <div>
                                    <label>id Produk</label>
                                    <input type="text" name="id_produk" class="form-control" value="" readonly>
                                </div>
                            </div>
                            <div class="col-sm-3" hidden>
                                <div>
                                    <label>kode Produk</label>
                                    <input type="text" name="kode_produk" class="form-control" value="" readonly>
                                </div>
                            </div>

                        <div class="row">
                            <div class="col-sm-3">
                                <div>
                                    <label>Nama Produk</label>
                                    <input type="text" name="nama_produk" class="form-control" value="" readonly>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div>
                                    <label>Kategori</label>
                                    <select name="id_kategori" class="form-control" required>
                                        <option value="">Pilih</option>
                                        @foreach ($kategori as $item)
                                        <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger KategoriError"></span>
                                </div>
                            </div>
                        

                            <div class="col-sm-3">
                                <div>
                                    <label>Jumlah</label>
                                    <input type="text" name="qty" class="form-control" value="" readonly>
                                </div>
                            </div>

                            <div class="col-sm-3" hidden>
                                <div>
                                    <label>Jumlah masuk</label>
                                    <input type="text" name="jumlah_masuk" class="form-control" value="" readonly>
                                </div>
                            </div>
                        

                            <div class="col-sm-3">
                                <div>
                                    <label>Tanggal</label>
                                    @foreach ($produk_masuk as $item)
                                    <input type="date" name="tanggal" class="form-control" value="{{ $item->tanggal }}" readonly>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-3">
                                <div>
                                    <label>Harga Beli</label>
                                    <input type="text" name="harga_beli" class="form-control" value=0 > 
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div>
                                    <label>Harga Jual</label>
                                    <input type="text" name="harga_jual" class="form-control" value=0 > 
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div>
                                    <label>Harga Grosir</label>
                                    <input type="text" name="harga_grosir" class="form-control" value=0>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div>
                                    <label>Harga Retail</label>
                                    <input type="text" name="harga_retail" class="form-control" value=0>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-3" >
                                <div>
                                    <label>Harga Partai</label>
                                    <input type="text" name="harga_partai" class="form-control" value=0>
                                </div>
                            </div>
                            <div class="col-sm-3" hidden>
                                <div>
                                    <label>id_periode</label>
                                @foreach ($produk_masuk as $item)
                                    <input type="text" name="id_periode" class="form-control" value={{ $item->id_periode }}>                                    
                                @endforeach
                                </div>
                            </div>
                            <div class="col-sm-3" hidden>
                                <div>
                                    <label>kode masuk</label>
                                @foreach ($produk_masuk as $item)
                                    <input type="text" name="kode_masuk" class="form-control" value={{ $item->no_masuk }}>                                    
                                @endforeach
                                </div>
                            </div>
                        </div>
                    

                        <div class="row">
                            <div class="col-sm-3" id="tombol">
                                <div>
                                    <label class="col-sm-12"><br></label>
                                    <button type="button" class="btn btn-info btn-flat" onclick="save()">Tambah</button>
                                </div>
                            </div>
                        </div>
                    </form>
            </div>
		</div>
	</div>


@endsection

@section('script')
<script type="text/javascript">
    function pilih() {
            var pilihan = $('select[name="list"]').val()
            if(pilihan == ''){
                $('.TampilDetile').addClass('hide');
                $('.NamaError').addClass('hide');
            }else{
                $('.TampilDetile').removeClass('hide');
                $('.NamaError').removeClass('hide');
                $.get('/penerimaan/' + pilihan + '/edit', function (resp) {
                $('input[name="nama_produk"]').val(resp.detail.nama_produk)
                $('input[name="qty"]').val(resp.detail.jumlah_masuk)
                $('input[name="id_produk"]').val(resp.detail.id_produk)
                $('input[name="kode_produk"]').val(resp.detail.kode_produk)
                $('input[name="jumlah_masuk"]').val(resp.detail.jumlah_masuk)
            })
            }
            
        }

    function save() {
        if (confirm('Yakin Simpan Data Ini?') == true) {
            var data = $('#form').serialize()
                var url = '{{ route('penerimaan.store') }}'
            $.post(url, data, function (resp) {
                if (resp.sukses == false) {
                    if (resp.error.nama_produk) {
                        $('.NamaError').text(resp.error.nama_produk[0]);
                    }
                    if (resp.error.id_kategori) {
                        $('.KategoriError').text(resp.error.id_kategori[0]);
                    }
                }
                if (resp.sukses == true) {
                    $('#form')[0].reset();
                    Swal({
                        position: 'middle',
                        type: 'success',
                        title: resp.message,
                        showConfirmButton: true,
                    })
                    location.reload();
                }
            })
        }
        
    }

</script>
@endsection
