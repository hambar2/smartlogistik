<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">

    <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('backend/images/logo.jpeg') }}" class="img" alt="{{ config('app.name_full') }}">
        </div>
        <div class="pull-left info">
            <p>{{ Auth::user()->name }}</p>
            <!-- Status -->
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu" data-widget="tree">
        <li class="{{ set_active(['/']) }}">
          <a href="{{ url('/') }}"><i class="fa fa-home"></i><span>Beranda</span></a>
        </li>        
        <li class="treeview {{ set_active(['user.index','user.show','user.show','role.index']) }}">
          <a href="#">
              <i class="fa fa-users"></i> <span>MASTER USER</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
              <li class="{{ set_active(['user.index', 'user.show', 'user.show']) }}">
                <a href="{{ route('user.index') }}"><i class="fa fa-users"></i> <span>Pengguna</span></a>
              </li>
              <li class="{{ set_active(['role.index']) }}">
                <a href="{{ route('role.index') }}"><i class="fa fa-signal"></i> <span>Hak Akses</span></a>
              </li>
          </ul>
        </li>
        <li class="treeview {{ set_active(['gudang.index','kategori.index','kategori.edit','pelanggan.index','produk.index','produk.edit']) }}">
          <a href="#">
              <i class="fa fa-gears"></i> <span>MASTER</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
              <li class="{{ set_active(['gudang.index']) }}">
                <a href="{{ url('gudang') }}"><i class="fa fa-list"></i> <span>Gudang</span></a>
              </li>
              <li class="{{ set_active(['kategori.index','kategori.edit']) }}">
                <a href="{{ url('kategori') }}"><i class="fa fa-list"></i> <span>Kategori</span></a>
              </li>
              <li class="{{ set_active(['pelanggan.index']) }}">
                <a href="{{ url('pelanggan') }}"><i class="fa fa-list"></i> <span>Pelanggan</span></a>
              </li>
              <li class="{{ set_active(['produk.index','produk.edit']) }}">
                <a href="{{ url('produk') }}"><i class="fa fa-list"></i> <span>Produk</span></a>
              </li>
              <li class="treeview {{ set_active(['transaksi-masuk.index', 'transaksi-list-masuk.index']) }}">
                <a href="#">
                    <i class="fa fa-tasks"></i> <span>Import Data</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ set_active(['import.view-import']) }}">
                      <a href="{{ url('view-import-produk') }}"><i class="fa fa-circle-o text-red"></i> Produk</a>
                    </li>
                </ul>
              </li>
          </ul>
        </li>
        <li class="treeview {{ set_active(['transaksi.index','transaksi-list.index']) }}">
          <a href="#">
              <i class="fa fa-cart-plus"></i> <span>PENJUALAN</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ set_active(['transaksi.index']) }}">
              <a href="{{ url('transaksi') }}"><i class="fa fa-cart-plus"></i> <span>Tambah</span></a>
            </li>
            <li class="{{ set_active(['transaksi-list.index']) }}">
              <a href="{{ url('transaksi-list') }}"><i class="fa fa-list"></i> <span>Daftar</span></a>
            </li>
          </ul>
        </li>
        <li class="treeview {{ set_active(['transaksi-retur.index']) }}">
          <a href="#">
              <i class="fa fa-cart-arrow-down"></i> <span>RETUR</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ set_active(['transaksi-retur.index']) }}">
              <a href="{{ route('transaksi-retur.index') }}"><i class="fa fa-list"></i> <span>Retur</span></a>
            </li>
          </ul>
        </li>
        <li class="treeview {{ set_active(['transaksi-pengeluaran.index','transaksi-pengeluaran.create']) }}">
          <a href="#">
              <i class="fa fa-money"></i> <span>PENGELUARAN</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ set_active(['transaksi-pengeluaran.create']) }}">
              <a href="{{ url('transaksi-pengeluaran/create') }}"><i class="fa fa-money"></i> <span>Tambah</span></a>
            </li>
            <li class="{{ set_active(['transaksi-pengeluaran.index']) }}">
              <a href="{{ url('transaksi-pengeluaran') }}"><i class="fa fa-money"></i> <span>Daftar</span></a>
            </li>
          </ul>
        </li>
        <li class="treeview {{ set_active(['transaksi-masuk.index', 'transaksi-list-masuk.index','transaksi-list-masuk.show','transaksi-masuk.index','kartu-stok.index','stok-opname.index','penerimaan.index']) }}">
          <a href="#">
              <i class="fa fa-cubes"></i> <span>LOGISTIK</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li class="treeview {{ set_active(['transaksi-masuk.index', 'transaksi-list-masuk.index', 'penerimaan.index']) }}">
              <a href="#">
                  <i class="fa fa-tasks"></i> <span>Transaksi Masuk</span>
                  <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
              <ul class="treeview-menu">
                  <li class="{{ set_active(['transaksi-list-masuk.index','transaksi-list-masuk.show']) }}">
                    <a href="{{ url('transaksi-list-masuk') }}"><i class="fa fa-circle-o text-red"></i> List Daftar</a>
                  </li>
                  <li class="{{ set_active(['transaksi-masuk.index']) }}">
                    <a href="{{ url('transaksi-masuk') }}"><i class="fa fa-circle-o text-blue"></i> Tambah</a>
                  </li>
                  <li class="{{ set_active(['penerimaan.index']) }}">
                    <a href="{{ url('penerimaan') }}"><i class="fa fa-circle-o text-yellow"></i> penerimaan</a>
                  </li>
              </ul>
            </li>
            <li class="{{ set_active(['kartu-stok.index']) }}">
              <a href="{{ url('kartu-stok') }}"><i class="fa fa-book"></i> <span>Kartu Stok</span></a>
            </li>
            <li class="{{ set_active(['stok-opname.index']) }}">
              <a href="{{ url('stok-opname') }}"><i class="fa fa-check-square-o"></i> <span>Stok Opname</span></a>
            </li>
          </ul>
        </li>
        <li class="treeview {{ set_active(['laporan.transaksi-harian','laporan.barang','laporan.transaksi-pemakaian-harian','laporan.transaksi-pengeluaran','laporan.penerimaan','laporan.opname','laporan.penjualan','laporan.pelanggan','laporan.barang-masuk']) }}">
          <a href="#">
              <i class="fa fa-file"></i> <span>LAPORAN TRANSAKSI</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ set_active(['laporan.transaksi-harian']) }}">
              <a href="{{ url('/laporan/transaksi-harian') }}"><i class="fa fa-book"></i> <span>Harian</span></a>
            </li>
            <li class="{{ set_active(['laporan.barang']) }}">
              <a href="{{ url('/laporan/barang') }}"><i class="fa fa-book"></i> <span>Stok Barang</span></a>
            </li>
            <li class="{{ set_active(['laporan.transaksi-pemakaian-harian']) }}">
              <a href="{{ url('/laporan/transaksi-pemakaian-harian') }}"><i class="fa fa-book"></i> <span>Pemakaian</span></a>
            </li>
            <li class="{{ set_active(['laporan.transaksi-pengeluaran']) }}">
              <a href="{{ url('/laporan/transaksi-pengeluaran') }}"><i class="fa fa-book"></i> <span>Pengeluaran</span></a>
            </li>
            <li class="{{ set_active(['laporan.penerimaan']) }}">
              <a href="{{ url('/laporan/penerimaan') }}"><i class="fa fa-book"></i> <span>Penerimaan</span></a>
            </li>
            <li class="{{ set_active(['laporan.opname']) }}">
              <a href="{{ url('/laporan/opname') }}"><i class="fa fa-book"></i> <span>opname</span></a>
            </li>
            <li class="{{ set_active(['laporan.penjualan']) }}">
              <a href="{{ url('/laporan/penjualan') }}"><i class="fa fa-book"></i> <span>Penjualan</span></a>
            </li>
            <li class="{{ set_active(['laporan.pelanggan']) }}">
              <a href="{{ url('/laporan/pelanggan') }}"><i class="fa fa-book"></i> <span>Pelanggan</span></a>
            </li>
            <li class="{{ set_active(['laporan.barang-masuk']) }}">
              <a href="{{ url('/laporan/barang-masuk') }}"><i class="fa fa-book"></i> <span>Barang Masuk</span></a>
            </li>
          </ul>
        </li>
        {{-- <li class="header">PER ORDER</li>
        <li class="{{ set_active(['transaksi-pre-order.create']) }}">
          <a href="{{ route('transaksi-pre-order.create') }}"><i class="fa fa-shopping-cart"></i> <span>Tambah Transaksi Pre Order</span></a>
        </li>
        <li class="{{ set_active(['transaksi-pre-order.index']) }}">
          <a href="{{ route('transaksi-pre-order.index') }}"><i class="fa fa-list"></i> <span>Daftar Transaksi Pre Order</span></a>
        </li> --}}        
        {{-- <li class="header">PEMAKAIAN</li>
        <li class="{{ set_active(['transaksi-pemakaian.index']) }}">
          <a href="{{ url('transaksi-pemakaian') }}"><i class="fa fa-cart-plus"></i> <span>Tambah Transaksi</span></a>
        </li>
        <li class="{{ set_active(['transaksi-pemakaian-list.index']) }}">
          <a href="{{ url('transaksi-pemakaian-list') }}"><i class="fa fa-list"></i> <span>Daftar Transaksi</span></a>
        </li> --}}                
    </ul>
    <!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->
