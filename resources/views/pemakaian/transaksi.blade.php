@extends('template')

@section('title')
Transaksi Pemakaian
@endsection

@section('breadcrumb')

@endsection

@section('content')
<section class="content container-fluid">
    <div class="box box-default">
        <div class="box-body">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="info-box" style="box-shadow: 1px 1px 1px 1px rgba(6, 6, 6, 0.54);">
                        <span class="info-box-icon bg-orange"><i class="fa fa-money"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text" style="font-size: 22px;">Total Belanja</span>
                            <span class="info-box-number" style="font-size: 26px;">Rp. <u><b>{{ number_format($sub_total,0,",",".") }}</b></u></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="info-box" style="box-shadow: 1px 1px 1px 1px rgba(6, 6, 6, 0.54);">
                        <span class="info-box-icon bg-green"><i class="fa fa-money"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text" style="font-size: 22px;">Total Kembalian</span>
                            <span class="info-box-number" style="font-size: 26px;">Rp. <u><b class="total_kembali"></b></u></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box box-default">
        <div class="box-body">
            <div class="col-md-12">
                <form method="POST" class="form-horizontal" action="{{ route('transaksi-pemakaian.store') }}">
                    @csrf @method('POST')
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label class="label-control col-md-12">Nama Barang</label>
                                <select class="form-control col-md-5 select2" name="produk_id" style="width: 95%;" required>
                                    <option value="">Cari Produk</option>
                                    @foreach ($produk as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama_produk }} || QTY : {{ $item->jumlah_stok }} || Harga : {{ number_format($item->harga_jual,0,",",".") }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="label-control col-md-12">Jumlah</label>
                                <input type="number" name="qty" value="1" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label class="label-control col-md-12">&nbsp;&nbsp;</label>
                            <button type="submit" class="btn btn-info btn-flat">TAMBAH</button>
                        </div>
                    </div>
                </form>
                <hr style="border-top: 2px solid rgb(0, 0, 0);">
            </div>
            <div class="col-md-7 col-sm-7">
                @if ($sukses = Session::get('gagal'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ json_encode($sukses)  }}</strong>
                </div>
                @endif
                <table class="table table-condensed table-bordered">
                    <caption>Daftar Belanja</caption>
                    <thead>
                        <tr>
                            <th>
                                Nama Barang
                            </th>
                            <th>
                                Harga
                            </th>
                            <th>
                                Jumlah
                            </th>
                            <th>
                                Sub Total
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @isset($pemakaian)
                        @foreach ($pemakaian as $item)
                        <tr>
                            <th>
                                {{ baca_produk($item->id_produk)->nama_produk }}
                            </th>
                            <th>
                                {{ number_format(baca_produk($item->id_produk)->harga) }}
                            </th>
                            <th>
                                {{ $item->jumlah }}
                            </th>
                            <th>
                                {{ number_format($item->sub_total) }}
                            </th>
                        </tr>
                        @endforeach
                        @endisset
                    </tbody>
                </table>
            </div>
            <div class="col-md-5 col-sm-5">
                <form method="POST" class="form-horizontal" action="{{ route('simpan_transaksi_pemakaian.store') }}">
                    @csrf @method('POST')
                    <input type="text" name="total_belanja" value="{{ number_format($sub_total,0,",",".") }}" hidden>
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="col-sm-12">Jenis Transaksi</label>
                            <select name="jenis_transaksi" style="font-size: 17px;" class="form-control select ">
                                <option value='tiktok'>tiktok</option>
                                <option value='shopee'>shopee</option>
                                <option value='offline'>offline</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <label class="col-sm-12">Diskon Pelanggan</label>
                            <input type="text" class="form-control uang" name="diskon" value="0" onkeyup="hitung_diskon()" style="font-size: 26px;font-weight: bold;">
                            </input>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="col-sm-10">Total Dibayar</label>
                            <div class="input-group col-sm-12">
                                <span class="input-group-addon">Rp.</span>
                                <input type="text" class="form-control uang" name="total_bayar" onkeyup="hitung()" style="font-size: 26px;font-weight: bold;">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="col-sm-10">Total Kembalian</label>
                            <div class="input-group col-sm-12">
                                <span class="input-group-addon">Rp.</span>
                                <input type="text" class="form-control" name="total_kembali" value="" readonly style="font-size: 26px;font-weight: bold;">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="margin">
                                <div class="btn-group">
                                    <input type="submit" class="btn btn-success" name="simpan" value="Simpan">
                                </div>
                                <div class="btn-group">
                                    <input type="submit" class="btn btn-danger" name="batal" value="Batal">
                                </div>
                                <div class="btn-group">
                                    <input type="submit" class="btn btn-warning" name="pending" value="Pending">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endsection

    @section('script')
    <script type="text/javascript">
        jQuery(document).ready(function() {
            $('.total_kembali').text('0');
            $('input[name="total_kembali"]').val('0');
            $('input[name="total_bayar"]').val({
                {
                    $sub_total
                }
            });
        });

        function hitung_diskon() {
            var total_diskon = $('input[name="diskon"]').val();
            var total_belanja = $('input[name="total_belanja"]').val();
            var diskon = parseInt(total_diskon.split('.').join(""));
            var belanja = parseInt(total_belanja.split('.').join(""));
            var total_semua = belanja - diskon;
            $('input[name="total_bayar"]').val(ribuan(total_semua));

        }

        function hitung() {
            var total_diskon = $('input[name="diskon"]').val();
            var total_belanja = $('input[name="total_belanja"]').val();
            var total_bayar = $('input[name="total_bayar"]').val();
            var belanja = parseInt(total_belanja.split('.').join(""));
            var bayar = parseInt(total_bayar.split('.').join(""));
            var diskon = parseInt(total_diskon.split('.').join(""));
            var total = belanja - diskon;
            var total_semua = bayar - total;
            $('input[name="total_kembali"]').val(ribuan(total_semua));
            $('.total_kembali').text(ribuan(total_semua));
        }

    </script>
    @endsection
