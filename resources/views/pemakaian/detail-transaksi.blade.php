@extends('template')

@section('title')
  Transaksi Detail Pemakaian <a class="btn btn-default btn-sm btn-flat" href="{{ route('transaksi-pemakaian-list.index') }}"><i class="fa fa-list"></i> Daftar Transaksi Pemakaian</a>
@endsection

@section('breadcrumb')
  <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Transaksi Detail Pemakaian</li>
@endsection

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
		</div>
		<div class="box-body">
            <div class="table-responsive">
                <table class="table table-hover">
					<thead>
						<tr class="bg-primary">
							<th>Faktur</th>
                            <th>Pelanggan</th>
                            <th>Kasir</th>
						</tr>
                        <tr class="bg-info">
							<th>{{ $pemakaian->faktur }}</th>
							<th>{{ baca_pelanggan($pemakaian->id_pelanggan) }}</th>
                            <th>{{ baca_user($pemakaian->user_id) }}</th>
						</tr>
					</thead>
				</table>
                <hr>
                <table class="table table-hover">
					<thead class="bg-primary">
						<tr>
							<th>Kode Produk</th>
							<th>Nama Produk</th>
                            <th>Harga Produk</th>
							<th>Jumlah</th>
                            <th>Sub Total</th>
						</tr>
					</thead>
					<tbody class="bg-info">
                        @isset($detail)
                            @foreach($detail as $item)
                                <tr>
                                    <th>{{ baca_produk($item->id_produk)->kode_produk }}</th>
                                    <th>{{ baca_produk($item->id_produk)->nama_produk }}</th>
                                    <td>{{ number_format($item->harga) }}</td>
                                    <th>{{ number_format($item->jumlah) }}</th>
                                    <th>{{ number_format($item->sub_total ) }}</th>
                                </tr>
                            @endforeach
                        @endisset
					</tbody>
                    <tfoot>
                        <tr>
                            <th colspan="4" class="text-right">Total</th>
                            <th>{{ number_format($total_pemakaian) }}</th>
                        </tr>
                        <tr>
                            <th colspan="4" class="text-right">Total Diskon</th>
                            <th>{{ number_format($pemakaian->diskon) }}</th>
                        </tr>
                        <tr>
                            <th colspan="4" class="text-right">Total Bayar</th>
                            <th>{{ number_format($pemakaian->total_bayar) }}</th>
                        </tr>
                        <tr>
                            <th colspan="4" class="text-right">Total Kembali</th>
                            <th><u>{{ number_format($pemakaian->total_kembali) }}</u></th>
                        </tr>
                    </tfoot>
				</table>
			</div>
		</div>
		<div class="box-footer">

		</div>
	</div>

@endsection

@section('script')
  <script type="text/javascript">

  </script>
@endsection
