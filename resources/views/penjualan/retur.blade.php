@extends('template')

@section('title')
  Transaksi Retur <a class="btn btn-default btn-sm btn-flat" href="{{ route('transaksi-list.index') }}"><i class="fa fa-list"></i> Daftar Transaksi Penjualan</a>
@endsection

@section('breadcrumb')
  <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Transaksi Retur</li>
@endsection

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<i class="fa fa-calendar"></i>
            <h3 class="box-title">
            Data Transaksi Penjualan
        	</h3>
		</div>
		<div class="box-body">
            <div class="table-responsive">
                <form method="POST" class="form-horizontal" action="{{ route('transaksi-retur.store') }}">
                    @csrf @method('POST')
                    <table class="table table-hover table-condensed table-bordered" style="width: 50%;">
                        <thead>
                            <tr>
                                <th>Faktur : </th>
                                <th>
                                    {{ $penjualan->faktur }}
                                    <input type="hidden" name="faktur" class="form-control" value="{{ $penjualan->faktur }}">
                                </th>
                                <th>Kasir :</th>
                                <th>{{ baca_user($penjualan->user_id) }}</th>
                            </tr>
                        </thead>
                    </table>
                    <div class="col-md-7">
                        <table class="table table-hover table-condensed table-bordered">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" value="1" id="select-all"></th>
                                    <th>Kode Produk</th>
                                    <th>Nama Produk</th>
                                    <th>Harga Produk</th>
                                    <th>Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                @isset($detail)
                                    @foreach($detail as $item)
                                        <tr>
                                            <th><input type='checkbox' name='id_produk[]' value='{{ $item->id_produk }}' required></th>
                                            <th>{{ baca_produk($item->id_produk)->kode_produk }}</th>
                                            <th>{{ baca_produk($item->id_produk)->nama_produk }}</th>
                                            <td>{{ number_format($item->harga) }}</td>
                                            <th>
                                                <input type="number" name="jumlah" class="form-control" min="{{ number_format($item->jumlah) }}" max="{{ number_format($item->jumlah) }}" value="{{ number_format($item->jumlah) }}">
                                            </th>
                                        </tr>
                                    @endforeach
                                @endisset
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-5">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="col-sm-12">Jenis Retur</label>
                                <select class="form-control select2" name="jenis_retur" required>
                                    <option value="">Cari Jenis Retur</option>
                                    <option value="tunai">Tunai</option>
                                    <option value="tukar">Tukar</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="col-sm-12">Nama Pelanggan</label>
                                <select class="form-control select2" name="id_pelanggan">
                                    <option value="">Cari Pelanggan</option>
                                    @foreach ($pelanggan as $item)
                                        <option value="{{ $item->id }}">{{ $item->nama }} || Kode : {{ $item->kode_pelanggan }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-12">Tanggal</label>
                                <input type="date" class="form-control" name="tanggal" style="line-height: normal;" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="col-sm-10">Keterangan</label>
                                <textarea name="keterangan" class="form-control" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="margin">
                                    <div class="btn-group">
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                    </div>
                                    <div class="btn-group">
                                    <button type="reset" class="btn btn-danger">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
			</div>
		</div>
		<div class="box-footer">

		</div>
	</div>

@endsection

@section('script')
  <script type="text/javascript">
    $('#select-all').click(function(){
      $('input[type="checkbox"]').prop('checked', this.checked);
    });
  </script>
@endsection
