@extends('template')

@section('title')
  Transaksi Pre Order
@endsection

@section('breadcrumb')
 
@endsection

@section('content')
<section class="content container-fluid">
    <div class="box box-default">
        <div class="box-body">
            @if ($sukses = Session::get('sukses'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ json_encode($sukses)  }}</strong>
                </div>
            @endif
            <form method="POST" class="form-horizontal" action="{{ route('transaksi-pre-order.store') }}">
                @csrf @method('POST')
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-12">Nama Pelanggan</label>
                                <select class="form-control select2" name="id_pelanggan" required>
                                    <option value="">Cari Pelanggan</option>
                                    @isset($pelanggan)
                                        @foreach ($pelanggan as $item)
                                            <option value="{{ $item->id }}">{{ $item->nama }} || Kode : {{ $item->kode_pelanggan }}</option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="col-sm-12">Nama Kategori</label>
                            <select class="form-control select2" name="id_kategori" required>
                                <option value="">Cari Kategori</option>
                                @isset($kategori)
                                    @foreach ($kategori as $item)
                                        <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                    @endforeach
                                @endisset
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-12">Nama</label>
                                <input type="text" class="form-control" name="nama" style="line-height: normal;" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="col-sm-12">Warna</label>
                            <input type="text" class="form-control" name="warna" style="line-height: normal;" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-12">Ukuran</label>
                                <select name="ukuran" class="form-control" required>
                                    <option value="">Pilih</option>
                                    <option value="S">S</option>
                                    <option value="M">M</option>
                                    <option value="L">L</option>
                                    <option value="XL">XL</option>
                                    <option value="XXL">XXL</option>
                                    <option value="XXXL">XXXL</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="col-sm-12">Harga</label>
                            <input type="text" class="form-control uang" name="harga" style="line-height: normal;" required>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="col-sm-10">Keterangan</label>
                            <textarea name="keterangan" class="form-control" cols="30" rows="10" required></textarea>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="margin">
                                <div class="btn-group">
                                <button type="submit" class="btn btn-success">Simpan</button>
                                </div>
                                <div class="btn-group">
                                <button type="submit" class="btn btn-danger">Batal</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
  <script type="text/javascript">
    
  </script>
@endsection
