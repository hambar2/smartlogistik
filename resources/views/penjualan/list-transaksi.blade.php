@extends('template')

@section('title')
  Transaksi Daftar Penjualan
@endsection

@section('breadcrumb')
  <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Transaksi Penjualan</li>
@endsection

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<i class="fa fa-calendar"></i>
            <h3 class="box-title">
            Data Transaksi Penjualan

        	</h3>
		</div>
		<div class="box-body">
            <div class="table-responsive">
                @if ($sukses = Session::get('sukses'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $sukses }}</strong>
                    </div>
                @endif
                <table class="table table-hover table-condensed table-bordered" id="example1">
					<thead>
						<tr>
							<th>No Faktur</th>
							<th>Nama Pelanggan</th>
							<th>Status</th>
							<th>Total Belanja</th>
							<th>Diskon</th>
							<th>Status Retur</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						@isset($list_transaksi)
							@foreach ($list_transaksi as $item)
								<tr>
									<th>{{ $item->faktur }}</th>
									<th>{{ baca_pelanggan($item->id_pelanggan) }}</th>
									<th>{{ $item->status }}</th>
									<th>{{ number_format($item->total_belanja) }}</th>
									<th>{{ number_format($item->diskon) }}</th>
									<th class="text-center">
										@if (baca_retur_count($item->faktur) > 0)
											<span class="pull-center-container">
												<small class="label pull-center bg-green">Ada Retur</small>
											</span>
										@else
										<span class="pull-center-container">
											<small class="label pull-center bg-red">Tidak Ada Retur</small>
										</span>
										@endif
									</th>
									<th>
										<a type="button" href="{{ route('transaksi-list.show', $item->faktur) }}" class="btn btn-sm btn-info btn-flat"><i class="fa fa-eye"></i></a>
										@if (baca_retur_count($item->faktur) > 0)
											<span class="pull-center-container">
												<small class="label pull-center bg-green">Ada Retur</small>
											</span>
											<a type="button" href="{{ route('transaksi-retur.detail', baca_retur($item->faktur)->kode_retur) }}" class="btn btn-sm btn-info btn-flat"><i class="fa fa-eye"> Detail Retur</i></a>
										@else
											<a type="button" href="{{ route('transaksi-retur.show', $item->faktur) }}" class="btn btn-sm btn-warning btn-flat"><i class="fa fa-edit"></i>Retur</a>
										@endif
									</th>
								</tr>
							@endforeach
						@endisset
					</tbody>
				</table>
			</div>
		</div>
		<div class="box-footer">

		</div>
	</div>

@endsection

@section('script')
  <script type="text/javascript">
    //RESET FORM
    function resetForm() {
    	$('.groupName').removeClass('has-error')
		$('.errorName').text('')
		$('.groupEmail').removeClass('has-error')
		$('.errorEmail').text('')
		$('.groupPassword').removeClass('has-error')
		$('.errorPassword').text('')
		$('.groupRole').removeClass('has-error')
		$('.errorRole').text('')
    }

    //ADD FORM
    function addForm() {
    	resetForm()
    	$('#modalUser').modal('show')
    	$('.modal-title').text('Tambah User')
    	$('#formUser')[0].reset()
    	$('input[type="checkbox"]').attr('checked', false);
    }

  </script>
@endsection
