@extends('template')

@section('title')
  Transaksi Detail Retur <a class="btn btn-default btn-sm btn-flat" href="{{ route('transaksi-retur.index') }}"><i class="fa fa-list"></i> Daftar Transaksi Retur</a>
@endsection

@section('breadcrumb')
  <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Transaksi Detail Retur</li>
@endsection

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<i class="fa fa-calendar"></i>
            <h3 class="box-title">
            Data Transaksi Retur
        	</h3>
		</div>
		<div class="box-body">
            <div class="table-responsive">
                <div class="col-md-7">
                    <table class="table table-hover table-condensed table-bordered">
                        <thead>
                            <tr>
                                <th>Kode Produk</th>
                                <th>Nama Produk</th>
                                <th>Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            @isset($detail)
                                @foreach($detail as $item)
                                    <tr>
                                        <th>{{ baca_produk($item->id_produk)->kode_produk }}</th>
                                        <th>{{ $item->nama_produk }}</th>
                                        <th>{{ number_format($item->jumlah) }}</th>
                                    </tr>
                                @endforeach
                            @endisset
                        </tbody>
                    </table>
                </div>
                <div class="col-md-5">
                    <table class="table table-hover table-condensed table-bordered">
                        <thead>
                            <tr>
                                <th>Tanggal :</th>
                                <th>{{ $retur->tanggal }}</th>
                                <th>Kode Retur :</th>
                                <th>{{ $retur->kode_retur }}</th>
                            </tr>
                            <tr>
                                <th>Pelanggan :</th>
                                <th>{{ baca_pelanggan($retur->id_pelanggan) }}</th>
                                <th>User :</th>
                                <th>{{ baca_user($retur->user_id) }}</th>
                            </tr>
                            <tr>
                                <th colspan="4">Keterangan : {{ $retur->keterangan }}</th>
                            </tr>
                        </thead>
                    </table>
                </div>
			</div>
		</div>
		<div class="box-footer">

		</div>
	</div>

@endsection

@section('script')
  <script type="text/javascript">

  </script>
@endsection
