<html>
<head>
    <title>Nota PDF</title>
    <link href="{{ asset('css/pdf.css') }}" rel="stylesheet">
    <meta http-equiv="Refresh" content="2; URL={{ url('/transaksi') }}">
</head>
<body onload="window.print();" align="center" style="width: 300px; height: 200px; font-size: 10px; font-family: serif;">
<table style="width:100%; font-size: 12px; letter-spacing: 2px;">
  <tr>
    <td align="center">
        {{ config('app.nama_toko') }}<br>
        {{ config('app.alamat_toko_1') }}<br>
        {{ config('app.alamat_toko_2') }}
    </td>
  </tr>
</table>
<hr>
<table style="width:100%; font-size: 10px; letter-spacing: 2px">
  <tbody>
    <tr>
      <td>{{ $penjualan->created_at }} / {{ $penjualan->faktur }}</td>
    </tr>
    <tr>
      <td>{{ baca_user($penjualan->user_id) }}</td>
    </tr>
  </tbody>
</table>
<hr>        
<table style="width:100%; font-size: 12px; letter-spacing: 2px">
  <tbody>
    @foreach($detail as $item)
    <tr>
        <td align="right">
            {{ baca_produk($item->id_produk)->nama_produk }}
        </td>
        <td align="right">
            {{ number_format($item->harga) }} x {{ number_format($item->harga) }}
        </td>
        <td>:</td>
        <td align="right">
            {{ number_format($item->sub_total,0,".",".") }}
        </td>
    </tr>
    @endforeach 
  </tbody>
</table>
<hr>  
<table style="width:100%; font-size: 12px; letter-spacing: 2px;">
  <tbody>
        <tr>
            <td colspan="1" align="right">Total Pembelian (Rp.)</td>
            <td colspan="3" align="right">{{ number_format($penjualan->total_belanja,0,".",".") }}</td>
        </tr>
        <tr>
            <td colspan="1" align="right">Diskon (Rp.)</td>
            <td colspan="3" align="right">{{ number_format($penjualan->diskon,0,".",".")}}</td>
        </tr>
        <tr>
            <td colspan="1" align="right">Total Bayar (Rp.)</td>
            <td colspan="3" align="right">{{ number_format($penjualan->total_bayar,0,".",".") }}</td>
        </tr>
        <tr>
            <td colspan="1" align="right">Kembalian (Rp.)</td>
            <td colspan="3" align="right">{{ number_format($penjualan->total_kembali,0,".",".") }}</td>
        </tr>
  </tbody>
</table>
<hr>
<table style="width:100%; font-size: 11px; letter-spacing: 2px;">
  <tr>
    <td align="center">
          TERIMAKSIH ATAS KUNJUNGAN ANDA<br>
          BARANG YANG SUDAH DIBELI <br> TIDAK DAPAT DITUKAR / DIKEMBALIKAN
    </td>
  </tr>
</table>
</body>
</html>