@extends('template')

@section('title')
  Detail Transaksi Pre Order <a class="btn btn-default btn-sm btn-flat" href="{{ route('transaksi-pre-order.index') }}"><i class="fa fa-list"></i> Daftar Transaksi Pre Order</a>
@endsection

@section('breadcrumb')
@endsection

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<i class="fa fa-calendar"></i>
            <h3 class="box-title">
                Detail Pre Order
        	</h3>
		</div>
		<div class="box-body">
            <div class="table-responsive">
                <table class="table table-hover table-condensed table-bordered" style="width: 50%">
					<thead class="bg-warning">
						<tr>
							<th>Kode Pre Order :</th>
                            <th>{{ $list_transaksi->nama }}</th>
							<th>User :</th> 
                            <th>{{ baca_user($list_transaksi->user_id) }}</th>
                        </tr>
					</thead>
				</table>
                <hr>
                <table class="table table-hover table-condensed" style="width: 100%">
					<thead class="bg-info">
						<tr>
							<th>Nama Barang</th>
                            <th>{{ $list_transaksi->nama }}</th>
                            <th rowspan="5" style="vertical-align:top">Keterangan :<br>{{ $list_transaksi->keterangan }}</th>
                        </tr>
                        <tr>
							<th>Kategori</th> 
                            <th>{{ baca_kategori($list_transaksi->id_kategori) }}</th>
                        </tr>
                        <tr>
							<th>Harga</th>
                            <th>{{ $list_transaksi->harga }}</th>
                        </tr>
                        <tr>
							<th>Warna</th> 
                            <th>{{ $list_transaksi->warna }}</th>
                        </tr>
                        <tr>
							<th>Ukuran</th> 
                            <th>{{ $list_transaksi->ukuran }}</th>
                        </tr>
					</thead>
				</table>
			</div>
		</div>
		<div class="box-footer">

		</div>
	</div>

@endsection

@section('script')
  <script type="text/javascript">

  </script>
@endsection
