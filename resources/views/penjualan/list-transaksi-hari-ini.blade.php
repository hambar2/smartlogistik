@extends('template')

@section('title')
@endsection

@section('breadcrumb')
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-calendar"></i>
            <h3 class="box-title">
                Data Transaksi Hari Ini
                <a class="btn btn-warning btn-sm" href="{{ url('/cetak-transaksi-hari-ini') }}" target="_blank"><i class="fa fa-print"></i> Cetak PDF</a>
            </h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-8">
                    <div class="table-responsive">
                        <table class="table table-hover table-condensed table-bordered" id="example1">
                            <thead>
                                <tr>
                                    <th>No Faktur</th>
                                    <th>Detail</th>
                                    <th>Total Belanja (Rp.)</th>
                                    <th>Diskon (Rp.)</th>
                                    <th>Status Retur</th>
                                </tr>
                            </thead>
                            <tbody>
                                @isset($list_transaksi)
                                    @foreach ($list_transaksi as $item)
                                        <tr>
                                            <th>{{ $item->faktur }}</th>
                                            <th>
                                                @foreach (data_penjualan_detail_kode($item->faktur) as $items)
                                                    <ul>
                                                        <li>
                                                            {{ baca_produk($items->id_produk)->nama_produk }} [{{ $items->jumlah }}] 
                                                            @if (baca_produk_retur_count($item->faktur,$items->id_produk) > 0)
                                                                <span class="pull-center-container">
                                                                    <small class="label pull-center bg-green">Retur</small>
                                                                </span>
                                                            @else
                                                                <span class="pull-center-container">
                                                                    <small class="label pull-center bg-red">Tidak Retur</small>
                                                                </span>
                                                            @endif
                                                        </li>
                                                    </ul>
                                                @endforeach    
                                            </th>
                                            <th>{{ number_format($item->total_belanja) }}</th>
                                            <th>{{ number_format($item->diskon) }}</th>
                                            <th class="text-center">
                                                @if (baca_retur_count($item->faktur) > 0)
                                                    <span class="pull-center-container">
                                                        <small class="label pull-center bg-green">Ada Retur</small>
                                                    </span>
                                                @else
                                                    <span class="pull-center-container">
                                                        <small class="label pull-center bg-red">Tidak Ada Retur</small>
                                                    </span>
                                                @endif
                                            </th>
                                        </tr>
                                    @endforeach
                                @endisset
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info-box bg-yellow">
                        <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Total Pemasukan Hari Ini</span>
                            <span class="info-box-number">Rp. {{ number_format($transaksi_total_belanja) }}</span>
                        </div>
                    </div>
                    <div class="info-box bg-green">
                        <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Total Diskon Hari Ini</span>
                            <span class="info-box-number">Rp. {{ number_format($transaksi_total_diskon) }}</span>
                        </div>
                    </div>
                    <div class="info-box bg-red">
                        <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Total Retur Hari Ini</span>
                            <span class="info-box-number">Rp. {{ number_format($transaksi_total_retur) }}</span>
                        </div>
                    </div>
                    <div class="info-box bg-aqua">
                        <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text text-wrap">Total Pemasukan + Diskon Hari Ini</span>
                            <span class="info-box-number">Rp. {{ number_format($transaksi_total_belanja-$transaksi_total_diskon) }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript"></script>
@endsection
