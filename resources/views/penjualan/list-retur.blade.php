@extends('template')

@section('title')
  Transaksi Retur
@endsection

@section('breadcrumb')
  <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Transaksi Retur</li>
@endsection

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<i class="fa fa-calendar"></i>
            <h3 class="box-title">
            Data Transaksi Retur
        	</h3>
		</div>
		<div class="box-body">
            <div class="table-responsive">
				<div class="col-md-7">

				</div>
                <table class="table table-hover table-condensed table-bordered" id="example1">
					<thead>
						<tr>
							<th>No Faktur</th>
							<th>Kode Retur</th>
							<th>Tanggal</th>
							<th>Pelanggan</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						@isset($list_transaksi)
							@foreach ($list_transaksi as $item)
								<tr>
									<th>{{ $item->faktur }}</th>
									<th>{{ $item->kode_retur }}</th>
									<th>{{ $item->tanggal }}</th>
									<th>{{ baca_pelanggan($item->id_pelanggan) }}</th>
									<th>
										<a type="button" href="{{ route('transaksi-retur.detail', $item->kode_retur) }}" class="btn btn-sm btn-info btn-flat"><i class="fa fa-eye"></i></a>
									</th>
								</tr>
							@endforeach
						@endisset
					</tbody>
				</table>
			</div>
		</div>
		<div class="box-footer">

		</div>
	</div>

@endsection

@section('script')
  <script type="text/javascript">
    //RESET FORM
    function resetForm() {
    	$('.groupName').removeClass('has-error')
		$('.errorName').text('')
		$('.groupEmail').removeClass('has-error')
		$('.errorEmail').text('')
		$('.groupPassword').removeClass('has-error')
		$('.errorPassword').text('')
		$('.groupRole').removeClass('has-error')
		$('.errorRole').text('')
    }

    //ADD FORM
    function addForm() {
    	resetForm()
    	$('#modalUser').modal('show')
    	$('.modal-title').text('Tambah User')
    	$('#formUser')[0].reset()
    	$('input[type="checkbox"]').attr('checked', false);
    }

  </script>
@endsection
