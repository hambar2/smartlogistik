<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('app.name') }}</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('backend/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('backend/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('backend/bower_components/Ionicons/css/ionicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('backend/dist/css/AdminLTE.css') }}">
  <link rel="stylesheet" href="{{ asset('backend/dist/css/skins/_all-skins.min.css') }}">
  <link rel="stylesheet" href="{{ asset('backend/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
  <link rel="stylesheet" href="{{ asset('backend/css/checkboxStyle.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('backend/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{ asset('backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">

  <!-- DATATABLES -->
  <link rel="stylesheet" href="{{ asset('backend/DataTables/css/dataTables.bootstrap.min.css') }}">
  <!-- SWEET ALERT2 -->
  <link rel="stylesheet" href="{{ asset('backend/sweetalert2/dist/sweetalert2.css') }}">
  <link rel="stylesheet" href="{{ asset('backend/bower_components/select2/dist/css/select2.css') }}">

  <style>
    .form-group{
      margin-bottom: 5px;
    }
    .select2-container *:focus {
      border-color: #FF0000;
      box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6);
    }
    .form-radio{
         -webkit-appearance: none;
         -moz-appearance: none;
         appearance: none;
         display: inline-block;
         position: relative;
         background-color: #f1f1f1;
         color: #666;
         top: 2px;
         height: 16px;
         width: 16px;
         cursor: pointer;
         border: 1px solid #d0d8e5;
    }
    .form-radio:checked::before{
         position: absolute;
         left: 4px;
         content: '\02143';
         transform: rotate(35deg);
    }
    .form-radio:hover{
         background-color: #f7f7f7;
    }
    .form-radio:checked{
         background-color: #f1f1f1;
    }

  </style>

</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse layout-fixed">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">
    @include('header')
  </header>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    @include('sidebar')
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @yield('title')
      </h1>
      <ol class="breadcrumb">
        @yield('breadcrumb')
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

      @yield('content')

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    @include('footer')
  </footer>

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{ asset('backend/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('backend/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('backend/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('backend/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('backend/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('backend/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('backend/bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('backend/dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('backend/dist/js/demo.js') }}"></script>
<!-- DATATABLES -->
<script src="{{ asset('backend/DataTables/js/jquery.dataTables.min.js') }}" charset="utf-8"></script>
<script src="{{ asset('backend/DataTables/js/dataTables.bootstrap.min.js') }}" charset="utf-8"></script>
<script src="{{ asset('backend/sweetalert2/dist/sweetalert2.all.js') }}" charset="utf-8"></script>
<script src="{{ asset('backend/js/jquery.masknumber.js') }}"></script>
<script src="{{ asset('backend/bower_components/select2/dist/js/select2.full.min.js') }}" charset="utf-8"></script>

@yield('script')
{{-- notifikasi --}}
@if ($message = Session::get('total_bayar_kosong'))
<script type="text/javascript">
  Swal.fire({
    icon: 'error',
    title: 'Oops...',
    text: 'Total Bayar Kosong !'
  })
</script>
@endif
@if ($message = Session::get('total_bayar_kurang'))
<script type="text/javascript">
  Swal.fire({
    icon: 'error',
    title: 'Oops...',
    text: 'Total Bayar Kurang !'
  })
</script>
@endif
@if ($message = Session::get('gagal'))
<script type="text/javascript">
    toastr.error('Gagal Hapus !');
</script>
@endif
@if ($message = Session::get('sukses'))
<script type="text/javascript">
  Swal.fire({
    position: 'center',
    icon: 'success',
    title: 'Berhasil !',
    showConfirmButton: false,
    timer: 1500
  })
</script>
@endif
@if ($message = Session::get('data_kosong'))
<script type="text/javascript">
  Swal.fire({
    position: 'center',
    icon: 'success',
    title: 'Data Kosong !',
    showConfirmButton: false,
    timer: 1500
  })
</script>
@endif
<script>
  jQuery(document).ready(function() {
    $('.uang').maskNumber({
        thousands: ".",
        integer: true,
    });
  });

  function ribuan(x) {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }
  $(function () {
    $("#example1").DataTable({
      "language": {
          'url': '/DataTables/datatable-language.json',
      },
      "responsive": true,
      "autoWidth": false,
      "ordering": false,
    });
    $('#example2').DataTable({
      "language": {
          'url': '/DataTables/datatable-language.json',
      },
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
    $('#example3').DataTable({
      "language": {
          'url': '/DataTables/datatable-language.json',
      },
      "paging": false,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "responsive": false,
    });
    $('.example4').DataTable({
      "language": {
          'url': '/DataTables/datatable-language.json',
      },
      "paging": false,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "responsive": false,
    });
  });
  $(function () {
    $('.select2').select2();

    $(".datepicker").datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
    });

    $("#tgl_mulai").on('changeDate', function(selected) {
        var startDate = new Date(selected.date.valueOf());
        $("#tgl_akhir").datepicker('setStartDate', startDate);
        if($("#tgl_mulai").val() > $("#tgl_akhir").val()){
          $("#tgl_akhir").val($("#tgl_mulai").val());
        }
    });
  });
</script>
</body>
</html>
