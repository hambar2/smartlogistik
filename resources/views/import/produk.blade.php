@extends('template')

@section('title')
    Laporan Barang
@endsection

@section('breadcrumb')
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="box box-default">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <form method="POST" class="form-horizontal" action="{{ route('import.template-produk') }}">
                            @csrf @method('POST')
                            {{-- <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Kategori Produk</label>
                                    <div class="input-group col-md-12">
                                        <div class="input-group-addon">
                                            <i class="fa fa-list"></i>
                                        </div>
                                        <select name="id_kategori" class="form-control">
                                            <option value="">Pilih</option>
                                            @foreach ($kategori as $item)
                                                <option value="{{ $item->id}}">{{ $item->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <small class="text-danger">{{ $errors->first('kategori') }}</small>
                                </div>
                            </div>
                        </div> --}}
                            <h4 class="box-title">Template Import</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="submit" name="lanjut" class="btn btn-warning btn-flat"
                                            value="Download Template">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-9">
                        <form method="POST" class="form-horizontal" action="{{ route('import.produk') }}"
                            enctype="multipart/form-data">
                            @csrf @method('POST')
                            <div class="row">
                                <h4 class="box-title">Import Data</h4>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Kategori Produk</label>
                                        <div class="input-group col-7">
                                            <select name="id_kategori" class="form-control" style="100%">
                                                <option value="">Pilih</option>
                                                @foreach ($kategori as $item)
                                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <small class="text-danger">{{ $errors->first('kategori') }}</small>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="control-label">File Import</label>
                                        <div class="input-group">
                                            <input type="file" name="file" class="form-control fill">
                                        </div>
                                        <small class="text-danger">{{ $errors->first('file') }}</small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="submit" name="lanjut" class="btn btn-primary btn-flat"
                                                value="Import">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {{-- <h6>(*) <i>Format untuk input Ukuran : S,M,L,XL,XXL.</i></h6> --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">List Laporan</h3>
            </div>
            <div class="box-body">
                <div class="col-md-12">
                    <table class="table table-borderless">
                        <thead>
                            <tr>
                                <th>
                                    No
                                </th>
                                <th>
                                    Kode Entri
                                </th>
                                <th>
                                    Kode Produk
                                </th>
                                <th>
                                    Nama Produk
                                </th>
                                <th>
                                    Kategori
                                </th>
                                {{-- <th>
                                    Ukuran
                                </th> --}}
                                <th>
                                    Jumlah Stok
                                </th>
                                <th>
                                    Harga (Rp.)
                                </th>
                                <th>
                                    Harga Jual (Rp.)
                                </th>
                                <th>
                                    Harga Grosir (Rp.)
                                </th>
                                <th>
                                    Harga Retail (Rp.)
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @isset($list)
                                @foreach ($list as $item)
                                    <tr>
                                        <td>
                                            {{ $no++ }}
                                        </td>
                                        <td>
                                            {{ $item->kode_entri }}
                                        </td>
                                        <td>
                                            {{ $item->kode_produk }}
                                        </td>
                                        <td>
                                            {{ $item->nama_produk }}
                                        </td>
                                        <td>
                                            {{ $item->id_kategori }}
                                        </td>
                                        <td>
                                            {{ $item->ukuran }}
                                        </td>
                                        <td>
                                            {{ $item->jumlah_stok }}
                                        </td>
                                        <td>
                                            {{ number_format($item->harga, 0, ',', '.') }}
                                        </td>
                                        <td>
                                            {{ number_format($item->harga_jual, 0, ',', '.') }}
                                        </td>
                                        <td>
                                            {{ number_format($item->harga_grosir, 0, ',', '.') }}
                                        </td>
                                        <td>
                                            {{ number_format($item->harga_retail, 0, ',', '.') }}
                                        </td>
                                    </tr>
                                @endforeach
                            @endisset
                        </tbody>
                    </table>
                    <div class="card-footer" style="position: fixed; bottom: 60px; right: 40%">
                        <form method="POST" class="form-horizontal" action="{{ route('verifikasi.data-produk') }}"
                            enctype="multipart/form-data">
                            @csrf @method('POST')
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="input-group col-7">
                                            <select name="kode_entri" class="form-control" style="100%">
                                                <option value="">Pilih Kode Entri</option>
                                                @foreach ($kode_entri as $item)
                                                    <option value="{{ $item->kode_entri }}">{{ $item->kode_entri }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <small class="text-danger">{{ $errors->first('kode_entri') }}</small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <input type="submit" name="simpan" class="btn btn-primary btn-flat" value="Simpan">
                                <input type="submit" name="batal" class="btn btn-danger btn-flat fa-file-excel-o"
                                    value="Batal">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endsection

    @section('script')
        <script type="text/javascript"></script>
    @endsection
