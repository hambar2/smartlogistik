@extends('template')

@section('title')
    Dashboard
@endsection

@section('breadcrumb')
    <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-dashboard"></i>
            <h3 class="box-title">Dashboard</h3>
        </div>
        <div class="box-body">
            <p>
                Selamat, {{ Auth::user()->name }}. Anda berhasil Login sebagai:
            </p>
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="fa fa-archive"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">PRODUK</span>
                            <span class="info-box-number"><small>{{ $produk }}</small></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="fa fa-file-text-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">KATEGORI</span>
                            <span class="info-box-number"><small>{{ $kategori }}</small></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-purple"><i class="fa fa-cart-plus"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Transaksi Hari Ini</span>
                            <span class="info-box-number"><small>{{ $penjualan }}</small></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-yellow"><i class="ion ion-ios-gear-outline"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">PENGGUNA</span>
                            <span class="info-box-number"> <small>{{ $user }}</small></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">

        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript"></script>
@endsection
