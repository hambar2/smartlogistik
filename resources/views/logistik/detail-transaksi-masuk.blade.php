@extends('template')

@section('title')
  Transaksi Masuk <a class="btn btn-default btn-sm btn-flat" href="{{ route('transaksi-list-masuk.index') }}"><i class="fa fa-list"></i> Daftar Produk Masuk</a>
@endsection

@section('breadcrumb')
  <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Transaksi Masuk</li>
@endsection

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<i class="fa fa-calendar"></i>
            <h3 class="box-title">
            Data Transaksi Masuk    
        	</h3>
		</div>
		<div class="box-body">
            <div class="table-responsive">
                <table class="table table-hover table-condensed table-bordered">
					<thead>
						<tr>
							<th>Kode Produk</th>
							<th>Nama Produk</th>
							<th>Jumlah Barang Masuk</th>
							
						</tr>
					</thead>
					<tbody>
                        @isset($detail)
                            @foreach($detail as $item)
                                <tr>
                                    <th>{{ $item->kode_produk }}</th>
                                    <th>{{ $item->nama_produk }}</th>
                                    <th>{{ $item->jumlah_masuk }}</th>
                                </tr>
                            @endforeach
                        @endisset
					</tbody>
                    <tfoot>
                        <tr>
                            <th colspan="2" class="text-right">Total</th>
                            <th>{{ number_format($total_masuk) }}</th>
                        </tr>
                    </tfoot>
				</table>
			</div>
		</div>
		<div class="box-footer">

		</div>
	</div>

@endsection

@section('script')
  <script type="text/javascript">

  </script>
@endsection
