@extends('template')

@section('title')
  Transaksi Produk Masuk
@endsection

@section('breadcrumb')
 
@endsection

@section('content')
{{-- bawah --}}
<section class="content container-fluid">
    <div class="box box-default">
        <div class="box-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Pilih Jenis Produk</label>
                        <select name="pilihTransaksi" class="form-control" onchange="pilih()">
                            <option value="">Pilih Jenis Produk</option>
                            <option value="lama">lama</option>
                            <option value="baru">baru</option>
                        </select>
                    </div>
                </div>
            </div>

            {{-- lama --}}
            <div class="row">
                <div class="col-sm-12">
                    <form method="POST" class="form-horizontal hide lama" action="{{ route('transaksi-list-masuk.store') }}">
                        @csrf @method('POST')
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nama Produk</label>
                                    <select class="form-control select2" name="produk_id" required style="width: 100%">
                                        <option value="">Cari Produk</option>
                                        @foreach ($produk as $item)
                                            <option value="{{ $item->id }}">{{ $item->nama_produk }} || QTY : {{ $item->jumlah_stok }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div>
                                    <label>Jumlah</label>
                                    <input type="number" name="qty" value="1" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label class="col-sm-12"><br></label>
                                <button type="submit" class="btn btn-info btn-flat">TAMBAH</button>
                            </div>
                    </form>
                </div>
            </div>

            {{-- baru  --}}
            <div class="row">
                <div class="col-sm-12">
                    <form id="form" method="POST" class="form-horizontal hide baru">
                            @csrf  @method('POST')                          
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Nama Produk</label>
                                    <input type="text" name="nama_produk" class="form-control" value="">
                                    <span class="text-danger namaError"></span>
                                    <br>
                                    <div class="hide data-produk-lama">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div>
                                    <label>Kategori</label>
                                    <select name="id_kategori" class="form-control" required>
                                        <option value="">Pilih</option>
                                        @foreach ($kategori as $item)
                                        <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger kategoriError"></span>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div>
                                    <label>Jumlah</label>
                                    <input type="number" name="qty" class="form-control" value="">
                                    <span class="text-danger jumlahError"></span>
                                </div>
                            </div>

                            <div class="form-group" hidden>
                                <label class="col-sm-3 control-label">Harga Beli</label>
                                <div class="col-sm-9">
                                    <input type="text" name="harga" class="form-control uang" value=0 > 
                                </div>
                            </div>
                            <div class="form-group" hidden>
                                <label class="col-sm-3 control-label">Harga ecer</label>
                                <div class="col-sm-9">
                                    <input type="text" name="harga_ecer" class="form-control uang" value=0>
                                </div>
                            </div>
                            <div class="form-group" hidden>
                                <label class="col-sm-3 control-label">Harga Grosir</label>
                                <div class="col-sm-9">
                                    <input type="text" name="harga_grosir" class="form-control uang" value=0>
                                </div>
                            </div>
                            <div class="form-group" hidden>
                                <label class="col-sm-3 control-label">Harga reseller</label>
                                <div class="col-sm-9">
                                    <input type="text" name="harga_reseller" class="form-control uang" value=0>
                                </div>
                            </div>
                            <div class="form-group" hidden>
                                <label class="col-sm-3 control-label">Harga Partai</label>
                                <div class="col-sm-9">
                                    <input type="text" name="harga_partai" class="form-control uang" value=0>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label class="col-sm-12"><br></label>
                                <button type="button" class="btn btn-info btn-flat" onclick="save()">Tambah</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <div class="box box-default">
        <div class="box-body">
            <div class="col-md-7">
                <table class="table table-borderless">
                    <caption><b>Daftar Belanja</b></caption>
                    <thead>
                        <tr>
                            <th>
                                Kode Produk
                            </th>
                            <th>
                                Nama Produk
                            </th>
                            <th>
                                Jumlah
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @isset($detailmasuk)
                            @foreach ($detailmasuk as $item)
                                <tr>
                                    <th>
                                        {{ baca_produk($item->id_produk)->kode_produk }}
                                    </th>
                                    <th>
                                        {{ baca_produk($item->id_produk)->nama_produk }}
                                    </th>
                                    <th>
                                        {{  number_format($item->jumlah_masuk) }}
                                    </th>
                                </tr>
                            @endforeach
                        @endisset
                    </tbody>
                </table>
            </div>
            <div class="col-md-5">
                <form method="POST" class="form-horizontal" action="{{ route('transaksi-masuk.store') }}">
                    @csrf @method('POST')
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="col-sm-12">Tanggal</label>
                            <input type="date" class="form-control" name="tanggal" style="line-height: normal;" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="col-sm-10">Keterangan</label>
                            <textarea name="keterangan" class="form-control" cols="30" rows="10" required></textarea>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="margin">
                                <div class="btn-group">
                                <button type="submit" class="btn btn-success">Simpan</button>
                                </div>
                                <div class="btn-group">
                                <button type="submit" class="btn btn-danger">Batal</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Modal --}}
    
@endsection

@section('script')
  <script type="text/javascript">
    jQuery(document).ready(function() {
        $('.uang').maskNumber({
            thousands: ".",
            integer: true,
        });
        $('.baru').addClass('hide');
        $('.lama').addClass('hide');

        $('input[name="nama_produk"]').keyup(function(){ 
	        var query = $(this).val();
	        if(query != '')
	        {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('cek-data-produk') }}",
                method:"POST",
                data:{query:query, _token:_token},
                success:function(data){
                    $('.data-produk-lama').removeClass('hide');
                    $('.data-produk-lama').fadeIn();  
                    $('.data-produk-lama').html(data);
                }
                });
	        }
	    });
	    $(document).on('click', 'li', function(){  
	        // $('input[name="nama_produk"]').val($(this).text());  
	        $('.data-produk-lama').fadeOut();  
	    }); 
    });

    function tambah() {
        $('#Modal').modal({
            backdrop: 'static',
            keyboard: false,
        })
        $("#form")[0].reset()
        $('input[name="id"]').val()
        $('input[name="_method"]').val('POST')

    }

    function save() {
        if (confirm('Yakin Simpan Data Ini?') == true) {
            var data = $('#form').serialize()
                var url = '{{ route('transaksi-tambah-produk.store') }}'
            $.post(url, data, function (resp) {
                if (resp.sukses == false) {
                    if (resp.error.nama_produk) {
                        $('.namaError').text(resp.error.nama_produk[0]);
                    }
                    if (resp.error.id_kategori) {
                        $('.kategoriError').text(resp.error.id_kategori[0]);
                    }
                    if (resp.error.jumlah_stok) {
                        $('.jumlahError').text(resp.error.jumlah_stok[0]);
                    }
                }
                if (resp.sukses == true) {
                    $('#form')[0].reset();
                    $('#Modal').modal('hide');
                    Swal({
                        position: 'middle',
                        type: 'success',
                        title: resp.message,
                        showConfirmButton: false,
                        timer: 1500
                    })
                    $('select[name="pilihTransaksi"]').val('');
                    location.reload()
                }
            })
        }
    }
    
    function ribuan(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    function hitung(){
        var total_diskon    = $('input[name="diskon"]').val();
		var total_belanja   = $('input[name="total_belanja"]').val();
        var total_bayar     = $('input[name="total_bayar"]').val();
		var belanja         = parseInt(total_belanja.split('.').join("") );
        var bayar           = parseInt(total_bayar.split('.').join("") );
        var diskon          = parseInt(total_diskon.split('.').join("") );
		var total    	    = belanja - diskon;
        var total_semua     = bayar - total;
		$('input[name="total_kembali"]').val(ribuan(total_semua));
	}

    function pilih() {
            var pilihan = $('select[name="pilihTransaksi"]').val()
            
            if(pilihan == 'lama'){
                $('.baru').addClass('hide');
                $('.lama').removeClass('hide');
            }else if (pilihan == 'baru'){
                $('.baru').removeClass('hide');
                $('.lama').addClass('hide');
            }else{
                $('.baru').addClass('hide');
                $('.lama').addClass('hide');
            }
        }

    </script>
@endsection
