@extends('template')

@section('title')
  Transaksi Stok Opname
@endsection

@section('breadcrumb')
 
@endsection

@section('content')
<section class="content container-fluid">
    <div class="box box-default">
        <div class="box-body">
            <div class="col-md-12">
                @if ($sukses = Session::get('sukses'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ json_encode($sukses)  }}</strong>
                    </div>
                @endif
                @if ($sukses = Session::get('gagal'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ json_encode($sukses)  }}</strong>
                    </div>
                @endif
                <form method="POST" class="form-horizontal" action="{{ url('stok-opname') }}">
                    @csrf @method('POST')
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Kategori</label>
                                <select class="form-control select2" name="id_kategori" required>
                                    @if (isset($kategori))
                                       <option value="">Cari Kategori</option>
                                        @foreach ($kategori as $item)
                                            @if (isset($_POST['id_kategori']) && $_POST['id_kategori'] == $item->id)
                                                <option value="{{ $item->id }}" selected="true">{{ $item->nama }}</option>
                                            @else
                                                <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label class="col-sm-12">&nbsp;&nbsp;</label>
                            <input type="submit" name="lanjut" class="btn btn-info btn-flat" value="TAMPIL">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Stok Opname</h3>
        </div>
        <div class="box-body">
            <div class="col-md-12">
                <form method="POST" action="{{ url('simpan-stok-opname') }}" class="form-horizontal">
                @csrf @method('POST')
                <input type="hidden" class="form-control" name="id_kategori" value="{{ !empty($_POST['id_kategori']) ? $_POST['id_kategori'] :'' }}">
                <input type="hidden" class="form-control" name="tanggal" value="{{ date('Y-m-d') }}">
                <input type="hidden" class="form-control" name="id_periode" value="{{ date('m') }}">
                <input type="hidden" class="form-control" name="tahun" value="{{ date('Y') }}">
                <table class="table table-borderless">
                    <thead>
                        <tr>
                            <th>
                                Kode Produk
                            </th>
                            <th>
                                Nama Produk
                            </th>
                            <th>
                                Stok Tercatat
                            </th>
                            <th>
                                Stok Sebenarnya
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @isset($produk)
                            @foreach ($produk as $item)
                                <tr>
                                    <th>
                                        {{ baca_produk($item->id)->kode_produk }}
                                    </th>
                                    <th>
                                        {{ baca_produk($item->id)->nama_produk }}
                                    </th>
                                    <th>
                                        {{ number_format($item->jumlah_stok) }}
                                    </th>
                                    <th>
                                        <input type="hidden" class="form-control" name="id_produk[]" value="{{ $item->id }}">
                                        <input type="hidden" class="form-control" name="stok_tercatat" value="{{ $item->jumlah_stok }}">
                                        <input type="number" class="form-control" name="stok_sebenarnya[]" value="0">
                                    </th>
                                </tr>
                            @endforeach
                        @endisset
                    </tbody>
                </table>
                <div class="card-footer" style="position: fixed; bottom: 60px; right: 40%">
                    <button type="submit" class="btn btn-success btn-sm" title="Simpan Opname">Selesai & Simpan <i class="fa fa-save"></i></button>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
  <script type="text/javascript">
  </script>
@endsection
