@extends('template')

@section('title')
  Transaksi Kartu Stok
@endsection

@section('breadcrumb')
 
@endsection

@section('content')
<section class="content container-fluid">
    <div class="box box-default">
        <div class="box-body">
            <div class="col-md-12">
                <form method="POST" class="form-horizontal" action="{{ url('kartu-stok') }}">
                    @csrf @method('POST')
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Nama Produk</label>
                                <select class="form-control select2" name="id_produk" required>
                                    @if (isset($produk))
                                       <option value="">Cari Produk</option>
                                        @foreach ($produk as $item)
                                            @if (isset($_POST['id_produk']) && $_POST['id_produk'] == $item->id)
                                                <option value="{{ $item->id }}" selected="true">{{ $item->nama_produk }}</option>
                                            @else
                                                <option value="{{ $item->id }}">{{ $item->nama_produk }}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label>Periode</label>
                            <select class="form-control select2" name="id_periode" required>
                                <option value="">Cari Periode</option>
                                @if (isset($periode))
                                    @foreach ($periode as $item)
                                        @if (empty($_POST['id_periode']) && date('m') == $item->id)
                                            <option value="{{ $item->id }}" selected="true">{{ $item->nama }}</option>
                                        @elseif(isset($_POST['id_periode']) && $_POST['id_periode'] == $item->id)
                                            <option value="{{ $item->id }}" selected="true">{{ $item->nama }}</option>
                                        @else
                                            <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <label class="col-sm-12">&nbsp;&nbsp;</label>
                            <input type="submit" name="lanjut" class="btn btn-info btn-flat" value="TAMPIL">
                            <input type="submit" name="pdf" class="btn btn-warning btn-flat" value="PDF">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Kartu Stok</h3>
        </div>
        <div class="box-body">
            <div class="col-md-12">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>
                                Kode Produk
                            </th>
                            <th>
                                Nama Produk
                            </th>
                            <th>
                                Masuk
                            </th>
                            <th>
                                Keluar
                            </th>
                            <th>
                                Total
                            </th>
                            <th>
                                keterangan
                            </th>
                            <th>
                                Periode
                            </th>
                            <th>
                                Tahun
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @isset($kartu_stok)
                            @foreach ($kartu_stok as $item)
                                <tr>
                                    <th>
                                        {{ baca_produk($item->id_produk)->kode_produk }}
                                    </th>
                                    <th>
                                        {{ baca_produk($item->id_produk)->nama_produk }}
                                    </th>
                                    <th>
                                        {{ number_format($item->masuk) }}
                                    </th>
                                    <th>
                                        {{ number_format($item->keluar) }}
                                    </th>
                                    <th>
                                        {{ number_format($item->total) }}
                                    </th>
                                    <th>
                                        {{ $item->keterangan }}
                                    </th>
                                    <th>
                                        {{ baca_periode( $item->id_periode) }}
                                    </th>
                                    <th>
                                        {{ $item->tahun }}
                                    </th>
                                </tr>
                            @endforeach
                        @endisset
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
  <script type="text/javascript">
  </script>
@endsection
