<!-- Logo -->
<a href="{{ url('/') }}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>{{ config('app.name_pendek') }}</b>{{ config('app.name_belakang_pendek') }}</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>{{ config('app.name') }}</b>{{ config('app.name_belakang') }}</span>
</a>

<!-- Header Navbar -->
<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>
    <div class="navbar-custom-menu" style="float: left;">
      <ul class="nav navbar-nav">
        <li>
          <a href="{{ url('/transaksi-hari-ini') }}" title="Transaksi Hari Ini">
            <i class="fa fa-shopping-cart"></i><span class="hidden-xs"></span>
          </a>
        </li>
        <li class="dropdown tasks-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Pending">
            <i class="fa fa-thumb-tack"></i>
            @if (count_penjualan(Auth::user()->id) == 0)
              <span class="label label-danger">{{ count_penjualan(Auth::user()->id) }}</span>
            @else
              <span class="label label-warning">{{ count_penjualan(Auth::user()->id) }}</span>
            @endif
          </a>
          <ul class="dropdown-menu" style="left: 0;width: -moz-min-content;">
            <li>
              <ul class="menu">
                @foreach(data_penjualan(Auth::user()->id) as $item)
                <li>
                  <a href="{{ url('transaksi/'.$item->faktur) }}">
                    <i class="fa fa-cart-arrow-down"></i><span class="hidden-xs"> {{ $item->faktur }}</span>
                  </a>
                </li>
                @endforeach
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </div>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <!-- The user image in the navbar-->
                    <img src="{{ asset('backend/images/logo.jpeg') }}" class="user-image" alt="{{ config('app.name_full') }}">
                    <!-- hidden-xs hides the username on small devices so only the image appears. -->
                    <span class="hidden-xs">{{ Auth::user()->name }}</span>
                </a>
                <ul class="dropdown-menu">
                    <!-- The user image in the menu -->
                    <li class="user-header">
                        @if (!empty(Auth::user()->foto))
                            <img src="{{ asset('images/users/' . Auth::user()->foto) }}" class="img-circle"
                                alt="User Image">
                        @else
                            <img src="{{ asset('images/users/default.jpg') }}" class="img-circle" alt="User Image">
                        @endif
                        <p>
                            {{ Auth::user()->name }}
                            <small>Aktif sejak {{ Auth::user()->created_at->diffForHumans() }}</small>
                        </p>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="{{-- {{ route('user.show', Auth::user()->uuid) }} --}}" class="btn btn-default btn-flat">Profile</a>
                        </div>
                        <div class="pull-right">
                            <a href="{{ route('logout') }}" class="btn btn-default btn-flat"
                                onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
