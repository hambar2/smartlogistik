<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class temporaryProduk extends Model
{
    protected $fillable = ['kode_entri','kode_produk','id_kategori','nama_produk','jumlah_stok','tanggal_masuk','ukuran','harga','harga_jual','harga_grosir','harga_retail','laba'];
}
