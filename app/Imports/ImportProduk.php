<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use App\Produk;
use App\KartuStok;
use Auth;
use App\temporaryProduk;

class ImportProduk implements ToCollection, WithHeadingRow
{
    use Importable;
    
    public function __construct($id_kategori)
    {
        $this->kategori = $id_kategori;
    }

    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $cek_data = temporaryProduk::where('kode_produk', $row['kode_produk'])->count();
                if ($cek_data == '0') {
                temporaryProduk::create([
                    'kode_entri'    => date('Ymd'),
                    'kode_produk'   => $row['kode_produk'],
                    'id_kategori'   => $this->kategori,
                    'nama_produk'   => $row['nama_produk'],
                    'jumlah_stok'   => $row['jumlah_stok'],
                    'tanggal_masuk' => date('Y-m-d'),
                    'ukuran'        => '-',
                    'harga'         => $row['harga_beli'],
                    'harga_jual'    => $row['harga_jual'],
                    'laba'          => $row['harga_jual']-$row['harga_beli'],
                    'harga_grosir'  => $row['harga_grosir'],
                    'harga_retail'  => $row['harga_retail'],
                ]);
            }
        }
    }

    // public function collection(Collection $rows)
    // {
    //     foreach ($rows as $row) 
    //     {
    //         $cek_produk = Produk::where('kode_produk', $row['kode_produk'])->first();
    //         $cek_count = Produk::where('kode_produk', $row['kode_produk'])->count();

    //         if (empty($cek_produk->jumlah_stok)) {
    //             $total_stok = 0;
    //         }else{
    //             $total_stok = $row['jumlah_stok'] + $cek_produk->jumlah_stok;
    //         }

    //         if ($cek_count > 0) {
    //             Produk::where('kode_produk', $row['kode_produk'])
    //             ->update(['jumlah_stok' => $total_stok]);
    //         } else {
    //             Produk::create([
    //                 // 'kode_entri'    => $kode,
    //                 'kode_produk'   => $row['kode_produk'],
    //                 'id_kategori'   => $this->kategori,
    //                 'nama_produk'   => $row['nama_produk'],
    //                 'jumlah_stok'   => $row['jumlah_stok'],
    //                 'tanggal_masuk' => date('Y-m-d'),
    //                 'ukuran'        => $row['ukuran'],
    //                 'harga'         => $row['harga_beli'],
    //                 'harga_jual'    => $row['harga_jual'],
    //                 'laba'          => $row['harga_jual']-$row['harga_beli'],
    //             ]);
    //         }
            
    //         $produk = Produk::where('kode_produk', $row['kode_produk'])->first();

    //         if (!empty(KartuStok::where('id_produk', $produk->id)->latest()->first()->total)) {
    //             $saldo = KartuStok::where('id_produk', $produk->id)->latest()->first()->total;
    //         } else {
    //             if (Produk::where('id', $produk->id)->count() == 0) {
    //                 $saldo = 0;
    //             } else {
    //                 $saldo = Produk::where('id', $produk->id)->latest()->first()->jumlah_stok;
    //             }
    //         }

    //         KartuStok::create([
    //             'id_produk'  => $produk->id,
    //             'id_periode' => date('m'),
    //             'tahun'      => date('Y'),
    //             'masuk'      => $row['jumlah_stok'],
    //             'keluar'     => '0',
    //             'total'      => $saldo+$row['jumlah_stok'],
    //             'keterangan' => 'Import Data Tanggal '.date('Y-m-d'),
    //             'id_user'    => Auth::user()->id,
    //         ]);
    //     }
    // }

    public function headingRow(): int
    {
        return 1;
    }
}
