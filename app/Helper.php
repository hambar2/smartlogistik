<?php

function set_active($uri) {
	if (is_array($uri)) {
		foreach ($uri as $u) {
			if (Route::is($u)) {
				return 'active';
			}
		}
	} else {
		if (Route::is($uri)) {
			return 'active';
		}
	}
}

if (!function_exists('rupiah')) {
	function rupiah($angka)
	{
		$d = str_replace('.', '', $angka);
		$r = str_replace(',', '', $d);
		return $r;
	}
}

if (!function_exists('valid_date_tanggal')) {
	function valid_date_tanggal($tgl_indo) {
		$t = explode('-', $tgl_indo);
		return $t[2] . '-' . $t[1] . '-' . $t[0];
	}
}

if (!function_exists('valid_date_bulan')) {
	function valid_date_bulan($tgl_indo) {
		$t = explode('-', $tgl_indo);
		return $t[1];
	}
}

if (!function_exists('valid_date_tahun')) {
	function valid_date_tahun($tgl_indo) {
		$t = explode('-', $tgl_indo);
		return $t[0];
	}
}

function randomKode() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 4; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

// function konek DB
if (!function_exists('baca_periode')) {
	function baca_periode($id) {
		if (!empty($id)) {
			return DB::table('periodes')->where('id', '=', $id)->first()->nama;
		} else {
			return '-';
		}
	}
}

if (!function_exists('baca_user')) {
	function baca_user($id) {
		if (!empty($id)) {
			return DB::table('users')->where('id', '=', $id)->first()->name;
		} else {
			return '-';
		}
	}
}

if (!function_exists('baca_produk')) {
	function baca_produk($id) {
		if (!empty($id)) {
			return DB::table('produks')->where('id', '=', $id)->first();
		} else {
			return '';
		}
	}
}

if (!function_exists('baca_produk_kode')) {
	function baca_produk_kode($id) {
		if (!empty($id)) {
			return DB::table('produks')->where('kode_produk', '=', $id)->first();
		} else {
			return '';
		}
	}
}

if (!function_exists('baca_kategori')) {
	function baca_kategori($id) {
		if (!empty($id)) {
			return DB::table('kategoris')->where('id', '=', $id)->first()->nama;
		} else {
			return '';
		}
	}
}

if (!function_exists('baca_pelanggan')) {
	function baca_pelanggan($id) {
		if ($id != 0) {
			return DB::table('pelanggans')->where('id', '=', $id)->first()->nama;
		} else {
			return '-';
		}
	}
}

if (!function_exists('baca_produk_stok')) {
	function baca_produk_stok($id) {
		if (!empty($id)) {
			return DB::table('produks')->where('id', '=', $id)->first()->jumlah_stok;
		} else {
			return '';
		}
	}
}

if (!function_exists('baca_retur_count')) {
	function baca_retur_count($id) {
		if (!empty($id)) {
			return DB::table('returs')->where('faktur', '=', $id)->count();
		} else {
			return '0';
		}
	}
}

if (!function_exists('baca_produk_retur_count')) {
	function baca_produk_retur_count($id,$produk) {
		if (!empty($id) || !empty($produk)) {
			return DB::table('detile_returs')->where('faktur', '=', $id)->where('id_produk', $produk)->count();
		} else {
			return '0';
		}
	}
}

if (!function_exists('baca_retur')) {
	function baca_retur($id) {
		if (!empty($id)) {
			return DB::table('returs')->where('faktur', '=', $id)->first();
		} else {
			return '';
		}
	}
}

if (!function_exists('count_penjualan')) {
	function count_penjualan($id) {
		if (!empty($id)) {
			return DB::table('penjualans')->where('user_id', $id)->where('status', '=', 'pending')->count();
		} else {
			return 0;
		}
	}
}

if (!function_exists('baca_penjualan')) {
	function baca_penjualan($id) {
		if (!empty($id)) {
			return DB::table('penjualans')->where('faktur', $id)->first();
		} else {
			return 0;
		}
	}
}

if (!function_exists('data_penjualan')) {
	function data_penjualan($id) {
		if (!empty($id)) {
			return DB::table('penjualans')->where('user_id', $id)->where('status', '=', 'pending')->get();
		} else {
			return 0;
		}
	}
}

if (!function_exists('data_penjualan_detail_kode')) {
	function data_penjualan_detail_kode($id) {
		if (!empty($id)) {
			return DB::table('detile_penjualans')->where('faktur', $id)->get();
		} else {
			return 0;
		}
	}
}

// pemakaian
if (!function_exists('count_pemakaian')) {
	function count_pemakaian($id) {
		if (!empty($id)) {
			return DB::table('pemakaians')->where('user_id', $id)->where('status', '=', 'pending')->count();
		} else {
			return 0;
		}
	}
}

if (!function_exists('baca_pemakaian')) {
	function baca_pemakaian($id) {
		if (!empty($id)) {
			return DB::table('pemakaians')->where('faktur', $id)->first();
		} else {
			return 0;
		}
	}
}

if (!function_exists('data_pemakaian')) {
	function data_pemakaian($id) {
		if (!empty($id)) {
			return DB::table('pemakaians')->where('user_id', $id)->where('status', '=', 'pending')->get();
		} else {
			return 0;
		}
	}
}

if (!function_exists('data_pemakaian_detail_kode')) {
	function data_pemakaian_detail_kode($id) {
		if (!empty($id)) {
			return DB::table('detail_pemakaians')->where('faktur', $id)->get();
		} else {
			return 0;
		}
	}
}

// pengeluaran
if (!function_exists('baca_pengeluaran_kode')) {
	function baca_pengeluaran_kode($id) {
		if (!empty($id)) {
			return DB::table('pengeluarans')->where('nomor_transaksi', $id)->first();
		} else {
			return 0;
		}
	}
}

if (!function_exists('sum_pengeluaran')) {
	function sum_pengeluaran($id) {
		if (!empty($id)) {
			return DB::table('pengeluarans')->where('nomor_transaksi', $id)->sum('total_pengeluaran');
		} else {
			return 0;
		}
	}
}

// data Penerimaan
if (!function_exists('data_penerimaan')){
	function data_penerimaan($no_masuk,$id_produk){
		if (!empty($no_masuk) || (!empty($id_produk))) {
			return DB::table('penerimaans')->where('no_masuk', $no_masuk)->where('id_produk', $id_produk)->count();
		} else {
			return 0 ;
		}
	}
}