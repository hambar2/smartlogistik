<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class TemplateProduk implements FromCollection, WithHeadings
{
    public function __construct()
    {
    }
    public function headings(): array
    {
        return ["Kode_Produk", "Nama_Produk", "Jumlah_Stok", "Harga_Beli", "Harga_Jual", "Harga_Grosir", "Harga_Retail"];
    }

    public function collection()
    {
        return collect([]);
    }
}
