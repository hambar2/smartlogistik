<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KartuStok extends Model
{
    protected $fillable = ['id_periode','tahun','id_produk','masuk','keluar','total','keterangan','id_user'];
    //
}
