<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PreOrder;
use App\Pelanggan;
use App\Kategori;
use Session;
use Validator;
use Auth;
use DB;

class PreOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['list_transaksi'] = PreOrder::all();
        return view('penjualan.list-pre-order', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['list_transaksi'] = PreOrder::all();
        $data['pelanggan']      = Pelanggan::all();
        $data['kategori']       = Kategori::all();
        return view('penjualan.pre-order', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {  
            $kategori = Kategori::find($request['id_kategori']);
            $kode = 'PO_'.(PreOrder::count() + 1);

            $preorder = New PreOrder();  
            $preorder->kode_pre_order = sprintf("%03s", strtoupper($kode));
            $preorder->id_pelanggan = $request['id_pelanggan'];
            $preorder->id_kategori = $request['id_kategori'];
            $preorder->nama = $request['nama'];
            $preorder->harga = rupiah($request['harga']);
            $preorder->ukuran = $request['ukuran'];
            $preorder->warna = $request['warna'];
            $preorder->keterangan = $request['keterangan'];
            $preorder->user_id = Auth::user()->id;
            $preorder->save();
        });
        Session::flash('sukses','Berhasil Simpan !');
        return redirect('transaksi-pre-order/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['list_transaksi'] = PreOrder::find($id);
        return view('penjualan.detail-pre-order', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
