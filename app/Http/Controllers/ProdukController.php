<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\temporaryProduk;
use App\Kategori;
use App\Exports\TemplateProduk;
use App\Imports\ImportProduk;
use App\KartuStok;
use Session;
use Validator;
use DB;
use PDF;
use Redirect;
use Excel;
use Auth;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['produk'] = Produk::all();
        return view('master.produk', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $kategori = Kategori::all();
        return view('master.produk-tambah', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all(); die;
        $cek = Validator::make($request->all(), [
            'nama_produk' => ['required', 'string', 'unique:produks'],
            'jumlah_stok' => ['required'],
            'harga' => ['required'],
            'harga_ecer' => ['required'],
            'harga_grosir' => ['required'],
            'harga_reseller' => ['required'],
            'harga_partai' => ['required']
          
        ],[
            'nama_produk.required' => 'nama_produk Wajib Diisi !',
            'nama_produk.unique' => 'nama_produk Sudah Ada !',
            'jumlah_stok.required' => 'jumlah_stok Wajib Diisi !',
            'warna.required' => 'ukuran Wajib Diisi !',
            'harga.required' => 'harga Wajib Diisi !',
            'harga_ecer.required' => 'harga ecer Wajib Diisi !',
            'harga_grosir.required' => 'harga grosir Wajib Diisi !',
            'harga_reseller.required' => 'harga reseller Wajib Diisi !',
            'harga_partai.required' => 'harga partai Wajib Diisi !',
        ]);
        if ($cek->fails()) {
            return redirect('produk/create')->withErrors($cek->errors());
        } else {
            DB::transaction(function () use ($request) {
                $kategori = Kategori::find($request['id_kategori']);
                $kode = 'P'.substr($kategori->nama,0,3).'_'.(Produk::count() + 1);

                $produk = New produk();
                $produk->kode_produk = sprintf("%03s", strtoupper($kode));
                $produk->id_kategori = $request['id_kategori'];
                $produk->nama_produk = $request['nama_produk'];
                $produk->jumlah_stok = $request['jumlah_stok'];
                $produk->ukuran = '-';
                $produk->harga = rupiah($request['harga']);
                $produk->harga_jual = rupiah($request['harga_ecer']);
                $produk->harga_grosir = rupiah($request['harga_grosir']);
                $produk->harga_retail = rupiah($request['harga_reseller']);
                $produk->harga_partai = rupiah($request['harga_partai']);
                $produk->laba = '0';
                $produk->tanggal_masuk = date('Y-m-d');
                $produk->save();
            });
            Session::flash('sukses','Berhasil Simpan !');
            return redirect('produk');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = Produk::find($id);
        $kategori = Kategori::all();
        return view('master.produk-edit', compact('produk','kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request->all(); die;
        $cek = Validator::make($request->all(), [
            'nama_produk' => ['required', 'string', 'unique:produks,nama_produk,'.$id],
            'harga' => ['required'],
            'harga_ecer' => ['required'],
            'harga_grosir' => ['required'],
            'harga_retail' => ['required'],
            'harga_partai' => ['required']
        
        ],[
            'nama_produk.required' => 'nama_produk Wajib Diisi !',
            'nama_produk.unique' => 'nama_produk Sudah Ada !',
            'harga.required' => 'harga Wajib Diisi !',
            'harga_ecer.required' => 'harga ecer Wajib Diisi !',
            'harga_grosir.required' => 'harga grosir Wajib Diisi !',
            'harga_retail.required' => 'harga reseller Wajib Diisi !',
            'harga_partai.required' => 'harga partai Wajib Diisi !',
            
        ]);
        if ($cek->fails()) {
            return redirect('produk/'.$id.'/edit')->withErrors($cek->errors());
        } else {
            $produk = Produk::where('id', $request['id'])->first();
            $produk->id_kategori = $request['id_kategori'];
            $produk->nama_produk = $request['nama_produk'];
            $produk->ukuran = '-';
            $produk->harga = rupiah($request['harga']);
            $produk->harga_jual = rupiah($request['harga_ecer']);
            $produk->harga_grosir = rupiah($request['harga_grosir']);
            $produk->harga_retail = rupiah($request['harga_retail']);
            $produk->harga_partai = rupiah($request['harga_partai']);
            $produk->laba = '0';
            $produk->tanggal_masuk = date('Y-m-d');
            $produk->update();
            Session::flash('sukses', 'Berhasil Edit !');
            return redirect('produk');
        }
    }
        
            

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk = Produk::findOrFail($id)->delete();
        return redirect()->route('produk.index')->with('message', 'Data berhasil dihapus!');
    }

    // laporan

    public function laporan()
    {
        $data['detail'] = [];

        // return $data; die;
        return view('laporan.transaksi.barang', $data)->with('no', 1);
    }

    public function laporan_by_waktu(Request $request)
    {
        $abjad = $request['abjad'];

        if (empty($abjad)) {
            $data['detail'] = Produk::all();
        } else {
            $data['detail'] = Produk::where('nama_produk', 'like', $abjad.'%')->get();
        }
        
        if ($request['lanjut']) {
            return view('laporan.transaksi.barang', $data)->with('no', 1);
        } elseif ($request['pdf']) {
            $no = 1;
            $pdf = PDF::loadView('laporan.transaksi.pdf-barang', $data);
            $pdf->setPaper('A4', 'potret');
            return $pdf->stream();
        } elseif ($request['excel']) {
            return Excel::download(new TabunganExport($awal,$akhir), 'laporan harian.xlsx');
        }
    }

    public function view_import()
    {
        $data['kategori'] = Kategori::all();
        $data['list']   = temporaryProduk::all();
        $data['kode_entri']   = temporaryProduk::distinct('kode_entri')
		->latest('kode_entri')
		->get(['kode_entri']);;
        return view('import.produk', $data)->with('no', 1);
    }

    public function template_import(Request $request)
    {
        // if (empty($request->kategori)) {
        //     request()->validate(['kategori' => 'required'], ['kategori.required' => 'Harap Diisi, Jagan Dilewati!']);
        // } else {
            return Excel::download(new TemplateProduk(), 'Format-Import-Produk-'.date('Y-m-d').'.xlsx');
        // }
    }

    public function import(Request $request)
    {
            request()->validate(['id_kategori' => 'required', 'file' => 'required'], ['id_kategori.required' => 'Harap Diisi, Jagan Dilewati!', 'file.required' => 'Harap Diisi, Jagan Dilewati!']);

            $import = Excel::import(new ImportProduk($request->id_kategori), $request->file('file'));

            Session::flash('sukses', 'Import Berhasil !');
            return redirect('view-import-produk');
    }

    public function data_verifikasi_produk(Request $request)
    {
        request()->validate(['kode_entri' => 'required'], ['kode_entri.required' => 'Harap Diisi, Jagan Dilewati!']);

        if ($request['simpan']) {
            DB::transaction(function () use ($request) {
                $data = temporaryProduk::where('kode_entri', $request['kode_entri'])->get();
                foreach ($data as $key => $value) {
                    $cek_material = Produk::where('kode_produk', $value->kode_produk)->count();
                    if ($cek_material == 0) {
                        $produk = new Produk();
                        $produk->kode_produk = $value->kode_produk;
                        $produk->nama_produk = $value->nama_produk;
                        $produk->id_kategori = $value->id_kategori;
                        $produk->jumlah_stok = $value->jumlah_stok;
                        $produk->ukuran = '-';
                        $produk->harga = rupiah($value->harga);
                        $produk->harga_jual = rupiah($value->harga_jual);
                        $produk->laba = rupiah($value->harga_jual)-rupiah($value->harga);
                        $produk->harga_grosir = rupiah($value->harga_grosir);
                        $produk->harga_retail = rupiah($value->harga_retail);
                        $produk->tanggal_masuk = date('Y-m-d');
                        $produk->save();
                    } else {
                        $produk = Produk::where('kode_produk', $value->kode_produk)->first();
                        $produk->jumlah_stok = $produk->jumlah_stok+$value->jumlah_stok;
                        $produk->update();
                    }
                    
        
                    if (!empty(KartuStok::where('id_produk', baca_produk_kode($value->kode_produk)->id)->latest()->first()->total)) {
                        $saldo = KartuStok::where('id_produk', baca_produk_kode($value->kode_produk)->id)->latest()->first()->total;
                    } else {
                        if (Produk::where('id', baca_produk_kode($value->kode_produk)->id)->count() == 0) {
                            $saldo = 0;
                        } else {
                            $saldo = Produk::where('id', baca_produk_kode($value->kode_produk)->id)->latest()->first()->jumlah_stok;
                        }
                    }
        
                    $kartu_stok = new KartuStok();
                    $kartu_stok->id_produk = $produk->id;
                    $kartu_stok->id_periode = date('m');
                    $kartu_stok->tahun = date('Y');
                    $kartu_stok->masuk = $value->jumlah_stok;
                    $kartu_stok->keluar = '0';
                    $kartu_stok->total = $saldo+$value->jumlah_stok;;
                    $kartu_stok->keterangan = 'Import Data Produk Dengan Kode '.$request['kode_entri'];
                    $kartu_stok->id_user = Auth::user()->id;
                    $kartu_stok->save();
                }
                $delete = temporaryProduk::where('kode_entri', $request['kode_entri'])->delete();
            });
            return redirect('produk');
        } elseif ($request['batal']) {
            $delete = temporaryProduk::where('kode_entri', $request['kode_entri'])->delete();
            return redirect('view-import-produk');
        }        
    }

    public function batal_import_produk(Request $request)
    {
        request()->validate(['kode_entri' => 'required'], ['kode_entri.required' => 'Harap Diisi, Jagan Dilewati!']);

        DB::transaction(function () use ($request) {
            $delete = temporaryProduk::where('kode_entri', $request['kode_entri'])->delete();
        });
        return redirect('view-import-produk');
    }

    public function cekData(Request $request)
    {
        if($request->get('query')){
          $query = $request->get('query');
          $data = DB::table('produks')
          ->where('nama_produk', 'LIKE', "%{$query}%")
          ->take(4)
          ->get();
          $output = '<ul class="list-group" style="border-color: #eee0; background-color: #f7ba59 !important;">';
          foreach($data as $row)
          {
           $output .= '
           <li class="list-group-item" style="color: red;background-color: black;"><b>'.$row->nama_produk . ' [ Stok '.$row->jumlah_stok.' ]</b> Data Sudah Ada!!</li>
           ';
          }
          $output .= '</ul>';
          echo $output;
        }
    }

}
