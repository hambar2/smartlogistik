<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengeluaran;
use Session;
use Validator;
use Auth;
use DB;
use PDF;
use Artisan;
use Symfony\Component\Process\Process;

class PengeluaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['pengeluaran'] = Pengeluaran::distinct('nomor_transaksi')
		->latest('nomor_transaksi')
		->get(['nomor_transaksi']);
        return view('pengeluaran.list-transaksi', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['pengeluaran_hari_ini'] = Pengeluaran::where('tanggal', date('Y-m-d'))->get();
        return view('pengeluaran.transaksi', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        DB::transaction(function () use ($request) {
            $faktur = 'PENGELUARAN'.'_'.date('dmY');

            $transaksi = new Pengeluaran();
            $transaksi->nomor_transaksi = $faktur;
            $transaksi->nama_pengeluaran = $request['nama_pengeluaran'];
            $transaksi->tanggal = $request['tanggal'];
            $transaksi->total_pengeluaran = rupiah($request['total_pengeluaran']);
            $transaksi->user_id = Auth::user()->id;
            $transaksi->save();
        });
        Session::flash('sukses', 'Berhasil Simpan !');
        return redirect('transaksi-pengeluaran/create');
    }

    public function show($id)
    {
        $data['pengeluaran'] = Pengeluaran::where('nomor_transaksi', $id)->first();
        $data['detail'] = Pengeluaran::where('nomor_transaksi', $id)->get();
        return view('pengeluaran.detail-transaksi', $data);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $Transaksi = Pengeluaran::findOrFail($id)->delete();
        return redirect()->route('transaksi-pengeluaran.index')->with('message', 'Data berhasil dihapus!');
    }

    // laporan

    public function laporan()
    {
        $data['pengeluaran'] = Pengeluaran::where('created_at', 'LIKE', date('Y-m-d') . '%')
        ->distinct('nomor_transaksi')
		->latest('nomor_transaksi')
		->get(['nomor_transaksi']);

        $data['total_pengeluaran'] = Pengeluaran::where('created_at', 'LIKE', date('Y-m-d') . '%')
        ->distinct('nomor_transaksi')
		->latest('nomor_transaksi')
		->sum('total_pengeluaran');

        // return $data; die;
        return view('laporan.transaksi.transaksi-pengeluaran', $data)->with('no', 1);
    }

    public function laporan_by_waktu(Request $request)
    {
        request()->validate(['tgl_awal' => 'required', 'tgl_akhir' => 'required'], ['tgl_awal.required' => 'Harap Diisi, Jagan Dilewati!', 'tgl_akhir.required' => 'Harap Diisi, Jagan Dilewati!']);

        $awal  = $request['tgl_awal'];
        $akhir = $request['tgl_akhir'];

        $data['pengeluaran'] = Pengeluaran::whereBetween('created_at', [date('' . $awal . ' 00:00:00'), date('' . $akhir . ' 23:59:00')])
        ->distinct('nomor_transaksi')
		->latest('nomor_transaksi')
		->get(['nomor_transaksi']);

        $data['total_pengeluaran'] = Pengeluaran::whereBetween('created_at', [date('' . $awal . ' 00:00:00'), date('' . $akhir . ' 23:59:00')])
        ->distinct('nomor_transaksi')
		->latest('nomor_transaksi')
		->sum('total_pengeluaran');

        if ($request['lanjut']) {
            return view('laporan.transaksi.transaksi-pengeluaran', $data)->with('no', 1);
        } elseif ($request['pdf']) {
            $no = 1;
            $pdf = PDF::loadView('laporan.transaksi.pdf-transaksi-pengeluaran-hari-ini', $data);
            $pdf->setPaper('A4', 'potret');
            return $pdf->stream();
        } elseif ($request['excel']) {
            return Excel::download(new TabunganExport($awal,$akhir), 'laporan harian.xlsx');
        }
    }
}
