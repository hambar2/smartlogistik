<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DetailPemakaian;
use App\Pemakaian;
use App\Produk;
use Session;
use Validator;
use Auth;
use DB;
use PDF;

class DetailPemakaianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list_transaksi = Pemakaian::all();
        // return $list_transaksi die;
        return view('pemakaian.list-transaksi', compact('list_transaksi'));
    }

    public function index_hari_ini()
    {
        $data['transaksi_total_belanja'] = Pemakaian::whereBetween('created_at', [date('Y/m/d 00:00:00'), date('Y/m/d 23:59:00')])
        ->where('status', 'lunas')
        ->where('user_id', Auth::user()->id)
        ->sum('total_belanja');

        $data['transaksi_total_diskon'] = Pemakaian::whereBetween('created_at', [date('Y/m/d 00:00:00'), date('Y/m/d 23:59:00')])
        ->where('status', 'lunas')
        ->where('user_id', Auth::user()->id)
        ->sum('diskon');

        $data['list_transaksi'] = Pemakaian::whereBetween('created_at', [date('Y/m/d 00:00:00'), date('Y/m/d 23:59:00')])
        ->where('status', 'lunas')
        ->where('user_id', Auth::user()->id)
        ->get();
        // return $data; die;
        return view('pemakaian.list-transaksi-hari-ini', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $data['detail'] = DetailPemakaian::where('faktur', $id)->get();
        $data['total_pemakaian'] = DetailPemakaian::where('faktur', $id)->sum('sub_total');
        $data['pemakaian'] = Pemakaian::where('faktur', $id)->first();
        // return $data; die;
        return view('pemakaian.detail-transaksi', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cetak_transaksi_hari_ini(Request $request)
    {
        $data['transaksi_total_belanja'] = Pemakaian::whereBetween('created_at', [date('Y/m/d 00:00:00'), date('Y/m/d 23:59:00')])
        ->where('status', 'lunas')
        ->where('user_id', Auth::user()->id)
        ->sum('total_belanja');

        $data['transaksi_total_diskon'] = Pemakaian::whereBetween('created_at', [date('Y/m/d 00:00:00'), date('Y/m/d 23:59:00')])
        ->where('status', 'lunas')
        ->where('user_id', Auth::user()->id)
        ->sum('diskon');

        $data['list_transaksi'] = Pemakaian::whereBetween('created_at', [date('Y/m/d 00:00:00'), date('Y/m/d 23:59:00')])
        ->where('status', 'lunas')
        ->where('user_id', Auth::user()->id)
        ->get();

        $data['no'] = 1;
        $pdf = PDF::loadView('laporan.transaksi.pdf-transaksi-hari-ini', $data);
        $pdf->setPaper('A4', 'portrait');
        return $pdf->stream();
    }
}

