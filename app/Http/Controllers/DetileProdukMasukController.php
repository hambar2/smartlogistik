<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DetileProdukMasuk;
use App\ProdukMasuk;
use App\KartuStok;
use App\Produk;
use Session;
use Validator;
use Auth;
use DB;
use PDF;

class DetileProdukMasukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list_transaksi_masuk = ProdukMasuk::all();
        return view('logistik.list-transaksi-masuk', compact('list_transaksi_masuk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {                 
            $produk = Produk::find($request['produk_id']);
            $faktur = 'MASUK'.'_'.date('dmY').(ProdukMasuk::count() + 1);
            $cek    = DetileProdukMasuk::where('no_masuk', session('kode_masuk'))
            ->where('id_produk', $request['produk_id'])
            ->count();

            if (!session('kode_masuk')) {
                session(['kode_masuk' => $faktur]);
            }
            
            if ($cek > 0) {
                $transaksi = DetileProdukMasuk::where('no_masuk', session('kode_masuk'))
                ->where('id_produk', $request['produk_id'])->first();  
                $transaksi->jumlah_masuk = $request['qty']+$transaksi->jumlah_masuk;
                $transaksi->update();
            } else {
                $transaksi = New DetileProdukMasuk();  
                $transaksi->no_masuk = session('kode_masuk');
                $transaksi->id_produk = $request['produk_id'];
                $transaksi->kode_produk = $produk->kode_produk;
                $transaksi->nama_produk = $produk->nama_produk;
                $transaksi->jumlah_masuk = $request['qty'];
                $transaksi->save();
            }
        });
        Session::flash('sukses','Berhasil Simpan !');
        return redirect('transaksi-masuk');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return $id; die;
        $data['detail'] = DetileProdukMasuk::where('no_masuk', $id)->get();
        $data['total_masuk'] = DetileProdukMasuk::where('no_masuk', $id)->sum('jumlah_masuk');
        // return $data; die;
        return view('logistik.detail-transaksi-masuk', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function laporan()
    {
        $data['masuk'] = DetileProdukMasuk::where('created_at', 'LIKE', date('Y-m-d') . '%')->get();

        return view('laporan.logistik.laporan-barang-masuk', $data)->with('no', 1);
    }

    public function laporan_by_tanggal(Request $request)
    {
        request()->validate(['tgl_awal' => 'required', 'tgl_akhir' => 'required'], ['tgl_awal.required' => 'Harap Diisi, Jagan Dilewati!', 'tgl_akhir.required' => 'Harap Diisi, Jagan Dilewati!']);

        $awal  = $request['tgl_awal'];
        $akhir = $request['tgl_akhir'];

        $data['masuk'] = DetileProdukMasuk::whereBetween('created_at', [date('' . $awal . ' 00:00:00'), date('' . $akhir . ' 23:59:00')])
        ->latest()
        ->get();

        if ($request['lanjut']) {
            // return [date('' . $awal . ' 00:00:00'), date('' . $akhir . ' 23:59:00')]; die;
            // return $data; die;
            return view('laporan.logistik.laporan-barang-masuk', $data)->with('no', 1);
        } elseif ($request['pdf']) {
            $no = 1;
            $pdf = PDF::loadView('laporan.logistik.pdf-laporan-barang-masuk', $data);
            $pdf->setPaper('A4', 'potret');
            return $pdf->stream();
        } elseif ($request['excel']) {
            return Excel::download(new TabunganExport($awal,$akhir), 'laporan harian.xlsx');
        }
    }
}