<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProdukMasuk;
use App\Penerimaan;
use App\DetileProdukMasuk;
use App\Produk;
use App\Kategori;
use App\KartuStok;
use Session;
use Validator;
use Auth;
use DB;
use PDF;
use Artisan;


class PenerimaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['list_produk'] = ProdukMasuk::all();
        return view('penerimaan.list-penerimaan', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cek = Validator::make($request->all(), [
            'nama_produk' => ['unique:penerimaans'],
            'id_kategori' => ['required'],
        ],[
            'nama_produk.unique' => 'Produk Sudah di inputkan !',
            'id_kategori.required' => 'Kategori Wajib di pilih !',
        ]);
        if ($cek->fails()) {
            return response()->json(['sukses' => false, 'error' => $cek->errors()]);
        
        } else { 
            DB::transaction(function () use ($request) {     
            
            $penerimaan = New Penerimaan();  
            $penerimaan->no_masuk        = $request['kode_masuk'];
            $penerimaan->id_produk       = $request['id_produk'];
            $penerimaan->nama_produk     = $request['nama_produk'];
            $penerimaan->id_kategori     = $request['id_kategori'];
            $penerimaan->harga_beli      = rupiah($request['harga_beli']);
            $penerimaan->harga_jual      = rupiah($request['harga_jual']);
            $penerimaan->harga_grosir    = rupiah($request['harga_grosir']);
            $penerimaan->harga_retail    = rupiah($request['harga_retail']);
            $penerimaan->harga_partai    = rupiah($request['harga_partai']);
            $penerimaan->jumlah_masuk    = $request['qty'];
            $penerimaan->tanggal         = $request['tanggal'];
            $penerimaan->id_periode      = $request['id_periode'];
            $penerimaan->tahun           = date('Y');
            $penerimaan->id_user         = Auth::user()->id;
            $penerimaan->status_terima   = 'sudah';
            $penerimaan->save();

            $produk = Produk::where('id', $request['id_produk'])->first();
            $produk->kode_produk = $request['kode_produk'];
            $produk->id_kategori = $request['id_kategori'];
            $produk->nama_produk = $request['nama_produk'];
            $produk->jumlah_stok = $request['qty'];
            $produk->tanggal_masuk = $request['tanggal'];
            $produk->ukuran = '-';
            $produk->harga = $request['harga_beli'];
            $produk->harga_jual = $request['harga_jual'];
            $produk->harga_grosir = $request['harga_grosir'];
            $produk->harga_retail = $request['harga_retail'];
            $produk->harga_partai = $request['harga_partai'];
            $produk->laba = 0 ;
            $produk->update();

            $kartu = New KartuStok();
            $kartu->id_periode = $request['id_periode'];
            $kartu->tahun = date('Y');
            $kartu->id_produk = $request['id_produk'];
            $kartu->masuk = $request['jumlah_masuk'];
            $kartu->keluar = 0 ;
            $kartu->total = 0 ;
            $kartu->keterangan = 'Penerimaan dengan kode masuk =>'.$request['kode_masuk'].'';
            $kartu->id_user = Auth::user()->id;
            $kartu->save();

            $detile = DetileProdukMasuk::where('id_produk' , $request['id_produk'])->first();
            $detile->status_terima = 'sudah';
            $detile->update();

            

            
            });
            return response()->json(['sukses' => true]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return $id; die;
        $data['produk_masuk'] = ProdukMasuk::where('no_masuk',$id)->get();
        $data['detail'] = DetileProdukMasuk::where('no_masuk', $id)->get();
        $data['total_masuk'] = DetileProdukMasuk::where('no_masuk', $id)->sum('jumlah_masuk');
        $data['kategori'] = Kategori::all();
        // return $data; die;
        return view('penerimaan.detile-list-penerimaan', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = DetileProdukMasuk::find($id);
        return response()->json(['detail' => $detail]);
    }

    public function detailBeritaAcara($id , $nomer_masuk)
    {
        $detail = DetileProdukMasuk::find($id);
        return response()->json(['detail' => $detail]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function laporan()
    {
        $data['penerimaan'] = Penerimaan::where('created_at', 'LIKE', date('Y-m-d') . '%')->get();

        return view('laporan.logistik.laporan-penerimaan', $data)->with('no', 1);
    }

    public function laporan_by_tanggal(Request $request)
    {
        request()->validate(['tgl_awal' => 'required', 'tgl_akhir' => 'required'], ['tgl_awal.required' => 'Harap Diisi, Jagan Dilewati!', 'tgl_akhir.required' => 'Harap Diisi, Jagan Dilewati!']);

        $awal  = $request['tgl_awal'];
        $akhir = $request['tgl_akhir'];

        $data['penerimaan'] = Penerimaan::whereBetween('created_at', [date('' . $awal . ' 00:00:00'), date('' . $akhir . ' 23:59:00')])
        ->latest()
        ->get();

        if ($request['lanjut']) {
            // return [date('' . $awal . ' 00:00:00'), date('' . $akhir . ' 23:59:00')]; die;
            // return $data; die;
            return view('laporan.logistik.laporan-penerimaan', $data)->with('no', 1);
        } elseif ($request['pdf']) {
            $no = 1;
            $pdf = PDF::loadView('laporan.logistik.pdf-laporan-penerimaan', $data);
            $pdf->setPaper('A4', 'potret');
            return $pdf->stream();
        } elseif ($request['excel']) {
            return Excel::download(new TabunganExport($awal,$akhir), 'laporan harian.xlsx');
        }
    }



}