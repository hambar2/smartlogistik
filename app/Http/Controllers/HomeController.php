<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\Kategori;
use App\Penjualan;
use App\User;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['produk'] = Produk::count();
        $data['kategori'] = Kategori::count();
        $data['penjualan'] = Penjualan::count();
        $data['user'] = User::count();
        return view('dashboard',$data);
    }
}
