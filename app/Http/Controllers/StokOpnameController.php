<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StokOpname;
use App\KartuStok;
use App\Produk;
use App\Kategori;
use Session;
use Validator;
use Auth;
use DB;
use PDF;


class StokOpnameController extends Controller
{
    public function stok_opname()
    {
        $data['kategori'] = Kategori::all();
        return view('logistik.stok-opname', $data);
    }

    public function stok_opname_by_index(Request $request)
    {
        $data['kategori'] = Kategori::all();

        $id_kategori  = $request['id_kategori'];
        $data['produk'] = Produk::where('id_kategori', $id_kategori)->get();

        if ($request['lanjut']) {
            return view('logistik.stok-opname', $data);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all(); die;

        $user_id            = Auth::user()->id;
        $tanggal            = $request->tanggal;
        $periode            = $request->id_periode;
        $tahun              = $request->tahun;
        $id_produk          = $request->id_produk;
        $stok_sebenarnya    = $request->stok_sebenarnya;
        $id_kategori        = $request->id_kategori;

        if(isset($id_kategori)){
            DB::transaction(function () use ($user_id, $tanggal, $periode, $tahun, $id_produk, $stok_sebenarnya, $id_kategori) {
                for($a = 0; $a < count($id_produk); $a++){
                    if(isset($stok_sebenarnya[$a]) != ''){
                        $opname_id = 0;
                        $count = StokOpname::count();
                        $cek = StokOpname::where('id_periode', $periode)
                        ->where('tahun', $tahun)
                        ->where('id_produk', $id_produk[$a])
                        ->first();
                        if($cek){
                            $opname = StokOpname::find($cek->id);
                            $opname->stok_tercatat         = $stok_sebenarnya[$a];
                            $opname->stok_sebenarnya       = $stok_sebenarnya[$a];
                            $opname->update();
                            $opname_id = $cek->id;
                        }else{
                            $obat = Produk::where('id', $id_produk[$a])->first();
                            $saveopname = new StokOpname();
                            $saveopname->id_produk        = $id_produk[$a];
                            $saveopname->id_kategori      = baca_produk($id_produk[$a])->id_kategori;
                            $saveopname->kode_produk      = baca_produk($id_produk[$a])->kode_produk;
                            $saveopname->nama_produk      = baca_produk($id_produk[$a])->nama_produk;
                            $saveopname->tanggal          = $tanggal;
                            $saveopname->id_periode       = $periode;
                            $saveopname->tahun            = $tahun;
                            $saveopname->stok_tercatat    = $stok_sebenarnya[$a];
                            $saveopname->stok_sebenarnya  = $stok_sebenarnya[$a];
                            $saveopname->keterangan       = 'Opname, '.$tanggal;
                            $saveopname->user_id          = $user_id;
                            $saveopname->save();
                            $opname_id = $saveopname->id;
                        }

                        $obat = Produk::where('id', $id_produk[$a])->first();
                        $obat->jumlah_stok = $stok_sebenarnya[$a];
                        $obat->update();

                        $kartu = New KartuStok();  
                        $kartu->id_produk       = $id_produk[$a];
                        $kartu->id_periode      = $periode;
                        $kartu->tahun           = $tahun;
                        $kartu->masuk           = '0';
                        $kartu->keluar          = '0';
                        $kartu->total           = $stok_sebenarnya[$a];
                        $kartu->keterangan      = 'Opname, '.$tanggal;
                        $kartu->id_user         = Auth::user()->id;
                        $kartu->save();
                    }
                }
            });
            Session::flash('sukses','Periode Opname Berhasil Simpan !');
        }else{
            Session::flash('gagal','Filter Kategori Belum Dipilih !');
        }
        return redirect('stok-opname');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function laporan()
    {
        $data['opname'] = StokOpname::where('created_at', 'LIKE', date('Y-m-d') . '%')->get();

        return view('laporan.logistik.laporan-opname', $data)->with('no', 1);
    }

    public function laporan_by_tanggal(Request $request)
    {
        request()->validate(['tgl_awal' => 'required', 'tgl_akhir' => 'required'], ['tgl_awal.required' => 'Harap Diisi, Jagan Dilewati!', 'tgl_akhir.required' => 'Harap Diisi, Jagan Dilewati!']);

        $awal  = $request['tgl_awal'];
        $akhir = $request['tgl_akhir'];

        $data['opname'] = StokOpname::whereBetween('created_at', [date('' . $awal . ' 00:00:00'), date('' . $akhir . ' 23:59:00')])
        ->latest()
        ->get();

        if ($request['lanjut']) {
            // return [date('' . $awal . ' 00:00:00'), date('' . $akhir . ' 23:59:00')]; die;
            // return $data; die;
            return view('laporan.logistik.laporan-opname', $data)->with('no', 1);
        } elseif ($request['pdf']) {
            $no = 1;
            $pdf = PDF::loadView('laporan.logistik.pdf-laporan-opname', $data);
            $pdf->setPaper('A4', 'potret');
            return $pdf->stream();
        } elseif ($request['excel']) {
            return Excel::download(new TabunganExport($awal,$akhir), 'laporan harian.xlsx');
        }
    }
}