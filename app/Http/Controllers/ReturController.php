<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Retur;
use App\DetileRetur;
use App\DetilePenjualan;
use App\KartuStok;
use App\Produk;
use App\Penjualan;
use App\Pelanggan;
use Session;
use Validator;
use Auth;
use DB;

class ReturController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['list_transaksi'] = Retur::all();
        return view('penjualan.list-retur', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {                 
            $produk = Produk::find($request['produk_id']);
            $faktur = 'RETUR'.'_'.date('dmY').(Retur::count() + 1);
            $cek    = DetileRetur::where('kode_retur', session('kode_retur'))
            ->where('id_produk', $request['produk_id'])
            ->count();

            if (!session('kode_retur')) {
                session(['kode_retur' => $faktur]);
            }

            $dataproduk = array();
            foreach($request['id_produk'] as $id_produk){
                $produk = Produk::where('id', $id_produk)->first();

                $detail = New DetileRetur();  
                $detail->kode_retur = session('kode_retur');
                $detail->faktur = $request['faktur'];
                $detail->id_produk = $id_produk;
                $detail->kode_produk = $produk->kode_produk;
                $detail->nama_produk = $produk->nama_produk;
                $detail->harga_jual = $request['harga_jual'];
                $detail->jumlah = $request['jumlah'];
                $detail->sub_total = $request['harga_jual']*$request['jumlah'];
                $detail->save();

                if (!empty(KartuStok::where('id_produk', $id_produk)->latest()->first()->total)) {
                    $saldo = KartuStok::where('id_produk', $id_produk)->latest()->first()->total;
                } else {
                    $saldo = Produk::where('id', $id_produk)->latest()->first()->jumlah_stok;
                }
                
                $kartu = New KartuStok();  
                $kartu->id_produk       = $id_produk;
                $kartu->id_periode      = date('m');
                $kartu->tahun           = date('Y');
                $kartu->masuk           = $request['jumlah'];
                $kartu->keluar          = '0';
                $kartu->total           = $saldo - $request['jumlah'];
                $kartu->keterangan      = 'Retur Barang';
                $kartu->id_user         = Auth::user()->id;
                $kartu->save();
            }

            $retur = New Retur();  
            $retur->jenis_retur = $request['jenis_retur'];
            $retur->faktur = $request['faktur'];
            $retur->kode_retur = session('kode_retur');
            $retur->tanggal = $request['tanggal'];
            $retur->id_user = Auth::user()->id;
            $retur->id_pelanggan = !empty($request['id_pelanggan']) ? $request['id_pelanggan'] :'0';
            $retur->keterangan = $request['keterangan'];
            $retur->save();
            
        });
        Session::flash('sukses','Berhasil Simpan !');
        return redirect('transaksi-retur');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['detail'] = DetilePenjualan::where('faktur', $id)->get();
        $data['total_penjualan'] = DetilePenjualan::where('faktur', $id)->sum('sub_total');
        $data['penjualan'] = Penjualan::where('faktur', $id)->first();
        $data['pelanggan'] = Pelanggan::all();
        // return $data; die;
        return view('penjualan.retur', $data);
    }

    public function detail($id)
    {
        $data['detail'] = DetileRetur::where('kode_retur', $id)->get();
        $data['retur'] = Retur::where('kode_retur', $id)->first();
        return view('penjualan.detail-retur', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
