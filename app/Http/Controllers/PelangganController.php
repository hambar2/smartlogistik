<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pelanggan;
use Session;
use Validator;

class PelangganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pelanggan = Pelanggan::all();
        return view('master.pelanggan', compact('pelanggan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master.pelanggan-tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all(); die;
        $cek = Validator::make($request->all(), [
            'email' => ['required', 'unique:pelanggans'],
            'nomortelepon' => ['required', 'unique:pelanggans'],
            
            'email.required' => 'Email Wajib Diisi !',
            'email.unique' => 'Email Sudah Ada !',
            
            'nomortelepon.required' => 'Nomor Telepon Wajib Diisi !',
            'nomortelepon.unique' => 'Nomor Telepon Sudah Ada !',
            
           
        ]);
        if ($cek->fails()) {
            Session::flash('gagal', 'Gagal Simpan !');
            return redirect('pelanggan/create');
        } else {
            $kode = (Pelanggan::count() + 1);
            $pelanggan = New pelanggan();
            $pelanggan->kode_pelanggan = sprintf("%05s", strtoupper($kode));
            $pelanggan->nama = $request['nama'];
            $pelanggan->alamat = $request['alamat'];
            $pelanggan->email = $request['email'];
            $pelanggan->nomortelepon = $request['nomortelepon'];
            
            $pelanggan->save();
            Session::flash('sukses','Berhasil Simpan !');
            return redirect('pelanggan');
        
        Session::flash('sukses','Berhasil Simpan !');
        return redirect('pelanggan');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pelanggan = Pelanggan::find($id);
        return view('master.pelanggan-edit', compact('pelanggan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cek = Validator::make($request->all(), [
            'nama' => ['required', 'unique:pelanggans,nama,'.$id],
            'alamat' => ['required', 'unique:pelanggans,alamat,'.$id],
            'email' => ['required','unique:pelanggans,email,'.$id],
            'nomortelepon' => ['required', 'unique:pelanggans,nomortelepon,'.$id],
            
        ],[
            'kode_pelanggan.unique' => 'kode_pelanggan Sudah Ada !',
            'nama.required' => 'nama Wajib Diisi !',
            'nama.unique' => 'nama Sudah Ada !',
            'alamat.required' => 'alamat Wajib Diisi !',
            'alamat.unique' => 'alamat Sudah Ada !',
            'email.required' => 'Email Wajib Diisi !',
            'email.unique' => 'Email Sudah Ada !',
            'nomortelepon.required' => 'Nomor Telepon Wajib Diisi !',
            'nomortelepon.unique' => 'Nomor Telepon Sudah Ada !',
            
           
        ]);
        if ($cek->fails()) {
            Session::flash('gagal', $cek->errors());
            return redirect('pelanggan/'.$id.'/edit');
        } else {
            $pelanggan = Pelanggan::where('id', $request['id'])->first();
            $pelanggan->nama = $request['nama'];
            $pelanggan->alamat = $request['alamat'];
            $pelanggan->email = $request['email'];
            $pelanggan->nomortelepon = $request['nomortelepon'];
            
            $pelanggan->update();
            Session::flash('sukses', 'Berhasil Edit !');
            return redirect('pelanggan');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pelanggan = Pelanggan::findOrFail($id)->delete();
        return redirect()->route('pelanggan.index')->with('message', 'Data berhasil dihapus!');
    }

    public function get_diskon($id)
    {
       $pelanggan = Pelanggan::find($id);
        return response()->json(['pelanggan' => $pelanggan]);
    }

    public function laporan()
    {
        $data['pelanggan'] = Pelanggan::where('created_at', 'LIKE', date('Y-m-d') . '%')->get();

        return view('laporan.transaksi.laporan-pelanggan', $data)->with('no', 1);
    }

    public function laporan_by_tanggal(Request $request)
    {
        request()->validate(['tgl_awal' => 'required', 'tgl_akhir' => 'required'], ['tgl_awal.required' => 'Harap Diisi, Jagan Dilewati!', 'tgl_akhir.required' => 'Harap Diisi, Jagan Dilewati!']);

        $awal  = $request['tgl_awal'];
        $akhir = $request['tgl_akhir'];

        $data['pelanggan'] = Pelanggan::whereBetween('created_at', [date('' . $awal . ' 00:00:00'), date('' . $akhir . ' 23:59:00')])
        ->latest()
        ->get();

        if ($request['lanjut']) {
            // return [date('' . $awal . ' 00:00:00'), date('' . $akhir . ' 23:59:00')]; die;
            // return $data; die;
            return view('laporan.transaksi.laporan-pelanggan', $data)->with('no', 1);
        } elseif ($request['pdf']) {
            $no = 1;
            $pdf = PDF::loadView('laporan.transaksi.pdf-laporan-pelanggan', $data);
            $pdf->setPaper('A4', 'potret');
            return $pdf->stream();
        } elseif ($request['excel']) {
            return Excel::download(new TabunganExport($awal,$akhir), 'laporan harian.xlsx');
        }
    }
}