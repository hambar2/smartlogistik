<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gudang;
use Session;
use Validator;
use DataTables;

class GudangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $gudang = Gudang::latest();
            return DataTables::of($gudang)
                ->addIndexColumn()
                ->addColumn('aksi', function ($gudang) {
                    return '<button type="button" onclick="edit(\'' . $gudang->id . '\')" class="btn btn-sm btn-success btn-flat" title="Edit"><i class="fa fa-edit"></i></button>
                    ';

                })
                ->rawColumns(['aksi'])
                ->make(true);
        }
        return view('master.gudang');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all(); die;
        $cek = Validator::make($request->all(), [
            'kode' => ['required', 'unique:gudangs'],
            'nama' => ['required', 'unique:gudangs'],
        ],[
            'kode.required' => 'Kode Wajib Diisi !',
            'kode.unique' => 'Kode Sudah Ada !',
            'nama.required' => 'Nama Wajib Diisi !',
            'nama.unique' => 'Nama Sudah Ada !',
        ]);
        if ($cek->fails()) {
            return response()->json(['sukses' => false, 'error' => $cek->errors()]);
        } else {
            $gudang = New Gudang();
            $gudang->kode = $request['kode'];
            $gudang->nama = $request['nama'];
            $gudang->save();
            return response()->json(['sukses' => true, 'message' => 'Berhasil Simpan']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gudang = Gudang::find($id);
        return response()->json(['gudang' => $gudang]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cek = Validator::make($request->all(), [
            'kode' => ['required', 'unique:gudangs,kode,'.$id],
            'nama' => ['required', 'unique:gudangs,nama,'.$id],
        ],[
            'kode.required' => 'Kode Wajib Diisi !',
            'kode.unique' => 'Kode Sudah Ada !',
            'nama.required' => 'Nama Wajib Diisi !',
            'nama.unique' => 'Nama Sudah Ada !',
        ]);
        if ($cek->fails()) {
            return response()->json(['sukses' => false, 'error' => $cek->errors()]);
        } else {
            $gudang = Gudang::where('id', $request['id'])->first();
            $gudang->kode = $request['kode'];
            $gudang->nama = $request['nama'];
            $gudang->update();
            return response()->json(['sukses' => true, 'message' => 'Berhasil Simpan']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}