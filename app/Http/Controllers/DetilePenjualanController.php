<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DetilePenjualan;
use App\Penjualan;
use App\Produk;
use Session;
use Validator;
use Auth;
use DB;
use PDF;

class DetilePenjualanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list_transaksi = Penjualan::all();
        // return $list_transaksi die;
        return view('penjualan.list-transaksi', compact('list_transaksi'));
    }

    public function index_hari_ini()
    {
        $data['transaksi_total_belanja'] = Penjualan::whereBetween('created_at', [date('Y/m/d 00:00:00'), date('Y/m/d 23:59:00')])
        ->where('status', 'lunas')
        ->where('user_id', Auth::user()->id)
        ->sum('total_belanja');

        $data['transaksi_total_diskon'] = Penjualan::whereBetween('created_at', [date('Y/m/d 00:00:00'), date('Y/m/d 23:59:00')])
        ->where('status', 'lunas')
        ->where('user_id', Auth::user()->id)
        ->sum('diskon');

        $data['transaksi_total_retur'] = '0';

        $data['list_transaksi'] = Penjualan::whereBetween('created_at', [date('Y/m/d 00:00:00'), date('Y/m/d 23:59:00')])
        ->where('status', 'lunas')
        ->where('user_id', Auth::user()->id)
        ->get();
        // return $data; die;
        return view('penjualan.list-transaksi-hari-ini', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $data['detail'] = DetilePenjualan::where('faktur', $id)->get();
        $data['total_penjualan'] = DetilePenjualan::where('faktur', $id)->sum('sub_total');
        $data['penjualan'] = Penjualan::where('faktur', $id)->first();
        // return $data; die;
        return view('penjualan.detail-transaksi', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (DetilePenjualan::where('id', $id)->count() == '0') {
            return response()->json(['sukses' => false, 'pesan' => 'Gagal hapus data !']);
        } else {
            $detail = DetilePenjualan::find($id);
            $detail->delete();
    
            session()->forget('kode_faktur');
            return response()->json(['sukses' => true, 'pesan' => 'Berhasil hapus data !']);
        }
        
    }

    public function updateJumlah(Request $request)
    {
        $order = DetilePenjualan::where('id', $request['id'])->first();
        $produk = Produk::where('id', $order->id_produk)->first();
        if ($request['jumlah'] > $produk->jumlah_stok) {
            $order->jumlah = '1';
            $order->sub_total = ($produk->harga_jual*1);
            $order->update();
            return response()->json(['sukses' => false, 'pesan' => "Stoknya Tinggal $produk->jumlah_stok"]);
        } else {
            $jumlah = $request['jumlah'];

            if($jumlah >= 10 && $jumlah <= 20){
                $harga_jual = $produk->harga_grosir;
            }else if($jumlah >= 4 && $jumlah <= 10){
                $harga_jual = $produk->harga_retail;
            }else if($jumlah >= 1 && $jumlah <=3){
                $harga_jual = $produk->harga_jual;
            }else if($jumlah >= 20){
                $harga_jual = $produk->harga_partai;
            }

            $order->jumlah = $request['jumlah'];
            $order->sub_total = ($harga_jual*$request['jumlah']);
            $order->update();
            return response()->json(['sukses' => true, 'pesan' => 'Berhasil perubahan jumlah !']);
        }
    }

    public function cetak_transaksi_hari_ini(Request $request)
    {
        $data['transaksi_total_belanja'] = Penjualan::whereBetween('created_at', [date('Y/m/d 00:00:00'), date('Y/m/d 23:59:00')])
        ->where('status', 'lunas')
        ->where('user_id', Auth::user()->id)
        ->sum('total_belanja');

        $data['transaksi_total_diskon'] = Penjualan::whereBetween('created_at', [date('Y/m/d 00:00:00'), date('Y/m/d 23:59:00')])
        ->where('status', 'lunas')
        ->where('user_id', Auth::user()->id)
        ->sum('diskon');

        $data['list_transaksi'] = Penjualan::whereBetween('created_at', [date('Y/m/d 00:00:00'), date('Y/m/d 23:59:00')])
        ->where('status', 'lunas')
        ->where('user_id', Auth::user()->id)
        ->get();

        $data['no'] = 1;
        $pdf = PDF::loadView('laporan.transaksi.pdf-transaksi-hari-ini', $data);
        $pdf->setPaper('A4', 'portrait');
        return $pdf->stream();
    }

    public function laporan()
    {
        // $data['penjualan'] = Penjualan::where('created_at', 'LIKE', date('Y-m-d') . '%')->get();
        return view('laporan.transaksi.laporan-penjualan')->with('no', 1);
    }

    public function laporan_by_tanggal(Request $request)
    {
        request()->validate(['tgl_awal' => 'required', 'tgl_akhir' => 'required'], ['tgl_awal.required' => 'Harap Diisi, Jagan Dilewati!', 'tgl_akhir.required' => 'Harap Diisi, Jagan Dilewati!']);

        $awal  = $request['tgl_awal'];
        $akhir = $request['tgl_akhir'];

        $data['penjualan'] = Penjualan::whereBetween('created_at', [date('' . $awal . ' 00:00:00'), date('' . $akhir . ' 23:59:00')])
        ->latest()
        ->get();

        if ($request['lanjut']) {
            return view('laporan.transaksi.laporan-penjualan', $data)->with('no', 1);
        } elseif ($request['pdf']) {
            $no = 1;
            $pdf = PDF::loadView('laporan.transaksi.pdf-laporan-penjualan', $data);
            $pdf->setPaper('A4', 'potret');
            return $pdf->stream();
        } elseif ($request['excel']) {
            return Excel::download(new TabunganExport($awal,$akhir), 'laporan harian.xlsx');
        }
    }
}