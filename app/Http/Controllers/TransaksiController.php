<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DetilePenjualan;
use App\Penjualan;
use App\Produk;
use App\Pelanggan;
use App\KartuStok;
use Session;
use Validator;
use Auth;
use DB;
use PDF;
use Artisan;
use Symfony\Component\Process\Process;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penjualan = DetilePenjualan::where('faktur', session('kode_faktur'))
        ->whereNotIn('status', ['pending'])
        ->get();
        $produk = Produk::all();
        $pelanggan = Pelanggan::all();
        $sub_total = DetilePenjualan::where('faktur', session('kode_faktur'))
        ->whereNotIn('status', ['pending'])
        ->sum('sub_total');
        return view('penjualan.transaksi', compact('penjualan','produk','pelanggan','sub_total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if ($request['qty'] > baca_produk_stok($request['produk_id'])) {
            Session::flash('gagal', 'Gagal Simpan Stok Kurang!');
            return redirect('transaksi');
        } else {
            DB::transaction(function () use ($request) {
                $produk = Produk::find($request['produk_id']);
                $faktur = 'PENJUALAN'.'_'.date('dmY').(Penjualan::count() + 1);
                $cek    = DetilePenjualan::where('faktur', session('kode_faktur'))
                ->where('id_produk', $request['produk_id'])
                ->count();

                if (!session('kode_faktur')) {
                    session(['kode_faktur' => $faktur]);
                }

                $jumlah = $request['qty'];

                if($jumlah >= 10 && $jumlah <= 20){
                    $harga_jual = $produk->harga_grosir;
                }else if($jumlah >= 4 && $jumlah <= 10){
                    $harga_jual = $produk->harga_retail;
                }else if($jumlah >= 1 && $jumlah <=3){
                    $harga_jual = $produk->harga_jual;
                }else if($jumlah >= 20){
                    $harga_jual = $produk->harga_partai;
                }
                
                if ($cek > 0) {
                    $transaksi = DetilePenjualan::where('faktur', session('kode_faktur'))
                    ->where('id_produk', $request['produk_id'])->first();
                    $transaksi->jumlah = ($request['qty']+$transaksi->jumlah);
                    $transaksi->sub_total = $harga_jual*$transaksi->jumlah;
                    $transaksi->sub_total_laba = $produk->laba*$transaksi->jumlah;
                    $transaksi->update();
                } else {
                    $transaksi = new DetilePenjualan();
                    $transaksi->faktur = session('kode_faktur');
                    $transaksi->id_produk = $request['produk_id'];
                    $transaksi->kode_produk = $produk->kode_produk;
                    $transaksi->id_kategori = $produk->id_kategori;
                    $transaksi->harga = $produk->harga;
                    $transaksi->harga_jual = $harga_jual;
                    $transaksi->jumlah = $request['qty'];
                    $transaksi->sub_total = $harga_jual*$request['qty'];
                    $transaksi->sub_total_laba = $produk->laba*$request['qty'];
                    $transaksi->user_id = Auth::user()->id;
                    $transaksi->save();
                }
            });
            Session::flash('sukses', 'Berhasil Simpan !');
            return redirect('transaksi');
        }
    }

    public function simpan_transaksi(Request $request)
    {
        if ($request['simpan']) {
            if (empty($request['total_bayar'])) {
                Session::flash('total_bayar_kosong');
                return redirect('transaksi');
            } else {
                if ((rupiah($request['total_bayar'])+rupiah($request['diskon'])) < rupiah($request['total_belanja'])) {
                    Session::flash('total_bayar_kurang');
                    return redirect('transaksi');
                } else {
                    DB::transaction(function () use ($request) {             
                        $transaksi = New Penjualan();  
                        $transaksi->faktur = session('kode_faktur');
                        $transaksi->total_belanja = rupiah($request['total_belanja']);
                        $transaksi->total_bayar = rupiah($request['total_bayar']);
                        $transaksi->total_kembali = rupiah($request['total_kembali']);
                        $transaksi->jenis_bayar = 'tunai';
                        $transaksi->status = 'lunas';
                        $transaksi->id_pelanggan = !empty($request['id_pelanggan']) ? $request['id_pelanggan'] :0;
                        $transaksi->user_id = Auth::user()->id;
                        $transaksi->diskon = rupiah($request['diskon']);
                        $transaksi->save();
        
                        $detail = DetilePenjualan::where('faktur', session('kode_faktur'))->get();
                        foreach ($detail as $data) {
                            $transaksi_detail = DetilePenjualan::where('id', $data->id)->first();
                            $transaksi_detail->status = 'lunas';
                            $transaksi_detail->update();

                            $produk = Produk::where('id', '=', $data->id_produk)->first();
                            $produk->jumlah_stok -= $data->jumlah;
                            $produk->update();
        
                            if (!empty(KartuStok::where('id_produk', $data->id_produk)->latest()->first()->total)) {
                                $saldo = KartuStok::where('id_produk', $data->id_produk)->latest()->first()->total;
                            } else {
                                $saldo = Produk::where('id', $data->id_produk)->latest()->first()->jumlah_stok;
                            }
        
                            $kartu = New KartuStok();  
                            $kartu->id_produk       = $produk->id;
                            $kartu->id_periode      = date('m');
                            $kartu->tahun           = date('Y');
                            $kartu->masuk           = '0';
                            $kartu->keluar          = $data->jumlah;
                            $kartu->total           = $saldo - $data->jumlah;
                            $kartu->keterangan      = 'Penjualan';
                            $kartu->id_user         = Auth::user()->id;
                            $kartu->save();
                        }
                    });
                    Session::flash('sukses','Berhasil Simpan !');
                    return redirect('transaksi-cetak-nota');
                    session()->forget('kode_faktur');
                }            
            }
        } else if ($request['batal']) {
            if (!empty(session('kode_faktur'))) {
                if (Penjualan::where('faktur', session('kode_faktur'))->count() != 0) {
                    $transaksi = Penjualan::where('faktur', session('kode_faktur'))->first();
                    $transaksi->delete();
                }

                $detail = DetilePenjualan::where('faktur', session('kode_faktur'))->first();
                $detail->delete();

                Session::flash('sukses');
                session()->forget('kode_faktur');
                return redirect('transaksi');
            }else{
                Session::flash('data_kosong');
                return redirect('transaksi');
            }
            
        }  else if ($request['pending']) {
            if (!empty(session('kode_faktur'))) {
                if (Penjualan::where('faktur', session('kode_faktur'))->count() > 0) {
                    $transaksi = Penjualan::where('faktur', session('kode_faktur'))->first(); 
                    $transaksi->status = 'pending';
                    $transaksi->update();
    
                    $detail = DetilePenjualan::where('faktur', session('kode_faktur'))->first();
                    $detail->status = 'pending';
                    $detail->update();

                    Session::flash('sukses');
                    session()->forget('kode_faktur');
                    return redirect('transaksi');
                } else {
                    $transaksi = New Penjualan();  
                    $transaksi->faktur = session('kode_faktur');
                    $transaksi->total_belanja = 0;
                    $transaksi->total_bayar = 0;
                    $transaksi->total_kembali = 0;
                    $transaksi->jenis_bayar = 'tunai';
                    $transaksi->status = 'pending';
                    $transaksi->id_pelanggan = !empty($request['id_pelanggan']) ? $request['id_pelanggan'] :0;
                    $transaksi->user_id = Auth::user()->id;
                    $transaksi->diskon = 0;
                    $transaksi->save();
    
                    $detail = DetilePenjualan::where('faktur', session('kode_faktur'))->first();
                    $detail->status = 'pending';
                    $detail->update();
    
                    Session::flash('sukses');
                    session()->forget('kode_faktur');
                    return redirect('transaksi');
                }
                
            }else{
                Session::flash('data_kosong');
                return redirect('transaksi');
            }
            
        }
    }

    public function show($id)
    {
        if (!empty($id)) {
            if (!empty(session('kode_faktur'))) {
                if (Penjualan::where('faktur', session('kode_faktur'))->count() > 0) {
                    $transaksi_awal = Penjualan::where('faktur', session('kode_faktur'))->first(); 
                    $transaksi_awal->status = 'pending';
                    $transaksi_awal->update();
    
                    $detail_awal = DetilePenjualan::where('faktur', session('kode_faktur'))->first();
                    $detail_awal->status = 'pending';
                    $detail_awal->update();
                } else {
                    $transaksi_awal = new Penjualan();
                    $transaksi_awal->faktur = session('kode_faktur');
                    $transaksi_awal->total_belanja = 0;
                    $transaksi_awal->total_bayar = 0;
                    $transaksi_awal->total_kembali = 0;
                    $transaksi_awal->jenis_bayar = 'tunai';
                    $transaksi_awal->status = 'pending';
                    $transaksi_awal->id_pelanggan = !empty($request['id_pelanggan']) ? $request['id_pelanggan'] :0;
                    $transaksi_awal->user_id = Auth::user()->id;
                    $transaksi_awal->diskon = 0;
                    $transaksi_awal->save();
    
                    $detail_awal = DetilePenjualan::where('faktur', session('kode_faktur'))->first();
                    $detail_awal->status = 'pending';
                    $detail_awal->update();

                    return redirect('transaksi');
                }
            }

            session(['kode_faktur' => $id]);
            $transaksi = Penjualan::where('faktur', session('kode_faktur'))->first();
            $transaksi->status = 'belum';
            $transaksi->update();

            $detail = DetilePenjualan::where('faktur', session('kode_faktur'))->first();
            $detail->status = 'belum';
            $detail->update();

            return redirect('transaksi');
        }

        Session::flash('sukses');
        session()->forget('kode_faktur');
        return redirect('transaksi');
    }

    public function cetak_nota()
    {
        $data['detail'] = DetilePenjualan::where('faktur', session('kode_faktur'))->get();
        $data['total_penjualan'] = DetilePenjualan::where('faktur', session('kode_faktur'))->sum('sub_total');
        $data['penjualan'] = Penjualan::where('faktur', session('kode_faktur'))->first();

        session()->forget('kode_faktur');
        return view('penjualan.cetak-nota-transaksi', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Transaksi = Transaksi::findOrFail($id)->delete();
        return redirect()->route('.index')->with('message', 'Data berhasil dihapus!');
    }

    // laporan

    public function laporan()
    {
        $data['detail'] = DetilePenjualan::where('created_at', 'LIKE', date('Y-m-d') . '%')->get();
        $data['total_penjualan'] = DetilePenjualan::where('created_at', 'LIKE', date('Y-m-d') . '%')->sum('sub_total_laba');
        $data['penjualan'] = Penjualan::where('created_at', 'LIKE', date('Y-m-d') . '%')->get();

        // return $data; die;
        return view('laporan.transaksi.transaksi-penjualan-hari-ini', $data)->with('no', 1);
    }

    public function laporan_by_waktu(Request $request)
    {
        request()->validate(['tgl_awal' => 'required', 'tgl_akhir' => 'required'], ['tgl_awal.required' => 'Harap Diisi, Jagan Dilewati!', 'tgl_akhir.required' => 'Harap Diisi, Jagan Dilewati!']);

        $awal  = $request['tgl_awal'];
        $akhir = $request['tgl_akhir'];

        $data['total_penjualan'] = DetilePenjualan::whereBetween('created_at', [date('' . $awal . ' 00:00:00'), date('' . $akhir . ' 23:59:00')])->sum('sub_total_laba');
        $data['penjualan'] = Penjualan::whereBetween('created_at', [date('' . $awal . ' 00:00:00'), date('' . $akhir . ' 23:59:00')])
        ->distinct('faktur')
		->latest('faktur')
		->get(['faktur']);

        if ($request['lanjut']) {
            return view('laporan.transaksi.transaksi-penjualan-hari-ini', $data)->with('no', 1);
        } elseif ($request['pdf']) {
            $no = 1;
            $pdf = PDF::loadView('laporan.transaksi.pdf-transaksi-penjualan-hari-ini', $data);
            $pdf->setPaper('A4', 'potret');
            return $pdf->stream();
        } elseif ($request['excel']) {
            return Excel::download(new TabunganExport($awal,$akhir), 'laporan harian.xlsx');
        }
    }
}
