<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DetailPemakaian;
use App\Pemakaian;
use App\Produk;
use App\Penjualan;
use App\KartuStok;
use Session;
use Validator;
use Auth;
use DB;
use PDF;
use Artisan;
use Symfony\Component\Process\Process;

class PemakaianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['pemakaian'] = DetailPemakaian::where('faktur', session('kode_faktur_pemakaian'))
        ->whereNotIn('status', ['pending'])
        ->get();
        $data['produk'] = Produk::all();
        $data['sub_total'] = DetailPemakaian::where('faktur', session('kode_faktur_pemakaian'))
        ->whereNotIn('status', ['pending'])
        ->sum('sub_total');
        
        return view('pemakaian.transaksi', $data);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if ($request['qty'] > baca_produk_stok($request['produk_id'])) {
            Session::flash('gagal', 'Gagal Simpan Stok Kurang!');
            return redirect('transaksi-pemakaian');
        } else {
            DB::transaction(function () use ($request) {
                $produk = Produk::find($request['produk_id']);
                $faktur = 'PEMAKAIAN'.'_'.date('dmY').(Pemakaian::count() + 1);
                $cek    = DetailPemakaian::where('faktur', session('kode_faktur_pemakaian'))
                ->where('id_produk', $request['produk_id'])
                ->count();

                if (!session('kode_faktur_pemakaian')) {
                    session(['kode_faktur_pemakaian' => $faktur]);
                }
                
                if ($cek > 0) {
                    $transaksi = DetailPemakaian::where('faktur', session('kode_faktur_pemakaian'))
                    ->where('id_produk', $request['produk_id'])->first();
                    $transaksi->jumlah = ($request['qty']+$transaksi->jumlah);
                    $transaksi->sub_total = $produk->harga_jual*$transaksi->jumlah;
                    $transaksi->sub_total_laba = $produk->laba*$transaksi->jumlah;
                    $transaksi->update();
                } else {
                    $transaksi = new DetailPemakaian();
                    $transaksi->faktur = session('kode_faktur_pemakaian');
                    $transaksi->id_produk = $request['produk_id'];
                    $transaksi->kode_produk = $produk->kode_produk;
                    $transaksi->id_kategori = $produk->id_kategori;
                    $transaksi->harga = $produk->harga;
                    $transaksi->harga_jual = $produk->harga_jual;
                    $transaksi->jumlah = $request['qty'];
                    $transaksi->sub_total = $produk->harga_jual*$request['qty'];
                    $transaksi->sub_total_laba = $produk->laba*$request['qty'];
                    $transaksi->user_id = Auth::user()->id;
                    $transaksi->save();
                }
            });
            Session::flash('sukses', 'Berhasil Simpan !');
            return redirect('transaksi-pemakaian');
        }
    }

    public function simpan_transaksi(Request $request)
    {
        if ($request['simpan']) {
            if (empty($request['total_bayar'])) {
                Session::flash('total_bayar_kosong');
                return redirect('transaksi-pemakaian');
            } else {
                if ((rupiah($request['total_bayar'])+rupiah($request['diskon'])) < rupiah($request['total_belanja'])) {
                    Session::flash('total_bayar_kurang');
                    return redirect('transaksi-pemakaian');
                } else {
                    DB::transaction(function () use ($request) {             
                        $transaksi = New Pemakaian();  
                        $transaksi->faktur = session('kode_faktur_pemakaian');
                        $transaksi->total_belanja = rupiah($request['total_belanja']);
                        $transaksi->total_bayar = rupiah($request['total_bayar']);
                        $transaksi->total_kembali = rupiah($request['total_kembali']);
                        $transaksi->jenis_bayar = 'tunai';
                        $transaksi->status = 'lunas';
                        $transaksi->id_pelanggan = !empty($request['id_pelanggan']) ? $request['id_pelanggan'] :0;
                        $transaksi->user_id = Auth::user()->id;
                        $transaksi->diskon = rupiah($request['diskon']);
                        $transaksi->save();
        
                        $detail = DetailPemakaian::where('faktur', session('kode_faktur_pemakaian'))->get();
                        foreach ($detail as $data) {
                            $transaksi_detail = DetailPemakaian::where('id', $data->id)->first();
                            $transaksi_detail->status = 'lunas';
                            $transaksi_detail->update();

                            $produk = Produk::where('id', '=', $data->id_produk)->first();
                            $produk->jumlah_stok -= $data->jumlah;
                            $produk->update();
        
                            if (!empty(KartuStok::where('id_produk', $data->id_produk)->latest()->first()->total)) {
                                $saldo = KartuStok::where('id_produk', $data->id_produk)->latest()->first()->total;
                            } else {
                                $saldo = Produk::where('id', $data->id_produk)->latest()->first()->jumlah_stok;
                            }
                            
                            $kartu = New KartuStok();  
                            $kartu->id_produk       = $produk->id;
                            $kartu->id_periode      = date('m');
                            $kartu->tahun           = date('Y');
                            $kartu->masuk           = '0';
                            $kartu->keluar          = $data->jumlah;
                            $kartu->total           = $saldo - $data->jumlah;
                            $kartu->keterangan      = 'Pemakaian Toko';
                            $kartu->id_user         = Auth::user()->id;
                            $kartu->save();
                        }
                    });
                    Session::flash('sukses','Berhasil Simpan !');
                    return redirect('transaksi-pemakaian-cetak-nota');
                }            
            }
        } else if ($request['batal']) {
            if (!empty(session('kode_faktur_pemakaian'))) {
                if (Pemakaian::where('faktur', session('kode_faktur_pemakaian'))->count() != 0) {
                    $transaksi = Pemakaian::where('faktur', session('kode_faktur_pemakaian'))->first();
                    $transaksi->delete();
                }

                $detail = DetailPemakaian::where('faktur', session('kode_faktur_pemakaian'))->first();
                $detail->delete();

                Session::flash('sukses');
                session()->forget('kode_faktur_pemakaian');
                return redirect('transaksi-pemakaian');
            }else{
                Session::flash('data_kosong');
                return redirect('transaksi-pemakaian');
            }
            
        }  else if ($request['pending']) {
            if (!empty(session('kode_faktur_pemakaian'))) {
                if (Pemakaian::where('faktur', session('kode_faktur_pemakaian'))->count() > 0) {
                    $transaksi = Pemakaian::where('faktur', session('kode_faktur_pemakaian'))->first(); 
                    $transaksi->status = 'pending';
                    $transaksi->update();
    
                    $detail = DetailPemakaian::where('faktur', session('kode_faktur_pemakaian'))->first();
                    $detail->status = 'pending';
                    $detail->update();

                    Session::flash('sukses');
                    session()->forget('kode_faktur_pemakaian');
                    return redirect('transaksi-pemakaian');
                } else {
                    $transaksi = New Pemakaian();  
                    $transaksi->faktur = session('kode_faktur_pemakaian');
                    $transaksi->total_belanja = 0;
                    $transaksi->total_bayar = 0;
                    $transaksi->total_kembali = 0;
                    $transaksi->jenis_bayar = 'tunai';
                    $transaksi->status = 'pending';
                    $transaksi->id_pelanggan = !empty($request['id_pelanggan']) ? $request['id_pelanggan'] :0;
                    $transaksi->user_id = Auth::user()->id;
                    $transaksi->diskon = 0;
                    $transaksi->save();
    
                    $detail = DetailPemakaian::where('faktur', session('kode_faktur_pemakaian'))->first();
                    $detail->status = 'pending';
                    $detail->update();
    
                    Session::flash('sukses');
                    session()->forget('kode_faktur');
                    return redirect('transaksi-pemakaian');
                }
                
            }else{
                Session::flash('data_kosong');
                return redirect('transaksi-pemakaian');
            }
            
        }
    }

    public function show($id)
    {
        if (!empty($id)) {
            if (!empty(session('kode_faktur_pemakaian'))) {
                if (Pemakaian::where('faktur', session('kode_faktur_pemakaian'))->count() > 0) {
                    $transaksi_awal = Pemakaian::where('faktur', session('kode_faktur_pemakaian'))->first(); 
                    $transaksi_awal->status = 'pending';
                    $transaksi_awal->update();
    
                    $detail_awal = DetailPemakaian::where('faktur', session('kode_faktur_pemakaian'))->first();
                    $detail_awal->status = 'pending';
                    $detail_awal->update();
                } else {
                    $transaksi_awal = new Pemakaian();
                    $transaksi_awal->faktur = session('kode_faktur_pemakaian');
                    $transaksi_awal->total_belanja = 0;
                    $transaksi_awal->total_bayar = 0;
                    $transaksi_awal->total_kembali = 0;
                    $transaksi_awal->jenis_bayar = 'tunai';
                    $transaksi_awal->status = 'pending';
                    $transaksi_awal->id_pelanggan = !empty($request['id_pelanggan']) ? $request['id_pelanggan'] :0;
                    $transaksi_awal->user_id = Auth::user()->id;
                    $transaksi_awal->diskon = 0;
                    $transaksi_awal->save();
    
                    $detail_awal = DetailPemakaian::where('faktur', session('kode_faktur_pemakaian'))->first();
                    $detail_awal->status = 'pending';
                    $detail_awal->update();

                    return redirect('transaksi-pemakaian');
                }
            }

            session(['kode_faktur' => $id]);
            $transaksi = Pemakaian::where('faktur', session('kode_faktur_pemakaian'))->first();
            $transaksi->status = 'belum';
            $transaksi->update();

            $detail = DetailPemakaian::where('faktur', session('kode_faktur_pemakaian'))->first();
            $detail->status = 'belum';
            $detail->update();

            return redirect('transaksi-pemakaian');
        }

        Session::flash('sukses');
        session()->forget('kode_faktur');
        return redirect('transaksi-pemakaian');
    }

    public function cetak_nota()
    {
        $data['detail'] = DetailPemakaian::where('faktur', session('kode_faktur_pemakaian'))->get();
        $data['total_pemakaian'] = DetailPemakaian::where('faktur', session('kode_faktur_pemakaian'))->sum('sub_total');
        $data['pemakaian'] = Pemakaian::where('faktur', session('kode_faktur_pemakaian'))->first();

        session()->forget('kode_faktur');
        return view('pemakaian.cetak-nota-transaksi', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $Transaksi = Transaksi::findOrFail($id)->delete();
        return redirect()->route('.index')->with('message', 'Data berhasil dihapus!');
    }

    // laporan

    public function laporan()
    {
        $data['detail'] = DetailPemakaian::where('created_at', 'LIKE', date('Y-m-d') . '%')->get();
        $data['total_penjualan'] = DetailPemakaian::where('created_at', 'LIKE', date('Y-m-d') . '%')->sum('sub_total_laba');
        $data['pemakaian'] = Pemakaian::where('created_at', 'LIKE', date('Y-m-d') . '%')->get();

        // return $data; die;
        return view('laporan.transaksi.transaksi-pemakaian-hari-ini', $data)->with('no', 1);
    }

    public function laporan_by_waktu(Request $request)
    {
        request()->validate(['tgl_awal' => 'required', 'tgl_akhir' => 'required'], ['tgl_awal.required' => 'Harap Diisi, Jagan Dilewati!', 'tgl_akhir.required' => 'Harap Diisi, Jagan Dilewati!']);

        $awal  = $request['tgl_awal'];
        $akhir = $request['tgl_akhir'];

        $data['total_penjualan'] = DetailPemakaian::whereBetween('created_at', [date('' . $awal . ' 00:00:00'), date('' . $akhir . ' 23:59:00')])->sum('sub_total_laba');
        $data['pemakaian'] = Pemakaian::whereBetween('created_at', [date('' . $awal . ' 00:00:00'), date('' . $akhir . ' 23:59:00')])
        ->distinct('faktur')
		->latest('faktur')
		->get(['faktur']);

        if ($request['lanjut']) {
            return view('laporan.transaksi.transaksi-pemakaian-hari-ini', $data)->with('no', 1);
        } elseif ($request['pdf']) {
            $no = 1;
            $pdf = PDF::loadView('laporan.transaksi.pdf-transaksi-pemakaian', $data);
            $pdf->setPaper('A4', 'potret');
            return $pdf->stream();
        } elseif ($request['excel']) {
            return Excel::download(new TabunganExport($awal,$akhir), 'laporan harian.xlsx');
        }
    }
}