<?php

namespace App\Http\Controllers;

use DataTables;
use Illuminate\Http\Request;

class RoleController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return view('user.role');
	}

	public function datalist() {
		$role = \App\Role::all();
		return Datatables::of($role)
			->addIndexColumn()
			->addColumn('edit', function ($role) {
				return '<button type="button" onclick="editForm(' . $role->id . ')" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-edit"></i></button>';
			})
			->rawColumns(['edit'])
			->make(true);

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$role = \App\Role::find($id);
		return $role;
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$role = \App\Role::find($id);
		$role->display_name = $request['display_name'];
		$role->description = $request['description'];
		$role->update();
		return response()->json(['success' => true, 'message' => 'Role berhasil diupdate']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
