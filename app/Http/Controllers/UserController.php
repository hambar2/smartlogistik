<?php

namespace App\Http\Controllers;

use DataTables;
use Illuminate\Http\Request;
use Validator;

class UserController extends Controller {

	public function index() {
		return view('user.user');
	}

	public static function datalist() {
		$user = \App\User::all();
		return Datatables::of($user)
			->addIndexColumn()
			->addColumn('role', function ($user) {
				foreach ($user->roles as $d) {
					$r[] = ' ' . $d->display_name;
				}
				return implode(',', $r);
			})
			->addColumn('edit', function ($user) {
				return '<button type="button" onclick="editForm(' . $user->id . ')" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-edit"></i></button>';
			})
			->rawColumns(['edit'])
			->make(true);
	}

	public static function getRole() {
		$role = \App\Role::all();
		return $role;
	}

	public function create() {
		//
	}

	public function store(Request $request) {
		$cek = Validator::make($request->all(), [
			'name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:users',
			'password' => 'required|string|min:6|confirmed',
			'role' => 'required',
		]);

		if ($cek->passes()) {
			$user = \App\User::create([
				'name' => $request['name'],
				'email' => $request['email'],
				'foto' => NULL,
				'password' => bcrypt($request['password']),
			]);
			foreach ($request['role'] as $d) {
				$role = \App\Role::where('name', $d)->first();
				$user->attachRole($role);
			}
			return response()->json(['success' => '1', 'message' => 'User berhasil ditambahkan.']);
		} else {
			return response()->json(['errors' => $cek->errors()]);
		}
	}

	public function show($id) {
		//
	}

	public function edit($id) {
		$data['user'] = \App\User::findOrFail($id);
		$data['role'] = $data['user']->roles;
		return $data;
	}

	public function update(Request $request, $id) {
		$user = \App\User::findOrFail($id);
		$cek = Validator::make($request->all(), [
			'name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:users,email,' . $id,
			'password' => 'nullable|string|min:6|confirmed',
			'role' => 'required',
		]);

		if ($cek->passes()) {
			$password = !empty($request['password']) ? bcrypt($request['password']) : $user->password;
			$user->update([
				'name' => $request['name'],
				'email' => $request['email'],
				'foto' => NULL,
				'password' => $password,
			]);
			foreach ($user->roles as $d) {
				$user->detachRole($d);
			}
			foreach ($request['role'] as $d) {
				$role = \App\Role::where('name', $d)->first();
				$user->attachRole($role);
			}

			return response()->json(['success' => '1', 'message' => 'User berhasil di update']);
		} else {
			return response()->json(['errors' => $cek->errors()]);
		}

	}

	public function destroy($id) {
		//
	}
}
