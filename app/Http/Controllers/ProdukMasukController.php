<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DetileProdukMasuk;
use App\ProdukMasuk;
use App\KartuStok;
use App\Produk;
use App\Kategori;
use Session;
use Validator;
use Auth;
use DB;

class ProdukMasukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori = Kategori::all();
        $detailmasuk = DetileProdukMasuk::where('no_masuk', session('kode_masuk'))->get();
        $produk = Produk::all();
        return view('logistik.transaksi-produk-masuk', compact('detailmasuk','produk','kategori'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {       
            $transaksi = New ProdukMasuk();  
            $transaksi->no_masuk        = session('kode_masuk');
            $transaksi->tanggal         = $request['tanggal'];
            $transaksi->id_periode      = valid_date_bulan($request['tanggal']);
            $transaksi->tahun           = valid_date_tahun($request['tanggal']);
            $transaksi->id_user         = Auth::user()->id;
            $transaksi->keterangan     = $request['keterangan'];
            $transaksi->save();

            
            $detail = DetileProdukMasuk::where('no_masuk', session('kode_masuk'))->get();
            foreach ($detail as $data) {
                $produk = Produk::where('id', '=', $data->id_produk)->first();
                $produk->jumlah_stok += $data->jumlah_masuk;
                $produk->update();

                if (!empty(KartuStok::where('id_produk', $data->id_produk)->latest()->first()->total)) {
                    $saldo = KartuStok::where('id_produk', $data->id_produk)->latest()->first()->total;
                } else {
                    $saldo = Produk::where('id', $data->id_produk)->latest()->first()->saldo_obat;
                }
                

                $kartu = New KartuStok();  
                $kartu->id_produk       = $produk->id;
                $kartu->id_periode      = date('m');
                $kartu->tahun           = date('Y');
                $kartu->masuk           = $data->jumlah_masuk;
                $kartu->keluar          = '0';
                $kartu->total           = $saldo + $data->jumlah_masuk;
                $kartu->keterangan      = 'Produk Masuk Keterangan =>'.$request['keterangan'].'';
                $kartu->id_user         = Auth::user()->id;
                $kartu->save();
            }
        });
        session()->forget('kode_masuk');
        Session::flash('sukses_simpan_total','Berhasil Simpan !');
        return redirect('transaksi-masuk');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function simpanProduk(Request $request)
    {
        // return $request->all(); die;
        // return "berhasil";

        
        $cek = Validator::make($request->all(), [
        'nama_produk' => ['required','unique:produks'],
        'id_kategori' => ['required'],
        'qty' => ['required'],
        ],[
        'nama_produk.required' => 'Nama produk Wajib Diisi !',
        'nama_produk.unique' => 'nama produk Sudah Ada !',
        'id_kategori.required' => 'Kategori Wajib Diisi !',
        'qty.required' => 'Jumlah Wajib Diisi !',
        ]);
        if ($cek->fails()) {
        return response()->json(['sukses' => false, 'error' => $cek->errors()]);
        } else {
            $kategori = Kategori::find($request['id_kategori']);
            $kode = 'P'.substr($kategori->nama,0,3).'_'.(Produk::count() + 1);             
                $produk = New produk();
                $produk->kode_produk = sprintf("%03s", strtoupper($kode));
                $produk->id_kategori = $request['id_kategori'];
                $produk->nama_produk = $request['nama_produk'];
                $produk->jumlah_stok = $request['qty'];
                $produk->ukuran = '-';
                $produk->harga = rupiah($request['harga']);
                $produk->harga_jual = rupiah($request['harga_ecer']);
                $produk->harga_grosir = rupiah($request['harga_grosir']);
                $produk->harga_retail = rupiah($request['harga_reseller']);
                $produk->harga_partai = rupiah($request['harga_partai']);
                $produk->laba = '0';
                $produk->tanggal_masuk = date('Y-m-d');
                $produk->save();

                $dataproduk = Produk::find($produk->id);
                $faktur = 'MASUK'.'_'.date('dmY').(ProdukMasuk::count() + 1);
                $cek = DetileProdukMasuk::where('no_masuk', session('kode_masuk'))
                ->where('id_produk', $dataproduk->id)
                ->count();

                if (!session('kode_masuk')) {
                session(['kode_masuk' => $faktur]);
                }

                if ($cek > 0) {
                $transaksi = DetileProdukMasuk::where('no_masuk', session('kode_masuk'))
                ->where('id_produk', $dataproduk->id)->first();
                $transaksi->jumlah_masuk = $request['qty']+$transaksi->jumlah_masuk;
                $transaksi->update();
                } else {
                $transaksi = New DetileProdukMasuk();
                $transaksi->no_masuk = session('kode_masuk');
                $transaksi->id_produk = $dataproduk->id;
                $transaksi->kode_produk = $dataproduk->kode_produk;
                $transaksi->nama_produk = $dataproduk->nama_produk;
                $transaksi->jumlah_masuk = $request['qty'];
                $transaksi->save();
        }
        return response()->json(['sukses' => true, 'message' => 'Berhasil Simpan']);
        
        }
    }
}