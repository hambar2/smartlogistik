<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KartuStok;
use App\Produk;
use App\Periode;
use Session;
use Validator;
use Auth;
use DB;
use PDF;

class KartuStokController extends Controller
{
    public function kartu_stok()
    {
        $data['produk'] = Produk::all();
        $data['periode'] = Periode::all();
        return view('logistik.kartu-stok', $data);
    }

    public function kartu_stok_by_index(Request $request)
    {
        $data['periode'] = Periode::all();
        $data['produk'] = Produk::all();

        $id_produk  = $request['id_produk'];
        $id_periode = $request['id_periode'];
        $tahun  = $request['tahun'];

        $data['kartu_stok'] = KartuStok::where('id_produk', $id_produk)
        ->where('id_periode', $id_periode)
        ->get();

        if ($request['lanjut']) {
            return view('logistik.kartu-stok', $data);
        }if ($request['pdf']) {
            $data['no'] = 1;
            $pdf = PDF::loadView('laporan.logistik.pdf-kartu-stok', $data);
            $pdf->setPaper('A4', 'portrait');
            return $pdf->stream();
        }
    }
}
