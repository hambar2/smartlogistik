<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenerimaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penerimaans', function (Blueprint $table) {
            $table->id();
            $table->string('no_masuk', 25);
            $table->bigInteger('id_produk');
            $table->string('nama_produk');
            $table->bigInteger('id_kategori');
            $table->bigInteger('harga_beli')->default('0');
            $table->bigInteger('harga_jual')->default('0');
            $table->bigInteger('harga_grosir')->default('0');
            $table->bigInteger('harga_retail')->default('0');
            $table->bigInteger('harga_partai')->default('0');
            $table->bigInteger('jumlah_masuk');
            $table->date('tanggal');
            $table->bigInteger('id_periode');
            $table->string('tahun', 4);
            $table->bigInteger('id_user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penerimaans');
    }
}