<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKartuStoksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kartu_stoks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_periode');
            $table->string('tahun', 4);
            $table->bigInteger('id_produk');
            $table->bigInteger('masuk');
            $table->bigInteger('keluar');
            $table->bigInteger('total');
            $table->longText('keterangan');
            $table->bigInteger('id_user');
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kartu_stoks');
    }
}
