<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHargaSubTotalToDetileRetursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detile_returs', function (Blueprint $table) {
            $table->bigInteger('harga_jual')->after('nama_produk');
            $table->bigInteger('sub_total')->after('jumlah');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detile_returs', function (Blueprint $table) {
            //
        });
    }
}
