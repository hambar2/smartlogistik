<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenjualansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penjualans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('faktur', 25);
            $table->bigInteger('total_belanja');
            $table->bigInteger('diskon');
            $table->bigInteger('total_bayar');
            $table->bigInteger('total_kembali');
            $table->string('jenis_bayar', 10);
            $table->enum('status', ['lunas', 'belum', 'pending'])->default('belum');
            $table->bigInteger('user_id');
            $table->bigInteger('id_pelanggan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penjualans');
    }
}
