<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetilePenjualansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detile_penjualans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('faktur', 25);
            $table->bigInteger('id_produk');
            $table->string('kode_produk', 10);
            $table->bigInteger('id_kategori');
            $table->bigInteger('harga');
            $table->bigInteger('harga_jual');
            $table->bigInteger('jumlah');
            $table->bigInteger('sub_total');
            $table->bigInteger('sub_total_laba');
            $table->bigInteger('user_id');
            $table->enum('status', ['lunas', 'belum', 'pending'])->nullable()->default('belum');
            $table->timestamps();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detile_penjualans');
    }
}
