<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetileProdukMasuksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detile_produk_masuks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_masuk', 25);
            $table->bigInteger('id_produk');
            $table->string('nama_produk');
            $table->string('kode_produk', 10);
            $table->bigInteger('jumlah_masuk');
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detile_produk_masuks');
    }
}
