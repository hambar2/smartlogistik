<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusTerimaToDetileProdukMasuks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detile_produk_masuks', function (Blueprint $table) {
            $table->enum('status_terima', ['sudah','belum'])->default('belum')->after('jumlah_masuk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detile_produk_masuks', function (Blueprint $table) {
            
        });
    }
}