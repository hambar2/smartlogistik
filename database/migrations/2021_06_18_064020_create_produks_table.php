<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProduksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_produk', 10)->unique();
            $table->bigInteger('id_kategori');
            $table->string('nama_produk');
            $table->bigInteger('jumlah_stok');
            $table->date('tanggal_masuk');
            $table->string('ukuran', 100);
            $table->bigInteger('harga')->default('0');
            $table->bigInteger('harga_jual')->default('0');
            $table->bigInteger('harga_grosir')->default('0');
            $table->bigInteger('harga_retail')->default('0');
            $table->bigInteger('laba')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produks');
    }
}
