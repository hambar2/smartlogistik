<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRetursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('returs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('faktur', 25);
            $table->string('kode_retur', 25);
            $table->date('tanggal');
            $table->bigInteger('id_user');
            $table->bigInteger('id_pelanggan');
            $table->longText('keterangan');
            $table->enum('jenis_retur', ['tunai', 'tukar'])->nullable();
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('returs');
    }
}
