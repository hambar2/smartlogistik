<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetileRetursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detile_returs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_retur', 25);
            $table->string('kode_produk', 10);
            $table->bigInteger('id_produk');
            $table->string('nama_produk');
            $table->bigInteger('jumlah');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detile_returs');
    }
}
