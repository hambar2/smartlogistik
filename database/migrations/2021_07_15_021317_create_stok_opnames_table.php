<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStokOpnamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stok_opnames', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_kategori');
            $table->bigInteger('id_produk');
            $table->string('kode_produk', 10);
            $table->string('nama_produk', 25);
            $table->date('tanggal');
            $table->bigInteger('id_periode');
            $table->string('tahun', 4);
            $table->bigInteger('stok_tercatat');
            $table->bigInteger('stok_sebenarnya');
            $table->longText('keterangan');
            $table->bigInteger('user_id');
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stok_opnames');
    }
}
