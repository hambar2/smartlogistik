<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		//Create Administrator role
		$administratorRole = new Role();
		$administratorRole->name = 'administrator';
		$administratorRole->display_name = 'Administrator';
		$administratorRole->save();

		//Create Admin Role
		$adminRole = new Role();
		$adminRole->name = 'kasir';
		$adminRole->display_name = 'kasir';
		$adminRole->save();

		//Create staff role
		$staffRole = new Role();
		$staffRole->name = 'pemilik';
		$staffRole->display_name = 'pemilik';
		$staffRole->save();

		//Create Administrator User
		$administrator = new User();
		$administrator->name = 'Administrator';
		$administrator->email = 'administrator@mail.com';
		$administrator->password = bcrypt('123456');
		$administrator->foto = '';
		$administrator->save();
		$administrator->attachRole($administratorRole);
		$administrator->attachRole($adminRole);
		$administrator->attachRole($staffRole);

		//Create Admin User
		$admin = new User();
		$admin->name = 'kasir';
		$admin->email = 'kasir@mail.com';
		$admin->password = bcrypt('123456');
		$admin->foto = '';
		$admin->save();
		$admin->attachRole($adminRole);
		$admin->attachRole($staffRole);

		//Create Staff Users
		$staff = new User();
		$staff->name = 'pemilik';
		$staff->email = 'pemilik@mail.com';
		$staff->password = bcrypt('123456');
		$staff->foto = '';
		$staff->save();
		$staff->attachRole($staffRole);

		//periode		
		$periode = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
		foreach ($periode as $periode) {
			DB::table('periodes')->insert([
    			'nama' => $periode,
    		]);
		}
	}
}
