<?php
use Illuminate\Support\Facades\Route;
Auth::routes();
Auth::id();

Route::group(['middleware' => 'auth'], function () {
	Route::get('/', 'HomeController@index');
	Route::get('/home', 'HomeController@index')->name('home');

	Route::resource('user', 'UserController')->except(['create', 'show', 'destroy']);
	Route::get('user-data', 'UserController@datalist')->name('user-data');
	Route::get('getRole', 'UserController@getRole')->name('getRole');

	Route::resource('role', 'RoleController')->except(['create', 'store', 'show', 'destroy']);
	Route::get('role-data', 'RoleController@datalist')->name('role-data');

	Route::resource('periode', 'PeriodeController');
	Route::resource('kategori', 'KategoriController');
	Route::resource('pelanggan', 'PelangganController');
	Route::get('get-diskon-member/{id}', 'PelangganController@get_diskon');
	Route::resource('produk', 'ProdukController');
	Route::post('cek-data-produk', 'ProdukController@cekData')->name('cek-data-produk');
	Route::resource('gudang', 'GudangController');

	// Transaksi Masuk
	Route::resource('transaksi-masuk', 'ProdukMasukController');
	Route::post('transaksi-tambah-produk', 'ProdukMasukController@simpanProduk')->name('transaksi-tambah-produk.store');
	Route::resource('transaksi-list-masuk', 'DetileProdukMasukController');
	
	// Transaksi Penjualan
	Route::resource('transaksi', 'TransaksiController');
	Route::post('simpan-transaksi', 'TransaksiController@simpan_transaksi')->name('simpan_transaksi.store');
	Route::get('transaksi-cetak-nota', 'TransaksiController@cetak_nota');
	Route::resource('transaksi-list', 'DetilePenjualanController');
	Route::get('transaksi-list/{id?}/delete', 'DetilePenjualanController@destroy');
	Route::get('transaksi-hari-ini', 'DetilePenjualanController@index_hari_ini');
	Route::get('cetak-transaksi-hari-ini', 'DetilePenjualanController@cetak_transaksi_hari_ini');
	Route::post('edit-jumlah-detail-transaksi', 'DetilePenjualanController@updateJumlah');

	// Transaksi Pemakaian
	Route::resource('transaksi-pemakaian', 'PemakaianController');
	Route::post('simpan-transaksi-pemakaian', 'PemakaianController@simpan_transaksi')->name('simpan_transaksi_pemakaian.store');
	Route::get('transaksi-pemakaian-cetak-nota', 'PemakaianController@cetak_nota');
	Route::resource('transaksi-pemakaian-list', 'DetailPemakaianController');
	Route::get('transaksi-pemakaian-hari-ini', 'DetailPemakaianController@index_hari_ini');
	Route::get('cetak-transaksi-pemakaian-hari-ini', 'DetailPemakaianController@cetak_transaksi_hari_ini');

	// Transaksi Pengeluaran
	Route::resource('transaksi-pengeluaran', 'PengeluaranController');

	// Transaksi Pre Order
	Route::resource('transaksi-pre-order', 'PreOrderController');

	// Transaksi Retur
	Route::resource('transaksi-retur', 'ReturController');
	Route::get('detil-transaksi-retur/{id}', 'ReturController@detail')->name('transaksi-retur.detail');
	
	// Logistik
	// kartu stok
	Route::get('kartu-stok', 'KartuStokController@kartu_stok')->name('kartu-stok.index');
	Route::post('kartu-stok', 'KartuStokController@kartu_stok_by_index')->name('kartu-stok.index');

	// stok opname
	Route::get('stok-opname', 'StokOpnameController@stok_opname')->name('stok-opname.index');
	Route::post('stok-opname', 'StokOpnameController@stok_opname_by_index')->name('stok-opname.index');
	Route::post('simpan-stok-opname', 'StokOpnameController@store');

	// Penerimaan 
	Route::resource('penerimaan','PenerimaanController');
	// Route::get('detail-penerimaan/{id}/{nomer_masuk}','PenerimaanController@detailBeritaAcara');


	

	// laporan
	Route::prefix('laporan')->group(function () {
		Route::get('transaksi-harian', 'TransaksiController@laporan')->name('laporan.transaksi-harian');
		Route::post('transaksi-harian', 'TransaksiController@laporan_by_waktu')->name('laporan.transaksi-harian');

		Route::get('transaksi-pemakaian-harian', 'PemakaianController@laporan')->name('laporan.transaksi-pemakaian-harian');
		Route::post('transaksi-pemakaian-harian', 'PemakaianController@laporan_by_waktu')->name('laporan.transaksi-pemakaian-harian');

		Route::get('barang', 'ProdukController@laporan')->name('laporan.barang');
		Route::post('barang', 'ProdukController@laporan_by_waktu')->name('laporan.barang');

		Route::get('transaksi-pengeluaran', 'PengeluaranController@laporan')->name('laporan.transaksi-pengeluaran');
		Route::post('transaksi-pengeluaran', 'PengeluaranController@laporan_by_waktu')->name('laporan.transaksi-pengeluaran');

		Route::get('penerimaan', 'PenerimaanController@laporan')->name('laporan.penerimaan');
		Route::post('penerimaan', 'PenerimaanController@laporan_by_tanggal')->name('laporan.penerimaan');

		Route::get('opname', 'StokOpnameController@laporan')->name('laporan.opname');
		Route::post('opname', 'StokOpnameController@laporan_by_tanggal')->name('laporan.opname');

		Route::get('penjualan', 'DetilePenjualanController@laporan')->name('laporan.penjualan');
		Route::post('penjualan', 'DetilePenjualanController@laporan_by_tanggal')->name('laporan.penjualan');

		Route::get('pelanggan', 'PelangganController@laporan')->name('laporan.pelanggan');
		Route::post('pelanggan', 'PelangganController@laporan_by_tanggal')->name('laporan.pelanggan');

		Route::get('barang-masuk', 'DetileProdukMasukController@laporan')->name('laporan.barang-masuk');
		Route::post('barang-masuk', 'DetileProdukMasukController@laporan_by_tanggal')->name('laporan.barang-masuk');
	});

	Route::get('view-import-produk', 'ProdukController@view_import')->name('import.view-import');
	Route::post('template-produk', 'ProdukController@template_import')->name('import.template-produk');
	Route::post('import-produk', 'ProdukController@import')->name('import.produk');

	Route::prefix('verifikasi')->group(function () {
        Route::post('produk', 'ProdukController@data_verifikasi_produk')->name('verifikasi.data-produk');
    });
});